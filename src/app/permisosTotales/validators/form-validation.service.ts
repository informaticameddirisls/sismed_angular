import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormValidationService {

  constructor() { }

  // Método para validar un FormControl y mostrar el mensaje de error
  esCampoValido(form: FormGroup, controlName: string): boolean | null {
    return (form.controls[controlName].status == "INVALID")? true : false;

  }

  // Método para mostrar un mensaje de error
  mensajeError(form: FormGroup,controlName: string): string {
    // Aquí puedes implementar la lógica para mostrar el mensaje de error,
    // como por ejemplo, utilizando un Toast o un componente de alerta.
    if ( !form.controls[controlName] ) return '';

    const errors = form.controls[controlName].errors || {};

    for (const key of Object.keys(errors) ) {
      switch( key ) {
        case 'required':
          return 'Este campo es requerido';

        case 'minlength':
          return `Mínimo ${ errors['minlength'].requiredLength } caracters.`;

          case 'email':
            return `Formato correo no valido.`;
      }
    }

    return '';
  }
}
