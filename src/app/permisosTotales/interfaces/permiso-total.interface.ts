export interface Permisototal {
  id_permiso_total:     number;
  id_empresa:           string;
  id_ris:               null;
  id_almacen:           string;
  id_supermodulo:       number;
  id_modulo:            number;
  id_submodulo:         number;
  id_permiso:           number;
  id_usuario:           number;
  id_rol:               null;
  fecha_creacion:       Date;
  fecha_modificacion:   null;
  usuario_creacion:     null;
  usuario_modificacion: null;
  estado:               boolean;
  eliminado:            boolean;
  created_at:           Date;
  updated_at:           Date;
}
