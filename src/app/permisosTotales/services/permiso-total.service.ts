import { Injectable } from '@angular/core';
import { environments } from '../../../environments/environments';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, map, of, tap, throwError } from 'rxjs';
import { Permisototal } from '../interfaces/permiso-total.interface';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class PermisoTotalService {

  private baseUrl = environments.baseUrl;
  private token = localStorage.getItem('token');
  private headers = new HttpHeaders;


  constructor(private http: HttpClient,
    private _snackBar: MatSnackBar
  ) {
    if (this.token) {
      // Encabezados de solicitud HTTP con el token
      this.headers = new HttpHeaders({
        'Authorization': `Bearer ${this.token}`
      });
    }
   }

  buscarPorUsuario(id_usuario:number,parametros:any): Observable<any> {
    var headers = this.headers;

    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/administrador/maestra/permisototal/buscarporusuario/${id_usuario}`,parametros, { headers }).pipe(
        tap((supermodulo) => {
        }),
        catchError(error => {
          console.error('Error al listar permisoTotal:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  crear(permisoTotal:Permisototal,id_usuario:number): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/permisototal/crear/${id_usuario}`, permisoTotal, { headers }).pipe(
        tap((response) => {
          this._snackBar.open(`${response.message}\n
          creados: ${response.data.creados}\n
          existentes: ${response.data.existentes}\n
          total: ${response.data.total}`, 'ok', {
            horizontalPosition: 'start',
            verticalPosition: 'bottom',
            duration: 10 * 1000,
          });
        }),
        catchError(error => {
          console.error('Error al listar permisoTotal:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }


  eliminar(permisoTotal:Permisototal[]): Observable<any> {
    var headers = this.headers;

    const ids = permisoTotal.map(objeto => objeto.id_permiso_total);

    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/permisototal/eliminar`, ids, { headers }).pipe(
        tap((response) => {
          console.log(response.message);
          return
          this._snackBar.open(`${response.message}\n
          creados: ${response.data.creados}\n
          existentes: ${response.data.existentes}\n
          total: ${response.data.total}`, 'ok', {
            horizontalPosition: 'start',
            verticalPosition: 'bottom',
            duration: 10 * 1000,
          });

        }),
        catchError(error => {
          console.error('Error al listar permisoTotal:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }


  desactivar(permisoTotal:Permisototal[]): Observable<any> {
    var headers = this.headers;
    const ids = permisoTotal.map(objeto => objeto.id_permiso_total);
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/permisototal/desactivar`, ids, { headers }).pipe(
        tap((response) => {
          console.log(response.message);
        }),
        catchError(error => {
          console.error('Error al listar permisoTotal:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }


  activar(permisoTotal:Permisototal[]): Observable<any> {
    var headers = this.headers;

    const ids = permisoTotal.map(objeto => objeto.id_permiso_total);

    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/permisototal/activar`, ids, { headers }).pipe(
        tap((response) => {
          console.log(response.message);
        }),
        catchError(error => {
          console.error('Error al listar permisoTotal:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }


}
