import { Permisototal } from './../../interfaces/permiso-total.interface';
import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import {AsyncPipe} from '@angular/common';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { ComboSupermoduloComponent } from "../../../adminsitrador/maestras/supermodulos/components/combo-supermodulo/combo-supermodulo.component";
import { ComboAlmacenComponent } from "../../../adminsitrador/maestras/almacenes/components/combo-almacen/combo-almacen.component";
import { ComboModuloComponent } from "../../../adminsitrador/maestras/modulos/components/combo-modulo/combo-modulo.component";
import { Supermodulo } from '../../../adminsitrador/maestras/supermodulos/interfaces/Supermodulo.interface';
import { ComboSubmoduloComponent } from "../../../adminsitrador/maestras/submodulos/components/combo-submodulo/combo-submodulo.component";
import { ComboPermisoComponent } from '../../../adminsitrador/maestras/permisos/components/combo-permiso/combo-permiso.component';
import { FormValidationService } from '../../validators/form-validation.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PermisoTotalService } from '../../services/permiso-total.service';
import { ActivatedRoute } from '@angular/router';
import { UpdateListService } from '../../../shared/services/update-list.service';

@Component({
    selector: 'app-modal-form-permiso-total',
    templateUrl: './modal-form-permiso-total.component.html',
    styleUrl: './modal-form-permiso-total.component.css',
    imports: [
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatCardModule,
        MatButtonModule,
        ReactiveFormsModule, CommonModule,
        FormsModule, MatAutocompleteModule,
        ComboSupermoduloComponent, ComboAlmacenComponent, ComboModuloComponent,
        ComboSubmoduloComponent, ComboPermisoComponent
    ]
})
export class ModalFormPermisoTotalComponent  implements OnInit {
  selectedSupermodulo!: any;
  selectedModulo!: any;
  selectedSubmodulo!: any;
  selectedPermiso!: any;
  selectedAlmacen!: any;
  @Output() crearConfirmado: EventEmitter<boolean> = new EventEmitter<boolean>();
  formularioPermisoTotal!: FormGroup;
  id_usuario:number;



  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<ModalFormPermisoTotalComponent>,
    private permisototalService: PermisoTotalService,
    private formValidationService: FormValidationService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private activateRoute: ActivatedRoute,
    private updateListService: UpdateListService
  ){

    this.id_usuario =this.data.id_usuario;

    this.selectedAlmacen = data?.id_almacen;

    this.formularioPermisoTotal = this.fb.group({
      almacen:[null],
      supermodulo: [null],
      modulo: [null],
      submodulo: [null],
      permiso: [null],
    });

if (this.data.permisototalEditar) {
  this.onAlmacenSeleccionado(this.data.permisototalEditar.almacen)
  this.onSupermoduloSeleccionado(this.data.permisototalEditar.supermodulo)
  this.onModuloSeleccionado(this.data.permisototalEditar.modulo)
  this.onSubmoduloSeleccionado(this.data.permisototalEditar.submodulo)
  this.onPermisoSeleccionado(this.data.permisototalEditar.permiso)
}


  }

  OnInit(){

  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  esCampoValido( field: string ): boolean | null {

    return this.formValidationService.esCampoValido( this.formularioPermisoTotal,field);
  }

  mensajeError( field: string ): string | null {

    return this.formValidationService.mensajeError(this.formularioPermisoTotal,field);
  }

  guardarPermiso(){

    if (this.formularioPermisoTotal.valid) {

      const PermisoTotalForm = this.formularioPermisoTotal.value;

       this.permisototalService.crear(PermisoTotalForm,this.id_usuario).subscribe(
         (response)=>{
           if (response.success) {
             this.confirmarCreacion();
           }
         })
        this.updateListService.emitUpdateEvent();
        this.dialogRef.close(true);


      // Aquí puedes enviar los datos del formulario a través de un servicio o emitir un evento
      this.crearConfirmado.emit(true); // Emitir evento de confirmación
    } else {
      console.log('Formulario inválido. Debe completar todos los campos requeridos.');
    }

  }

  confirmarCreacion(): void {
    this.crearConfirmado.emit(true); // Emitir true para confirmar eliminación
    this.dialogRef.close(); // Cerrar el diálogo
  }

  cancelarCreacion(): void {
    this.crearConfirmado.emit(false); // Emitir false para cancelar eliminación
    this.dialogRef.close(); // Cerrar el diálogo
  }


  onSupermoduloSeleccionado(supermodulo: any): void {
    this.selectedSupermodulo = supermodulo.id_supermodulo;
    /***********************************************/
    this.formularioPermisoTotal.get('supermodulo')?.setValue(this.selectedSupermodulo);
    this.formularioPermisoTotal.patchValue({
      supermodulo: this.selectedSupermodulo
    });
    /***********************************************/
    this.selectedModulo = null;
    this.selectedSubmodulo = null;

    this.formularioPermisoTotal.patchValue({ modulo: null, submodulo: null });
  }

  onModuloSeleccionado(modulo: any): void {
    console.log(modulo,"onModuloSeleccionado");
    if (modulo==null) {
      this.selectedModulo = null
    } else {
      this.selectedModulo = modulo.id_modulo;
    }
    /***********************************************/
    this.formularioPermisoTotal.get('modulo')?.setValue(this.selectedModulo);
    this.formularioPermisoTotal.patchValue({
      modulo: this.selectedModulo
    });
    /***********************************************/

    this.selectedSubmodulo = null; // Reiniciar selección de submódulo
    this.formularioPermisoTotal.patchValue({ submodulo: null }); // Reiniciar valor en el formulario
  }

  onSubmoduloSeleccionado(submodulo: any): void {
    if (submodulo==null) {
      this.selectedSubmodulo = null
    } else {
      this.selectedSubmodulo = submodulo.id_submodulo;
    }

    /***********************************************/
    this.formularioPermisoTotal.get('submodulo')?.setValue(this.selectedSubmodulo);
    this.formularioPermisoTotal.patchValue({
      submodulo: this.selectedSubmodulo
    });
    /***********************************************/
  }

  onPermisoSeleccionado(permiso: any): void {
    if (permiso==null) {
      this.selectedPermiso = null
    } else {
      this.selectedPermiso = permiso;
    }

    /***********************************************/
    this.formularioPermisoTotal.get('permiso')?.setValue(this.selectedPermiso);
    this.formularioPermisoTotal.patchValue({
      permiso: this.selectedPermiso
    });
    /***********************************************/
  }

  onAlmacenSeleccionado(almacen: any): void {
    console.log(almacen);
    this.selectedAlmacen = almacen;
    /***********************************************/
    this.formularioPermisoTotal.get('almacen')?.setValue(this.selectedAlmacen);
    this.formularioPermisoTotal.patchValue({
      almacen: this.selectedAlmacen
    });
    /***********************************************/
  }

  ngOnInit(): void {
  }
}
