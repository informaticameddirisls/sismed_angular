import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-mi-cuenta-usuario-perfil',
    imports: [RouterOutlet],
    templateUrl: './layout-mi-cuenta-usuario-perfil.component.html',
    styleUrl: './layout-mi-cuenta-usuario-perfil.component.css'
})
export class LayoutMiCuentaUsuarioPerfilComponent {

}
