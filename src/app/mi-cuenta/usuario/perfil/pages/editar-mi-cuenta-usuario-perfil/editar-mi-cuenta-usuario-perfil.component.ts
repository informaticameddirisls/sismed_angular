import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { DataService } from '../../../../../shared/services/data.service';
import { FormValidationService } from '../../../../../usuarios/validators/form-validation.service';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UsuarioService } from '../../../../../usuarios/services/usuario.service';
import { Usuario } from '../../../../../usuarios/interfaces/Usuario.interface';
import { MatTabsModule } from '@angular/material/tabs';
import { TablaPermisosComponent } from "../../components/tabla-permisos/tabla-permisos.component";

@Component({
    selector: 'app-mi-cuenta-usuario-perfil',
    imports: [CommonModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule, MatTabsModule, TablaPermisosComponent],
    templateUrl: './editar-mi-cuenta-usuario-perfil.component.html',
    styleUrl: './editar-mi-cuenta-usuario-perfil.component.css'
})
export class EditarMiCuentaUsuarioPerfilComponent {
  titulo: string = 'PERFIL';


  @Output() tituloEnviado = new EventEmitter<string>();
  id_usuario = 0;
  imagePreview: string | ArrayBuffer | null = null;
  formularioUsuario: FormGroup;

  constructor(
    private dataService: DataService,
    private fb: FormBuilder,
    private formValidationService: FormValidationService,
    private dialog: MatDialog,
    private router: Router,
    private usuarioService: UsuarioService
  ){
    this.formularioUsuario = this.fb.group({
    id_usuario: [0, Validators.required],
    nombre: ['', Validators.required],
    ape_paterno: ['', Validators.required],
    ape_materno: ['', Validators.required],
    password: ['', Validators.required],
    correo_electronico: ['', [Validators.required, Validators.email]],
    image: [null]  // Cambiar Validators.required si no es obligatorio
  });
}

  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);
  }

  onSubmit() {
    if (this.formularioUsuario.valid) {
      // Procesar el formulario si es válido
      const usuario: Usuario = this.formularioUsuario.value;

      this.usuarioService.crear(usuario).subscribe(
        (response)=>{
          if (response.success) {
            this.router.navigate(['sismed', 'administrador', 'maestra', 'usuario', 'editar', response.data["id_usuario"]]);
          }
        }
      )
      console.log('Datos del formulario:', usuario);
      // Llamar al servicio para guardar el usuario
      // this.usuarioService.guardarUsuario(usuario).subscribe(...);
    } else {
      // Marcar todos los campos como tocados para mostrar errores
      this.formularioUsuario.markAllAsTouched();
    }
  }

  onFileChange(event: Event) {
    const input = event.target as HTMLInputElement;
    if (input.files && input.files.length > 0) {
      const file = input.files[0];
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent<FileReader>) => {
        this.imagePreview = e.target?.result ?? null;
      };
      reader.readAsDataURL(file);

      // Actualizar el valor del formulario
      this.formularioUsuario.patchValue({
        image: file
      });
      this.formularioUsuario.get('image')?.updateValueAndValidity();
    }
  }

  esCampoValido( field: string ): boolean | null {
    return this.formValidationService.esCampoValido( this.formularioUsuario,field);
  }

  mensajeError( field: string ): string | null {
    return this.formValidationService.mensajeError(this.formularioUsuario,field);
  }
  mostrarError(field: string): boolean | null {
    if ( !this.formularioUsuario.controls[field] ) return false;
    return this.formValidationService.esCampoValido(this.formularioUsuario, field);
  }

  regresar() {
    this.router.navigate(['sismed', 'administrador', 'maestra', 'usuario', 'listar']);
  }
}
