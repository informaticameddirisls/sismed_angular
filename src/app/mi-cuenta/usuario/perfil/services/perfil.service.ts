import { Injectable } from '@angular/core';
import { environments } from '../../../../../environments/environments';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, tap, throwError } from 'rxjs';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
@Injectable({
  providedIn: 'root'
})
export class PerfilService {
  private baseUrl = environments.baseUrl;
  private token = localStorage.getItem('token');
  private headers = new HttpHeaders;


  constructor(private http: HttpClient,
    private _snackBar: MatSnackBar
  ) {
    if (this.token) {
      // Encabezados de solicitud HTTP con el token
      this.headers = new HttpHeaders({
        'Authorization': `Bearer ${this.token}`
      });
    }
   }

   verPerfil(): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/mi_cuenta/usuario/perfil/listar`, { headers }).pipe(
        tap((usuario) => {
        }),
        catchError(error => {
          console.error('Error al listar usuario:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  crear(usuario:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/usuario/crear`, usuario , { headers }).pipe(
        tap((response) => {
          if (response.success) {
            this._snackBar.open(response.message, 'ok', {
              horizontalPosition: 'start',
              verticalPosition: 'bottom',
              duration: 5 * 1000,
            });
          }

        }),
        catchError(error => {
          console.error('Error al crear usuario:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  buscar(id_usuario:number): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/usuario/buscar/${id_usuario}`, { headers }).pipe(
        tap((response) => {
        }),
        catchError(error => {
          console.error('Error al buscar usuario:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  editar(usuario:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.put<any>(`${this.baseUrl}/api/usuario/editar/${usuario.id_usuario}`, usuario , { headers }).pipe(
        tap((response) => {
          if (response.success) {
            this._snackBar.open(response.message, 'ok', {
              horizontalPosition: 'start',
              verticalPosition: 'bottom',
              duration: 5 * 1000,
            });
          }

        }),
        catchError(error => {
          console.error('Error al editar usuario:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

}
