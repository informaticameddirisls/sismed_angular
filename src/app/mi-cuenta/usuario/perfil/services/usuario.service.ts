import { Injectable } from '@angular/core';
import { environments } from '../../../../../environments/environments';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, map, tap, throwError } from 'rxjs';
import { Usuario } from '../interfaces/Usuario.interface';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private baseUrl = environments.baseUrl;
  private imagenes = environments.imagenes;
  private token = localStorage.getItem('token');
  private headers = new HttpHeaders;


  constructor(private http: HttpClient,
    private _snackBar: MatSnackBar
  ) {
    if (this.token) {
      // Encabezados de solicitud HTTP con el token
      this.headers = new HttpHeaders({
        'Authorization': `Bearer ${this.token}`
      });
    }
   }


  buscarme(): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/mi_cuenta/usuario/perfil/buscarme`, { headers }).pipe(
        map((response) => {
          if (response.success) {
             response.data.imagen_perfil = `${this.baseUrl}/storage/${response.data.imagen_perfil}`
          }
          return response;
          //console.log(response,"responseresponseresponse");
        }),
        catchError(error => {
          console.error('Error al buscar usuario:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  actualizarme(usuario:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/mi_cuenta/usuario/perfil/actualizarme`, usuario , { headers }).pipe(
        tap((response) => {
          if (response.success) {
            this._snackBar.open(response.message, 'ok', {
              horizontalPosition: 'start',
              verticalPosition: 'bottom',
              duration: 5 * 1000,
            });
          }

        }),
        catchError(error => {
          console.error('Error al editar usuario:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

}
