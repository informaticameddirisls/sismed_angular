import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';
import { ListarMiCuentaUsuarioPerfilComponent } from './pages/listar-mi-cuenta-usuario-perfil/listar-mi-cuenta-usuario-perfil.component';
import { EditarMiCuentaUsuarioPerfilComponent } from './pages/editar-mi-cuenta-usuario-perfil/editar-mi-cuenta-usuario-perfil.component';

//import { ListarEstupefacienteIiaPageComponent } from './pages/listar-estupefaciente-iia-page/listar-estupefaciente-iia-page.component';

const routes: Routes = [
  {

    path: 'perfil',
    children:
      [
        {
          path: 'listar',
          component: ListarMiCuentaUsuarioPerfilComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'editar/:id',
          component: EditarMiCuentaUsuarioPerfilComponent,
          canActivate: [AuthGuard]
        }
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MiCuentaUsuarioPerfilRoutingModule { }
