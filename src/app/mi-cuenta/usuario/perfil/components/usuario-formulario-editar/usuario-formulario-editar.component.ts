import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { DataService } from '../../../../../shared/services/data.service';
import { FormValidationService } from '../../validators/form-validation.service';
import { Router } from '@angular/router';
import { UsuarioService } from '../../services/usuario.service';
import { MatSelectModule } from '@angular/material/select';
import { ComboDocumentoIdentidadService } from '../../../../../shared/services/combo/combo-documento-identidad.service';
import { catchError, map, Observable } from 'rxjs';
import { MatCheckboxModule } from '@angular/material/checkbox';

@Component({
    selector: 'app-usuario-formulario-editar',
    imports: [CommonModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatCheckboxModule],
    templateUrl: './usuario-formulario-editar.component.html',
    styleUrl: './usuario-formulario-editar.component.css'
})
export class UsuarioFormularioEditarComponent {

  formularioUsuario: FormGroup;
  comboDocumentoIdentidad$: Observable<any[]>;
  previewUrl: string | ArrayBuffer | null = null;
  isReadonly = false;
  @Input() id_usuario!: number;

  //titulo para la pagina
  @Output() tituloEnviado = new EventEmitter<string>();

  constructor(
    private dataService: DataService,
    private fb: FormBuilder,
    private formValidationService: FormValidationService,
    private router: Router,
    private usuarioService: UsuarioService,
    private comboDocumentoIdentidadService: ComboDocumentoIdentidadService
  ){
    this.formularioUsuario = this.fb.group({
      id_usuario: [0, Validators.required],
      nombres: ['', Validators.required],
      ape_paterno: ['', Validators.required],
      ape_materno: ['', Validators.required],
      id_tipo_documento: [''],
      numero_documento: [''],
      sexo: [''],
      fecha_nac: [''],
      correo_electronico: ['', [Validators.required, Validators.email]],
      direccion: [''],
      celular: [''],
      password: [''],
      imagen_perfil: [null]
    });

    // Initialize comboDocumentoIdentidad$ as an Observable
    this.comboDocumentoIdentidad$ = this.comboDocumentoIdentidadService.listarDocIdentidad().pipe(
      map(response => response.resultado) // Ajusta según la estructura de tu respuesta
    );
  }


  ngOnInit(): void {
    this.buscarUsuario();
    this.formularioUsuario.get('id_tipo_documento')?.disable();
  }
  // Método para obtener los controles del formulario
  get f() {
    return this.formularioUsuario.controls;
  }

  onFileSelected(event: any): void {
    const file = event.target.files[0];
    if (file) {
      this.formularioUsuario.patchValue({
        imagen_perfil: file // Almacena el archivo en el formulario
      });

      // Mostrar vista previa si es necesario
      const reader = new FileReader();
      reader.onload = () => {
        this.previewUrl = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  removeImage(): void {
    this.formularioUsuario.patchValue({
      imagen_perfil: null
    });
    this.previewUrl = null; // Restablece la vista previa
  }

  onSubmit(): void {
    if (this.formularioUsuario.valid) {
      const formData = new FormData();

      // Añadir los campos del formulario a FormData
      Object.keys(this.formularioUsuario.controls).forEach(key => {
        const value = this.formularioUsuario.get(key)?.value;
        if (key === 'imagen_perfil' && value) {
          formData.append('imagen_perfil', value); // Añadir archivo
        } else {
          formData.append(key, value); // Añadir otros campos
        }
      });

      // Enviar FormData al backend
      this.usuarioService.actualizarme(formData).subscribe(
        (response) => {
          if (response.success) {
            console.log(response);
            //this.router.navigate(['sismed', 'administrador', 'maestra', 'usuario', 'editar', response.data["id_usuario"]]);
          }
        },
        (error) => {
          console.error('Error al enviar los datos:', error);
        }
      );
    } else {
      this.formularioUsuario.markAllAsTouched();
    }
  }



  buscarUsuario(){
    this.usuarioService.buscarme().subscribe(
      response => {
        this.formularioUsuario.patchValue({
          id_usuario: response.data.id_usuario,
          nombres: response.data.persona.nombres,
          ape_paterno: response.data.persona.ape_paterno,
          ape_materno: response.data.persona.ape_materno,
          id_tipo_documento: response.data.persona.id_tipo_documento,
          numero_documento: response.data.persona.numero_documento,
          sexo: response.data.persona.sexo,
          fecha_nac: response.data.persona.fecha_nac,
          correo_electronico: response.data.correo_electronico,
          direccion: response.data.persona.direccion,
          celular: response.data.persona.celular,
          imagen_perfil: response.data.imagen_perfil,
          password: '',
        });
        this.previewUrl = response.data.imagen_perfil;
      },
      error => {
        console.error('Error fetching user', error);
      }
    );
  }

    esCampoValido(field: string): boolean | null {
      return this.formValidationService.esCampoValido(this.formularioUsuario, field);
    }

    mensajeError(field: string): string | null {
      return this.formValidationService.mensajeError(this.formularioUsuario, field);
    }

    // Método para validar y mostrar el mensaje de error
    mostrarError(field: string): boolean | null {
      if (!this.formularioUsuario.controls[field]) return false;
      return this.formValidationService.esCampoValido(this.formularioUsuario, field);
    }


    regresar() {
      this.router.navigate(['sismed', 'administrador', 'maestra', 'usuario', 'listar']);
    }


}
