import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';
import { MiCuentaUsuarioPerfilRoutingModule } from './mi-cuenta-usuario-perfil-routing.module';
import { BrowserModule } from '@angular/platform-browser';



@NgModule({ declarations: [], imports: [MiCuentaUsuarioPerfilRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class MiCuentaUsuarioPerfilModule { }
