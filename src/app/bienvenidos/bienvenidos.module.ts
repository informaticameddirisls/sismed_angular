import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../material/material.module';
import { BienvenidosRoutingModule } from './bienvenidos-routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MaterialModule,
    BienvenidosRoutingModule],
    providers: [provideHttpClient(withInterceptorsFromDi())] })
export class BienvenidosModule { }
