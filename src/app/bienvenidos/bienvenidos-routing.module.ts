import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth/guards/auth.guard';
import { ListarBienvenidosPageComponent } from './pages/listar-bienvenidos-page/listar-bienvenidos-page.component';

const routes: Routes = [
  {

    path: 'paginas',
    children:
      [
        {
          path: 'bienvenidos',
          component: ListarBienvenidosPageComponent,

        }
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BienvenidosRoutingModule { }
