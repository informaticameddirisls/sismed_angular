import { ChangeDetectorRef, Component, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { ChartModule } from 'primeng/chart';
import { GraficoIciDiarioComponent } from "../grafico-ici-diario/grafico-ici-diario.component";
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';


@Component({
    selector: 'app-modal-ici-diario-form',
    templateUrl: './modal-ici-diario-form.component.html',
    styleUrls: ['./modal-ici-diario-form.component.css'],
    imports: [MatFormFieldModule, MatInputModule, MatIconModule, MatCardModule, MatButtonModule, ReactiveFormsModule, CommonModule, MatTableModule,
        MatPaginatorModule, ChartModule, GraficoIciDiarioComponent, MatProgressSpinnerModule]
})
export class ModalIciDiarioComponent implements OnInit {
  formularioModulo!: FormGroup;
  selectedSupermodulo: any;
  chartOptions: any;
  resumen: any;
  fecha: any;
  cantidad: any;
  @Output() crearConfirmado: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  iciColumnas1: string[] = ['Sobrestock','Normostock','Substock','Desabastecido','Nivel'];
  iciColumnas2: string[] = ['id_almacen','almacen','total','sobrestock','sobrestockporc','normostock','normostockporc','substock','substockporc','desabastecido','desabastecidoporc','disponibilidad','nivel'];
  isLoadingResults = true;
  dataSource = new MatTableDataSource<any>([]);


  constructor(
    public dialogRef: MatDialogRef<ModalIciDiarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private cdref: ChangeDetectorRef
  ){
    console.log(this.data, "this.data");
    this.chartOptions = this.data.consolidado || {}; // Asegúrate de obtener solo chartOptions
    this.resumen = this.data.consolidado;
    this.fecha = this.data.fecha;
    this.cantidad = this.data.cantidad;
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.data.conteoPorAlmacen);
    console.log(this.data.conteoPorAlmacen, "this.data");
    console.log(this.chartOptions, "chartOptions antes de asignar a dataSource");
    this.dataSource.paginator = this.paginator;
    //this.cdref.detectChanges();
  }


}
