import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../../shared/services/data.service';
import { MatDialog } from '@angular/material/dialog';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment} from 'moment';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
    selector: 'app-listar-bienvenidos-page',
    imports: [

    ],
    templateUrl: './listar-bienvenidos-page.component.html',
    styleUrl: './listar-bienvenidos-page.component.css',
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-PE' },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    ]
})
export class ListarBienvenidosPageComponent  implements OnInit {
  titulo: string = 'BIENVENIDOS AL REDISPROD V 2.0';




  constructor(
    private dataService: DataService,
    private dialog: MatDialog,
    private cdr: ChangeDetectorRef
  ){


  }

  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);

  }





}
