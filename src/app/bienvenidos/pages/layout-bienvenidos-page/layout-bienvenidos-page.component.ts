import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-bienvenidos-page',
    imports: [RouterOutlet],
    templateUrl: './layout-bienvenidos-page.component.html',
    styleUrl: './layout-bienvenidos-page.component.css'
})
export class LayoutBienvenidosPageComponent {

}
