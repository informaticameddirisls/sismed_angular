import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, tap, of, map, catchError } from 'rxjs';
import { environments } from '../../../environments/environments';
import { Usuario } from '../interfaces/Usuario.interface';
import { isPlatformBrowser } from '@angular/common';
import { ErrorRedirectService } from './error-redirect.service';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthService {

  private baseUrl = environments.baseUrl;
  private usuario?: Usuario | null = null;
  private isAuthenticated: boolean = false;
  private usuarioLog: any;
  private isBrowser: boolean;

  constructor(private http: HttpClient,
    @Inject(PLATFORM_ID) private platformId: Object,
    private router: Router,
    private errorRedirectService: ErrorRedirectService,
  ) {
    this.isBrowser = isPlatformBrowser(this.platformId); // Verifica si es el navegador
  }

  get currentUser(): Usuario | null {
    if (this.isBrowser && typeof localStorage !== 'undefined') {
      const usuarioLog = localStorage.getItem('usuario');
      return usuarioLog ? JSON.parse(usuarioLog) : null;
    } else {
      return null;
    }
  }

  get autenticado(): boolean {
    return this.isAuthenticated;
  }

  setAutenticado(autenticado: boolean): void {
    this.isAuthenticated = autenticado;
  }

  setUsuario(usuario: Usuario | null): void {
    this.usuario = usuario;
  }

  login(correo_electronico: string, password: string): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/api/login`, { correo_electronico, password })
      .pipe(
        tap((usuario) => {
          console.log(usuario);
          if (usuario && usuario.usuario && usuario.usuario.imagen_perfil) {
            usuario.usuario.imagen_perfil = `${this.baseUrl}/storage/${usuario.usuario.imagen_perfil}`;
          } else {
            console.warn('La propiedad imagen_perfil no está definida en el objeto usuario:', usuario);
          }
        }),
        tap(usuario => this.setUsuario(usuario)),
        tap(usuario => localStorage.setItem('usuario', JSON.stringify(usuario.usuario))),
        tap(usuario => localStorage.setItem('token', usuario.token)),
      );
  }

  guardarTokenEnSesion(): Observable<any> {
    if (this.isBrowser) {
      const token = localStorage.getItem('token');
      return this.http.post<any>(`${this.baseUrl}/api/guardartoken`, { token })
        .pipe(
          tap(response => {
            this.isAuthenticated = response.status === 'success';
          })
        );
    }
    return of(null);
  }

  checkAuthentication(url?: string): Observable<any> {
    if (this.isBrowser && typeof localStorage !== 'undefined' && localStorage !== null) {
      const token = localStorage.getItem('token');
      if (!token) {
        this.usuario = undefined;
        this.isAuthenticated = false;
        return of({ "success": false, error: "1", code: "405", mensaje: "No se encuentra el token" });
      }
      const currentRoute = this.router.url;
      return this.http.post<any>(`${this.baseUrl}/api/checkauth`, { token, route: url }).pipe(
        map(authenticated => {
          this.isAuthenticated = authenticated.success;
          this.usuario = authenticated.usuario;
          return { success: authenticated.success, error: "", mensaje: authenticated.error, code: authenticated.code };
        }),
        catchError(error => {
          console.error('Error en la verificación de autenticación:', error);
          this.usuario = undefined;
          this.isAuthenticated = false;
          return of({ success: false, error: "405", mensaje: "Error no especificado" });
        })
      );
    } else {
      this.usuario = undefined;
      this.isAuthenticated = false;
      return of({ "success": false, error: "405", mensaje: "Error en el localstorage" });
    }
  }

  logout() {
    this.usuario = undefined;
    this.isAuthenticated = false;
    if (this.isBrowser) {
      localStorage.clear();
    }
    this.router.navigate(['/login']);
  }
}
