import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ErrorRedirectService {

  constructor(private router: Router) { }

  redirectToErrorPage(): void {
    this.router.navigate(['/acceso-denegado']); // Aquí ajusta la ruta a tu vista de acceso denegado
  }
}
