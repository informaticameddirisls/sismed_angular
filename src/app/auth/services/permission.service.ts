import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environments } from '../../../environments/environments';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {
  private baseUrl = environments.baseUrl;
  constructor(private http: HttpClient) { }

  checkPermissions(token: string, route: string): Observable<boolean> {
    // Lógica para consultar la base de datos y verificar permisos para la ruta especificada
    return this.http.post<boolean>(`${this.baseUrl}/api/permissions/check`, { token, route });
  }
}
