import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-auth-page',
    imports: [RouterOutlet],
    templateUrl: './layout-auth-page.component.html'
})
export class LayoutAuthPageComponent {

}
