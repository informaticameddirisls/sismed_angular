import { HttpClientModule } from '@angular/common/http';
import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'auth-layout-page',
  standalone: true,
  imports: [RouterOutlet],
  templateUrl: './layout-page.component.html'
})
export class LayoutPageComponent {

}
