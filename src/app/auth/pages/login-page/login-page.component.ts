import { Component } from '@angular/core';
//import { AuthService } from '../../services/auth.service';
import { Router, RouterLink, RouterModule, RouterOutlet } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
@Component({
    selector: 'auth-login-page',
    imports: [
        // TODO: `HttpClientModule` should not be imported into a component directly.
        // Please refactor the code to add `provideHttpClient()` call to the provider list in the
        // application bootstrap logic and remove the `HttpClientModule` import from this component.
        HttpClientModule, ReactiveFormsModule, CommonModule, RouterModule
    ],
    templateUrl: './login-page.component.html',
    styleUrl: './login-page.component.css',
    providers: [AuthService, Router, FormBuilder]
})
export class LoginPageComponent {
  loginForm!: FormGroup;

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder
  ){
    this.loginForm = this.fb.group({
      correo_electronico: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    })

  }

  onLogin(): void {
    if (this.loginForm.valid) {
      this.authService.login(this.loginForm.value.correo_electronico, this.loginForm.value.password).subscribe(
          (user) => {
            console.log('Respuesta del servicio:', user);
            this.guardarTokenEnSesion();
        },
        (err) => {
          console.log(err.error.error.message);
        }
      );
    }
  }


  guardarTokenEnSesion(): void {
    this.authService.guardarTokenEnSesion().subscribe(
      // Manejar la respuesta del servicio después de guardar el token en la sesión
      response => {
        //console.log('Token guardado en la sesión:22', response);
        this.router.navigate(['sismed', 'administrador', 'maestra', 'supermodulo', 'listar']);
        window.location.reload();
        //window.location.href = '/sismed/administrador/maestra/supermodulo/listar';
      },
      error => {
        console.error('Error al guardar el token en la sesión:', error);
        // Puedes mostrar mensajes de error al usuario o tomar otras acciones necesarias
      }
    );
  }

}
