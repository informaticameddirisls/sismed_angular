import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { LayoutPageComponent } from './pages/layout-page/layout-page.component';
import { PublicGuard } from './guards/public.guard';

const routes: Routes = [
  { path: 'login',
  component: LoginPageComponent,
  //canActivate: [ PublicGuard ],
  //canMatch: [ PublicGuard ]
  },
  { path: 'layout', component: LayoutPageComponent },
  //{ path: '**', redirectTo: 'login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
