import { Injectable } from '@angular/core';
import { CanMatch, CanActivate, Router, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, UrlTree } from '@angular/router';
import { Observable, tap, map, of, switchMap } from 'rxjs';
import { AuthService } from '../services/auth.service';


@Injectable({providedIn: 'root'})
export class PublicGuard implements  CanActivate  {

  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }


  private checkAuthStatus(url: string): Observable<boolean> {
    return this.authService.checkAuthentication(url).pipe(
      map(authResult => {

        //return true;
        if (authResult.success==false) {
          //this.router.navigate(['sismed', 'auth', 'login']);
          return true; // No está autenticado, navega a la página de inicio de sesión
        } else {
          //this.router.navigate(['sismed', 'administrador', 'maestra','supermodulo','listar']);
          //this.router.navigate(['sismed', 'consulta', 'stock','medicamento', 'listar']);
          this.router.navigate(['sismed', 'bienvenidos', 'paginas','bienvenidos']);
          //http://localhost:4200/sismed/bienvenidos/paginas/bienvenidos
          return false; // Está autenticado, permite el acceso a la ruta solicitada
        }
      })
    );
  }


  // canMatch(route: Route, segments: UrlSegment[]): boolean | Observable<boolean> {
  //   console.log('Can Match');
  //   // console.log({ route, segments })
  //   return this.checkAuthStatus();
  // }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> {
     //console.log('Can Activate public',route,state);

     const currentRoute = route.routeConfig?.path || '';
     //console.log('Ruta actual desde el guard public:', currentRoute);

    return this.checkAuthStatus(state.url);
  }

}
