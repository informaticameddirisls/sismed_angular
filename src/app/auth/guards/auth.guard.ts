import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanMatch, Route, UrlSegment, UrlTree, RouterStateSnapshot, Router, CanActivateChild } from '@angular/router';
import { Observable, take, tap } from 'rxjs';

import { AuthService } from '../services/auth.service';
import { isPlatformBrowser } from '@angular/common';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanMatch, CanActivate, CanActivateChild {

  constructor(
    private authService: AuthService,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: Object
  ) { }

  private checkAuthStatus(url?: string): any | Observable<any> {

    if (isPlatformBrowser(this.platformId) && typeof localStorage !== 'undefined' && localStorage !== null) {

      return this.authService.checkAuthentication(url)
        .pipe(
          tap(isAuthenticated => {
            switch (isAuthenticated.code) {
              case "200":
                return true;
              case "401":
              case "403":
              case "404":
              case "405":
              default:
                this.router.navigate(['sismed', 'auth', 'login']);
                return false;
            }
          }),
          take(1)
        );
    } else {
      return false;
    }
  }


  canMatch(route: Route, segments: UrlSegment[]): boolean | Observable<boolean> {
    // console.log('Can Match');
    // console.log({ route, segments })
    return this.checkAuthStatus();
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> {
    //console.log(state.url)
    //console.log('Can Activate auth');
    const currentRoute = route.routeConfig?.path || '';
    //console.log('Ruta actual desde el guard auth:', currentRoute);
    return this.checkAuthStatus(state.url);
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> {
    return this.checkAuthStatus();
  }

}
