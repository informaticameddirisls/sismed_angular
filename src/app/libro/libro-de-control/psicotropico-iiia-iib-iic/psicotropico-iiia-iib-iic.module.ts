import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';
import { PsicotropicoIiiaIibIicRoutingModule } from './psicotropico-iiia-iib-iic-routing.module';



@NgModule({ declarations: [], imports: [CommonModule,
        PsicotropicoIiiaIibIicRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class PsicotropicoIiiaIibIicModule { }
