import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-layout-psicotropico-iiia-iib-iic-page',
  standalone: true,
  imports: [RouterOutlet],
  templateUrl: './layout-psicotropico-iiia-iib-iic-page.component.html',
  styleUrl: './layout-psicotropico-iiia-iib-iic-page.component.css'
})
export class LayoutPsicotropicoIiiaIibIicPageComponent {

}
