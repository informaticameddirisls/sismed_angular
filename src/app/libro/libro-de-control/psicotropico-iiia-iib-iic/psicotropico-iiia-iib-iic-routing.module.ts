import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';

import { ListarPsicotropicoIiiaIibIicPageComponent } from './pages/listar-psicotropico-iiia-iib-iic-page/listar-psicotropico-iiia-iib-iic-page.component';

const routes: Routes = [
  {

    path: 'psicotropico_iiia_iib_iic',
    children:
      [
        {
          path: 'listar',
          component: ListarPsicotropicoIiiaIibIicPageComponent,
          canActivate: [AuthGuard]
        }
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PsicotropicoIiiaIibIicRoutingModule { }
