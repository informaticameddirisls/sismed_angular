import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-layout-psicotropico-lista-ivb-page',
  standalone: true,
  imports: [RouterOutlet],
  templateUrl: './layout-psicotropico-lista-ivb-page.component.html',
  styleUrl: './layout-psicotropico-lista-ivb-page.component.css'
})
export class LayoutPsicotropicoListaIvbPageComponent {

}
