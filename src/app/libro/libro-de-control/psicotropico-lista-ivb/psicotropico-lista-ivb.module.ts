import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';
import { PsicotropicoListaIvbRoutingModule } from './psicotropico-lista-ivb-routing.module';



@NgModule({ declarations: [], imports: [CommonModule,
        PsicotropicoListaIvbRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class PsicotropicoListaIvbModule { }
