import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';

import { ListarPsicotropicoListaIvbPageComponent } from './pages/listar-psicotropico-lista-ivb-page/listar-psicotropico-lista-ivb-page.component';

const routes: Routes = [
  {

    path: 'psicotropico_lista_ivb',
    children:
      [
        {
          path: 'listar',
          component: ListarPsicotropicoListaIvbPageComponent,
          canActivate: [AuthGuard]
        }
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PsicotropicoListaIvbRoutingModule { }
