import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-layout-estupefaciente-iia-page',
  standalone: true,
  imports: [RouterOutlet],
  templateUrl: './layout-estupefaciente-iia-page.component.html',
  styleUrl: './layout-estupefaciente-iia-page.component.css'
})
export class LayoutEstupefacienteIiaPageComponent {

}
