import { CommonModule, DatePipe, JsonPipe } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { ComboRisComponent } from '../../../../../adminsitrador/maestras/ris/components/combo-ris/combo-ris.component';
import { ComboAlmacenComponent } from '../../../../../adminsitrador/maestras/almacenes/components/combo-almacen/combo-almacen.component';
import { ComboMedicamentoComponent } from '../../../../../adminsitrador/maestras/medicamentos/components/combo-medicamento/combo-medicamento.component';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { ComboMaestraCategoriaComponent } from '../../../../../adminsitrador/maestras/maestra_categoria/components/combo-maestra-categoria/combo-maestra-categoria.component';
import { DataService } from '../../../../../shared/services/data.service';
import { ExcelService } from '../../../../../shared/services/excel.service';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DATE_LOCALE, provideNativeDateAdapter } from '@angular/material/core';
import { provideMomentDateAdapter } from '@angular/material-moment-adapter';
import 'moment/locale/es';
import { ComboAlmacenFiltradoComponent } from "../../../../../adminsitrador/maestras/almacenes/components/combo-almacen-filtrado/combo-almacen-filtrado.component";
import { EstupefacienteIiaService } from '../../services/estupefaciente-iia.service';
import { TablaDinamicaEstupefacienteIiaComponent } from "../../components/tabla-dinamica-estupefaciente-iia/tabla-dinamica-estupefaciente-iia.component";
import * as XLSX from 'xlsx';

@Component({
    selector: 'app-listar-estupefaciente-iia-page',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es' },
        provideMomentDateAdapter(),
    ],
    templateUrl: './listar-estupefaciente-iia-page.component.html',
    styleUrl: './listar-estupefaciente-iia-page.component.css',
    imports: [MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatExpansionModule,
        MatButtonModule,
        MatMenuModule,
        CommonModule, MatFormFieldModule, MatDatepickerModule, FormsModule, ReactiveFormsModule, ComboAlmacenFiltradoComponent, TablaDinamicaEstupefacienteIiaComponent]
})
export class ListarEstupefacienteIiaPageComponent {

  titulo: string = 'ESTUPEFACIENTE IIA';

  libroDeControl: string = 'ESTUPEFACIENTE_IIA';
  nombreSubmodulo: string = 'estupefaciente_iia';
  selectedRis!: any;
  selectedMaestraCategoria!: any;
  selectedMedicamento!: any;
  selectedAlmacen!: any;
  filtraUsuario: boolean = true;
  isLoadingResults = true;
  cabeceraVacia: boolean = true;
  displayedColumns: string[] = [];
  displayedColumns2: any[] = [];
  displayedColumns3: any[] = [];
  dataSource = new MatTableDataSource<any>([]);
  jsonTabla!: Object;


    // Referencia al componente hijo TablaDinamicaEstupefacienteIIAComponent
    @ViewChild(TablaDinamicaEstupefacienteIiaComponent) tablaComponent!: TablaDinamicaEstupefacienteIiaComponent;



  readonly range = new FormGroup({
    start: new FormControl<Date | null>(new Date()),
    end: new FormControl<Date | null>(new Date()),
  });


  constructor(
    private dataService: DataService,
    private dialog: MatDialog,
    private excelService: ExcelService,
    private estupefacienteIiaService: EstupefacienteIiaService,
    private cdr: ChangeDetectorRef
  ){}

  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);
    this.isLoadingResults = false;
  }


  onAlmacenSeleccionado(almacen: any): void {
    this.selectedAlmacen = almacen;
  }

  generarLibroControl() {
    var selectedAlmacen = this.selectedAlmacen ? this.selectedAlmacen.id_almacen : null;
    var start = this.range.value.start;
    var end = this.range.value.end;
    var libroDeControl = this.libroDeControl;
    var parametros = { selectedAlmacen, start, end, libroDeControl };

    this.estupefacienteIiaService.libro_de_control(parametros).subscribe((response) => {
      var jsonest = response.resultado;
      var headers = Object.keys(jsonest[0]);
      var jsonTotal = { data: jsonest, cabecera: headers };
      this.jsonTabla = jsonTotal; // Asignación correcta de datos a jsonTabla
      this.cdr.detectChanges();
    });
  }

  // Función para actualizar la tabla en el componente hijo
  actualizarTabla(datos: any) {
    if (this.tablaComponent) {
      this.tablaComponent.actualizarTabla(datos);
    }
  }

  exportTableToExcel(tableID:any, filename:any) {
    if (!filename) filename = 'excel_data.xlsx';

    const establecimiento = [
      `ESTABLECIMIENTO: ${this.selectedAlmacen.nombre}`
    ];
    const quimico = [
      `QUIMICO FARMACEUTICO: `
    ];
    const rango = [
      `RANGO: `
    ];

    console.log(document.getElementById(tableID));

    // Obtener la hoja de cálculo de la tabla HTML
    let ws = XLSX.utils.table_to_sheet(document.getElementById(tableID));

    // Obtener el arreglo de filas de la hoja de cálculo
    let rows = XLSX.utils.sheet_to_json(ws, { header: 1 });

    // Eliminar la primera columna de cada fila
    let rowsWithoutFirstColumn = rows.map((row:any) => row.slice(1));
    console.log(rowsWithoutFirstColumn,"11111");


    // Insertar la nueva fila al final del arreglo de filas
    rowsWithoutFirstColumn.unshift([]);
    rowsWithoutFirstColumn.unshift([]);
    rowsWithoutFirstColumn.unshift(rango);
    rowsWithoutFirstColumn.unshift([]);
    rowsWithoutFirstColumn.unshift(quimico);
    rowsWithoutFirstColumn.unshift([]);
    rowsWithoutFirstColumn.unshift(establecimiento);
    rowsWithoutFirstColumn.unshift([]);
    rowsWithoutFirstColumn.unshift([]);
    rowsWithoutFirstColumn.unshift([`LIBRO DE CONTROL ESTUPEFACIENTE IIA - DMID - DIRIS LIMA SUR`]);

    // Crear una nueva hoja de cálculo con el arreglo de filas actualizado
    let updatedSheet = XLSX.utils.aoa_to_sheet(rowsWithoutFirstColumn);



    // Crear un nuevo libro de Excel
    let wb = XLSX.utils.book_new();

    // Agregar la hoja de cálculo actualizada al libro
    XLSX.utils.book_append_sheet(wb, updatedSheet, "Sheet1");

    // Guardar el libro de Excel en un archivo
    return XLSX.writeFile(wb, filename);
  }


}
