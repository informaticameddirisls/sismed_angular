import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
    selector: 'app-tabla-dinamica-estupefaciente-iia',
    imports: [],
    templateUrl: './tabla-dinamica-estupefaciente-iia.component.html',
    styleUrl: './tabla-dinamica-estupefaciente-iia.component.css'
})
export class TablaDinamicaEstupefacienteIiaComponent implements OnChanges {
  @Input() jsonTabla: any;
  displayedColumns: string[] = [];
  dataSource: any[] = [];

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['jsonTabla'] && changes['jsonTabla'].currentValue) {
      this.actualizarTabla(changes['jsonTabla'].currentValue);
    }
  }

  actualizarTabla(jsonTabla: any): void {
    console.log(jsonTabla);
    this.displayedColumns = jsonTabla.cabecera;
    this.dataSource = jsonTabla.data;

    // Obtener el div donde se mostrará la tabla
    var tbllistado = document.getElementById("divtbllistado");
    tbllistado!.innerHTML = '';

    //tbllistado.id = "tbllistado";
    tbllistado!.classList.add("table")
    tbllistado!.classList.add("table-bordered")


    // Crear la tabla
    var tabla = document.createElement("table");

    tabla.setAttribute("id", "idPrueba");
    tabla.classList.add("tablalibrodecontrol");
    tabla.style.overflow='auto';
    tabla.style.display='block';
    // Crear la cabecera de la tabla
    var cabecera = tabla.createTHead();
    //cabecera.style.position = "sticky";
    //cabecera.style.top = "0";
    var filaCabecera = cabecera.insertRow();
    cabecera.classList.add("fs-4");

    var contador = 0;

    this.displayedColumns.forEach(campo => {
      if (contador<12) {
        var th = document.createElement("th");
        th.textContent = campo;
        th.rowSpan = 2;
        th.style.background = '#f8f5ff'
        th.style.color = '#7239ea'
        th.style.padding = '10px 20px'
        filaCabecera.appendChild(th);
      }
      if (contador>=12 && contador%2 != 0) {
        var th = document.createElement("th");
        th.textContent = campo;
        th.style.background = '#f8f5ff'
        th.style.color = '#7239ea'
        th.colSpan = 2;
         th.style.padding = '10px 20px'
        filaCabecera.appendChild(th);
      }

      contador++
    });

    // Crear la cabecera de la tabla
    var cabecera1 = tabla.createTHead();
    var filaCabecera1 = cabecera1.insertRow();
    cabecera1.classList.add("fs-4");
    var contador1=0;

    for (var campo in this.displayedColumns) {
      if (contador1>=12 && contador1%2 != 0) {
        var th = document.createElement("th");
        th.textContent = 'DEBE';
        th.style.background = '#DFFFEA'
        th.style.color = '#17c653'
        filaCabecera1.appendChild(th);
        var th = document.createElement("th");
        th.textContent = 'HABER';
        th.style.background = '#e9f3ff'
        th.style.color = '#1b84ff'
        filaCabecera1.appendChild(th);
      }

      contador1++
    }

          //console.log(contador1);

      // Crear el cuerpo de la tabla
      var cuerpo = tabla.createTBody();


      //console.log(sumatoria);
      this.dataSource.forEach(function(item) {

        var fila = cuerpo.insertRow();
        var contador = 0;
        for (var campo in item) {
            var celda = fila.insertCell();
          if (contador==4) {
            if (item[campo].startsWith("SALDO")) {
              fila.style.background = '#FFF8DD'
              //celda.style.color = '#F6C000'
            }else{
              if (item[campo].startsWith("CIERRE")) {
                fila.style.background = '#FFEEF3'
                //celda.style.color = '#f8285a'
              }else{
                fila.style.background = '#fff'
              }
            }
          }



            if (contador>=12) {
              celda.textContent = String(Math.round(item[campo]));
              celda.style.textAlign = 'center';

              if (item[campo]!=0) {
                celda.style.fontWeight = 'bold';
              }
            }else{
              celda.textContent = item[campo];
            }


            if (contador>=11) {
              //sumatoria[contador-11]=Number(sumatoria[contador-11])+Number(item[campo]);
            }
            contador++;
        }
      });



      var fila = cuerpo.insertRow();
      var celda = fila.insertCell();
      //celda.colSpan = "11";
      //celda.textContent = "SALDOS";

      //sumatoria.forEach(e => {
      //  var celda = fila.insertCell();
     //   celda.textContent = e;
      //});

            // Agregar la tabla al div
      tbllistado!.appendChild(tabla);



      //crearTabla(json);
      //console.log(tbllistado);
      return

  }
}
