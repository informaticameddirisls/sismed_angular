import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';
import { EstupefacienteIiaRoutingModule } from './estupefaciente-iia-routing.module';



@NgModule({ declarations: [], imports: [CommonModule,
        EstupefacienteIiaRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class EstupefacienteIiaModule { }
