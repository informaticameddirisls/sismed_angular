import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';
import { ListarLotesPorVencerPageComponent } from '../lotes-por-vencer/pages/listar-lotes-por-vencer-page/listar-lotes-por-vencer-page.component';
import { ListarStockMedicamentoPageComponent } from './pages/listar-stock-medicamento-page/listar-stock-medicamento-page.component';


const routes: Routes = [
  {
    path: 'medicamento',
    children:

      [{
        path: 'listar',
        component: ListarStockMedicamentoPageComponent,
        canActivate: [AuthGuard]
      }]
  }
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MedicamentoRoutingModule { }
