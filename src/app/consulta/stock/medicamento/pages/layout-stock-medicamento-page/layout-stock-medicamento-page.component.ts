import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-stock-medicamento-page',
    imports: [RouterOutlet],
    templateUrl: './layout-stock-medicamento-page.component.html',
    styleUrl: './layout-stock-medicamento-page.component.css'
})
export class LayoutStockMedicamentoPageComponent {

}
