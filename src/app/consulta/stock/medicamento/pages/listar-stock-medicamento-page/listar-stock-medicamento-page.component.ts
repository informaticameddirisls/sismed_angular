import { AfterContentChecked, AfterViewInit, ChangeDetectorRef, Component, EventEmitter, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { DataService } from '../../../../../shared/services/data.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSort, MatSortModule, SortDirection } from '@angular/material/sort';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { CommonModule, DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Observable, catchError, map, merge, startWith, switchMap, of as observableOf } from 'rxjs';
import { StockMedicamentoService } from '../../services/stock-medicamento.service';
import { MatExpansionModule } from '@angular/material/expansion';
import { ComboRisComponent } from "../../../../../adminsitrador/maestras/ris/components/combo-ris/combo-ris.component";
import { ComboAlmacenComponent } from "../../../../../adminsitrador/maestras/almacenes/components/combo-almacen/combo-almacen.component";
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import * as XLSX from 'xlsx';
import { ComboMedicamentoComponent } from "../../../../../adminsitrador/maestras/medicamentos/components/combo-medicamento/combo-medicamento.component";
import { MatIconModule } from '@angular/material/icon';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MedicamentoTablaDetalleStockComponent } from "../../components/medicamento-tabla-detalle-stock/medicamento-tabla-detalle-stock.component";
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { ComboAlmacenAutocompletadoComponent } from "../../../../../adminsitrador/maestras/almacenes/components/combo-almacen-autocompletado/combo-almacen-autocompletado.component";

@Component({
    selector: 'app-listar-stock-medicamento-page',
    templateUrl: './listar-stock-medicamento-page.component.html',
    styleUrl: './listar-stock-medicamento-page.component.css',
    animations: [
        trigger('detailExpand', [
            state('collapsed,void', style({ height: '0px', minHeight: '0' })),
            state('expanded', style({ height: '*' })),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ],
    imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatExpansionModule,
    ComboRisComponent,
    ComboAlmacenAutocompletadoComponent,
    ComboMedicamentoComponent,
    MatIconModule,
    MedicamentoTablaDetalleStockComponent,
    MatButtonModule, MatMenuModule,

    //ComboAlmacenComponent
]
})
export class ListarStockMedicamentoPageComponent implements OnInit, AfterViewInit, OnChanges, AfterContentChecked  {
  titulo: string = 'STOCK MEDICAMENTO';
  selectedRis!: any;
  selectedAlmacen!: any;
  selectedMedicamento!: any;
  detalle_stock : any;

  @Output() tituloEnviado = new EventEmitter<string>();


  constructor(
    private dataService: DataService,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private _httpClient: HttpClient,
    private stockMedicamentoService: StockMedicamentoService,
    private cd: ChangeDetectorRef
  ){}

  ngOnChanges(changes: SimpleChanges): void {
  }

  ngAfterContentChecked(): void {
    this.cd.detectChanges();
  }

  onSelectionChange(event: MatAutocompleteSelectedEvent) {}

  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);
  }

  displayedColumns: string[] = ['almacen_codigo','almacen_nombre','medicamento_codigo','medicamento_nombre','stock','stock_ultima_fecha_mov','fecha_reporte'];

  data: any[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  spinerDetallado = true;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  expandedElement1: any | null;
  columnsToDisplayWithExpand1 = [...this.displayedColumns, 'expand'];

  toggleDetail(row: any): void {
    row.expanded = !row.expanded;
  }

  isExpansionDetailRow = (i: number, row: any) => {
    row.hasOwnProperty('detailRow')
  };

  expandRow(row: any) {
    this.spinerDetallado = true;
    this.stockMedicamentoService.listarDetalle(row).subscribe((response)=>{
      this.detalle_stock = response.resultado;
      this.spinerDetallado = false;
    })
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          var campo = this.sort.active;
          var direccion = this.sort.direction;
          var pagina = this.paginator.pageIndex;
          var cantidad = this.paginator.pageSize;
          var almacen = this.selectedAlmacen;
          var medicamento = this.selectedMedicamento ? this.selectedMedicamento.id_medicamento : null;
          var parametros = {campo,direccion,pagina,almacen,cantidad,medicamento}

          return this.stockMedicamentoService.listar(parametros).
          pipe(catchError(() => observableOf(null)));
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = data === null;

          if (data === null) {
            return [];
          }
          this.resultsLength = data.total;
          data.resultado.forEach((row:any) => row.expanded = false);
          return data.resultado;
        }),
      )
      .subscribe(data => (this.data = data));
  }

  onRisSeleccionado(ris: any): void {
    if (ris == null) {
      this.selectedRis = null
      return
    }
    this.selectedRis = ris;
  }

  buscarStock(){
    const parametros = {
      campo: this.sort.active,
      direccion: this.sort.direction,
      pagina: this.paginator.pageIndex,
      cantidad: this.paginator.pageSize,
      ris: this.selectedRis ? this.selectedRis.id_tipo : null,
      almacen: this.selectedAlmacen,
      medicamento: this.selectedMedicamento ? this.selectedMedicamento.id_medicamento : null,
    };

    this.stockMedicamentoService.listar(parametros)
    .subscribe(data => {
      this.isLoadingResults = false;
      this.isRateLimitReached = data === null;

      if (data === null) {
        this.data = [];
      } else {
        this.resultsLength = data.total;
        this.data = data.resultado;
      }
    });
  }

  onAlmacenSeleccionado(almacen: any): void {
    this.selectedAlmacen = almacen;
  }

  onMedicamentoSeleccionado(medicamento: any): void {
    this.selectedMedicamento = medicamento;
  }

  generarExcel(): void {
    var fecha = new Date();
    const parametros = {
      ris: this.selectedRis ? this.selectedRis.id_tipo : null,
      almacen: this.selectedAlmacen,
      medicamento: this.selectedMedicamento ? this.selectedMedicamento.id_medicamento : null,
    };

    this.stockMedicamentoService.listar(parametros)
    .subscribe(data => {
      const workbook = XLSX.utils.book_new();
      const worksheet = XLSX.utils.json_to_sheet(data.resultado);
      XLSX.utils.book_append_sheet(workbook, worksheet, 'Stock');
      XLSX.writeFile(workbook, `Stock_medicamentos${fecha.getDate()}_${fecha.getMonth() + 1}_${fecha.getFullYear()}.xlsx`);
    });
  }

  generarDetalladoExcel(): void {
    var fecha = new Date();
    const parametros = {
      ris: this.selectedRis ? this.selectedRis.id_tipo : null,
      almacen: this.selectedAlmacen,
      medicamento: this.selectedMedicamento ? this.selectedMedicamento.id_medicamento : null,
    };

    this.stockMedicamentoService.reporteListarDetalleStockMedicamento(parametros)
    .subscribe(data => {
      const workbook = XLSX.utils.book_new();
      const worksheet = XLSX.utils.json_to_sheet(data.resultado);
      XLSX.utils.book_append_sheet(workbook, worksheet, 'Stock');
      XLSX.writeFile(workbook, `Stock_medicamentos_detalle_${fecha.getDate()}_${fecha.getMonth() + 1}_${fecha.getFullYear()}.xlsx`);
    });
  }
}
