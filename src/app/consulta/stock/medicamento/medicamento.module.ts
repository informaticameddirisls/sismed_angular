import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';
import { MedicamentoRoutingModule } from './medicamento-routing.module';



@NgModule({ declarations: [], imports: [CommonModule,
        MaterialModule,
        MedicamentoRoutingModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class MedicamentoModule { }
