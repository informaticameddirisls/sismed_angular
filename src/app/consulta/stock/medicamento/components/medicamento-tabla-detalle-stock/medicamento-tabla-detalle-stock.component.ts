import { CommonModule } from '@angular/common';
import { AfterContentChecked, AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';

@Component({
    selector: 'medicamento-tabla-detalle-stock',
    imports: [CommonModule, MatTableModule, MatProgressSpinnerModule],
    templateUrl: './medicamento-tabla-detalle-stock.component.html',
    styleUrl: './medicamento-tabla-detalle-stock.component.css'
})
export class MedicamentoTablaDetalleStockComponent implements AfterContentChecked, OnInit, OnChanges {

  @Input() medicamento_tabla_stock: any;
  @Input() detalle_stock: any;
  @Input() isLoadingResultsDetallado: boolean=true;


  displayedColumns: string[] = ['lote', 'fecha_vto', 'med_reg_san', 'tipsum','ffinan','stock','precio_op'];

  constructor(){

  }


  ngOnChanges(changes: SimpleChanges): void {
    //console.log(changes,"changeschangeschangeschanges")
  }
  ngOnInit(): void {

  }
  ngAfterContentChecked(): void {
    //console.log(this.medicamento_tabla_stock)
  }




}
