import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../../../../shared/services/data.service';
import { CommonModule, DatePipe, JsonPipe   } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatExpansionModule } from '@angular/material/expansion';
import { ComboRisComponent } from '../../../../../adminsitrador/maestras/ris/components/combo-ris/combo-ris.component';
import { ComboAlmacenComponent } from '../../../../../adminsitrador/maestras/almacenes/components/combo-almacen/combo-almacen.component';
import { ComboMedicamentoComponent } from '../../../../../adminsitrador/maestras/medicamentos/components/combo-medicamento/combo-medicamento.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule} from '@angular/material/datepicker';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import {provideMomentDateAdapter} from '@angular/material-moment-adapter';
import { MatInputModule } from '@angular/material/input';
import 'moment/locale/es';
import { LotesPorVencerService } from '../../services/lotes-por-vencer.service';
import * as moment from 'moment';
import { catchError, map, merge, of, startWith, switchMap } from 'rxjs';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import * as XLSX from 'xlsx';
import { ComboAlmacenAutocompletadoComponent } from "../../../../../adminsitrador/maestras/almacenes/components/combo-almacen-autocompletado/combo-almacen-autocompletado.component";

@Component({
    selector: 'app-listar-lotes-por-vencer-page',
    imports: [CommonModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatExpansionModule,
    ComboRisComponent,
    //ComboAlmacenComponent,
    ComboMedicamentoComponent,
    MatIconModule,
    MatButtonModule, MatMenuModule, MatFormFieldModule, MatDatepickerModule,
    FormsModule, ReactiveFormsModule, MatInputModule, ComboAlmacenAutocompletadoComponent],
    templateUrl: './listar-lotes-por-vencer-page.component.html',
    styleUrl: './listar-lotes-por-vencer-page.component.css',
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es' },
        provideMomentDateAdapter(),
    ]
})
export class ListarLotesPorVencerPageComponent implements OnInit {

  @ViewChild('content', { static: false }) content!: ElementRef;

  titulo: string = 'LOTES POR VENCER';
  selectedRis!: any;
  selectedAlmacen!: any;
  selectedMedicamento!: any;
  range = new FormGroup({
    inicio: new FormControl<Date | null>(null),
    fin: new FormControl<Date | null>(null),
  });


  dataSource: any[] = [];
  resultsLength = 0;
  isLoadingResults = false;
  isRateLimitReached = false;
  spinerDetallado = true;
  displayedColumns: string[] = ['ris','codigo_establecimiento','nombre_establecimiento','codigo_medicamento','nombre_medicamento','lote','stock','fecha_vto'];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;



  constructor(
    private dataService: DataService,
    private lotesPorVencer: LotesPorVencerService
  ){}


  fechaInicio: Date | null = null;
  fechaFin: Date | null = null;

  buscarLotesPorVencer() {

    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          var campo = this.sort.active;
          var direccion = this.sort.direction;
          var pagina = this.paginator.pageIndex;
          var cantidad = this.paginator.pageSize;
          var ris=this.selectedRis ? this.selectedRis.id_tipo : null;
          var almacen = this.selectedAlmacen;
          var medicamento = this.selectedMedicamento ? this.selectedMedicamento.id_medicamento : null;
          var inicio = this.range.value.inicio;
          var fin = this.range.value.fin;
          var parametros = {campo,direccion,pagina,cantidad,ris,almacen,medicamento,inicio,fin}

          return this.lotesPorVencer.listar(parametros).
          pipe(catchError(() => of(null)));
        }),
        map(data => {

          this.isLoadingResults = false;
          this.isRateLimitReached = data === null;

          if (data === null) {
            return [];
          }
          this.resultsLength = data.total;


          return data.resultado;
        }),
      )
      .subscribe(data => (this.dataSource = data));
  }


  ngOnInit(): void {


    this.dataService.setPageTitle(this.titulo);
  }


  onRisSeleccionado(ris: any): void {
    if (ris == null) {
      this.selectedRis = null
      return
    }
    this.selectedRis = ris;
  }

  onAlmacenSeleccionado(almacen: any): void {
    this.selectedAlmacen = almacen;
  }

  onMedicamentoSeleccionado(medicamento: any): void {
    this.selectedMedicamento = medicamento;

  }

  generarPDF(){
    var ris=this.selectedRis ? this.selectedRis.id_tipo : null;
    var almacen = this.selectedAlmacen;
    var medicamento = this.selectedMedicamento ? this.selectedMedicamento.id_medicamento : null;
    var inicio = this.range.value.inicio;
    var fin = this.range.value.fin;
    var parametros = {ris,almacen,medicamento,inicio,fin}

    this.lotesPorVencer.listar(parametros).subscribe((data)=>{
      const doc = new jsPDF({
        orientation: 'landscape'
      });


      (doc as any).autoTable({
        head: [this.displayedColumns],
        body: data.resultado.map((row:any) => Object.values(row)), // Mapea los objetos a arrays para autoTable
      });
      console.log(doc);
      doc.save('lotes-por-vencer.pdf');
    })
  }


  generarExcel(): void {
    var fecha = new Date();

    var ris=this.selectedRis ? this.selectedRis.id_tipo : null;
    var almacen = this.selectedAlmacen;
    var medicamento = this.selectedMedicamento ? this.selectedMedicamento.id_medicamento : null;
    var inicio = this.range.value.inicio;
    var fin = this.range.value.fin;
    var parametros = {ris,almacen,medicamento,inicio,fin}

    this.lotesPorVencer.listar(parametros)
    .subscribe(data => {
      const workbook = XLSX.utils.book_new();
      const worksheet = XLSX.utils.json_to_sheet(data.resultado);
      XLSX.utils.book_append_sheet(workbook, worksheet, 'Stock');
      XLSX.writeFile(workbook, `Stock_medicamentos${fecha.getDate()}_${fecha.getMonth() + 1}_${fecha.getFullYear()}.xlsx`);
    });
  }
}
