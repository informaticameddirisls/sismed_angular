import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-lotes-por-vencer-page',
    imports: [RouterOutlet],
    templateUrl: './layout-lotes-por-vencer-page.component.html',
    styleUrl: './layout-lotes-por-vencer-page.component.css'
})
export class LayoutLotesPorVencerPageComponent {

}
