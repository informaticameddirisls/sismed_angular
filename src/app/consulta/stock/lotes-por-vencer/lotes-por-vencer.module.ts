import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LotesPorVencerRoutingModule } from './lotes-por-vencer-routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LotesPorVencerRoutingModule
  ]
})
export class LotesPorVencerModule { }
