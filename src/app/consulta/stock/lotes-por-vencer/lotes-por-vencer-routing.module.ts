import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';
import { ListarLotesPorVencerPageComponent } from './pages/listar-lotes-por-vencer-page/listar-lotes-por-vencer-page.component';


const routes: Routes = [
  {
    path: 'lotes_por_vencer',
    children:

      [{
        path: 'listar',
        component: ListarLotesPorVencerPageComponent,
        canActivate: [AuthGuard]
      }]
  }
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LotesPorVencerRoutingModule { }
