import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarCuadroResumenPageComponent } from './pages/listar-cuadro-resumen-page/listar-cuadro-resumen-page.component';
import { AuthGuard } from '../../../auth/guards/auth.guard';

const routes: Routes = [
  {

    path: 'cuadro_resumen',
    children:
      [
        {
          path: 'listar',
          component: ListarCuadroResumenPageComponent,
          canActivate: [AuthGuard]
        }
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CuadroResumenRoutingModule { }
