import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../../../../shared/services/data.service';
import { CommonModule, DatePipe } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { CuadroResumenService } from '../../services/cuadro-resumen.service';
import { catchError, map, merge, of, startWith, switchMap } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ModalCuadroResumenPageComponent } from '../../components/modal-cuadro-resumen-page/modal-cuadro-resumen-page.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { parse } from 'date-fns';
import { es } from 'date-fns/locale';

@Component({
    selector: 'app-listar-cuadro-resumen-page',
    imports: [
        CommonModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatIconModule,
        MatProgressSpinnerModule
    ],
    templateUrl: './listar-cuadro-resumen-page.component.html',
    styleUrl: './listar-cuadro-resumen-page.component.css'
})
export class ListarCuadroResumenPageComponent implements OnInit {
  titulo: string = 'CUADRO RESUMEN';

  displayedColumns1: string[] = [
    'cod_almacen',
    'nombre_almacen',
    'cantidad_total',
    'cantidad_medicamento',
    'con_stock_medicamento',
    'sin_stock_medicamento',
    'cantidad_insumo',
    'con_stock_insumo',
    'sin_stock_insumo',
    'fecha',
  ];

  data: any[] = [];
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private dataService: DataService,
    private cuadroResumenService: CuadroResumenService,
    private dialog: MatDialog,
  ) {}

  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);
  }

  ngAfterViewInit() {
    merge(this.sort.sortChange)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          var campo = this.sort.active;
          var direccion = this.sort.direction;
          var parametros = { campo, direccion };

          return this.cuadroResumenService.cuadro_resumen(parametros).pipe(catchError(() => of(null)));
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = data === null;

          if (data === null) {
            return [];
          }
          console.log(data.resultado);
          return data.resultado;
        }),
      )
      .subscribe(data => (this.data = data));
  }

  llenarCuadroResumen() {
    this.cuadroResumenService.cuadro_resumen().subscribe((response) => {
      this.data = response.resultado;
    });
  }

  abrirModal(titulo: string, almacen: string, medicamento: boolean, conStock: boolean) {
    const dialogRef = this.dialog.open(ModalCuadroResumenPageComponent, {
      data: { titulo, almacen, medicamento, conStock }
    });
  }

  // Función para calcular la diferencia de días entre dos fechas
  calcularDiferenciaDias(fecha: string | null): number|null {
    if (!fecha) {
      return null; // Valor predeterminado para fechas nulas
    }
    const fechaActual = new Date();
    const fechaActualizada = parse(fecha, 'dd/MM/yyyy hh:mm a', new Date(), { locale: es });
    const diferenciaTiempo = fechaActual.getTime() - fechaActualizada.getTime();
    const diferenciaDias = Math.floor(diferenciaTiempo / (1000 * 3600 * 24));
    return diferenciaDias;
  }

  // Función para obtener el color de la celda basado en la diferencia de días
  obtenerColorCelda(fecha: string | null): { [klass: string]: any } {
    const diferenciaDias = this.calcularDiferenciaDias(fecha);
    if (diferenciaDias === null) {
      return { 'background-color': 'gray', 'color': 'white' }; // Color para fechas nulas
    } else if (diferenciaDias <= 2) {
      return { 'background-color': 'green', 'color': 'white' };
    } else if (diferenciaDias <= 4) {
      return { 'background-color': 'yellow', 'color': 'black' };
    } else {
      return { 'background-color': 'red', 'color': 'white' };
    }
  }
}
