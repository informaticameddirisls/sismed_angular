import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-cuadro-resumen-page',
    imports: [RouterOutlet],
    templateUrl: './layout-cuadro-resumen-page.component.html',
    styleUrl: './layout-cuadro-resumen-page.component.css'
})
export class LayoutCuadroResumenPageComponent {

}
