import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CuadroResumenRoutingModule } from './cuadro-resumen-routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CuadroResumenRoutingModule
  ]
})
export class CuadroResumenModule { }
