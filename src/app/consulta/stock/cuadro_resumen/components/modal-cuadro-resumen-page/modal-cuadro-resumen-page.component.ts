import { CuadroResumenService } from './../../services/cuadro-resumen.service';
import { CommonModule } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';

@Component({
    selector: 'app-modal-cuadro-resumen-page',
    imports: [MatFormFieldModule, MatInputModule, MatIconModule, MatCardModule, MatButtonModule, ReactiveFormsModule, CommonModule, MatTableModule,
        MatProgressSpinnerModule
    ],
    templateUrl: './modal-cuadro-resumen-page.component.html',
    styleUrl: './modal-cuadro-resumen-page.component.css'
})
export class ModalCuadroResumenPageComponent implements OnInit {

  isLoadingResults :boolean =false;
  dataSource: any[] = [];
  displayedColumns: string[] = ['medicamento', 'id_medicamento', 'stock'];


  constructor(
    public dialogRef: MatDialogRef<ModalCuadroResumenPageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private cuadroResumenService: CuadroResumenService
  ){}

  ngOnInit(): void {
    this.isLoadingResults = true;
    this.cuadroResumenService.detalle_cuadro_resumen(this.data).subscribe((response)=>{
      this.dataSource = response.resultado
      this.isLoadingResults = false;
    })

  }


  onNoClick(): void {
    this.dialogRef.close();
  }

}
