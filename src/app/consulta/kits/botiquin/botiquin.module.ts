import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BotiquinRoutingModule } from './botiquin-routing.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';



@NgModule({ declarations: [], imports: [CommonModule,
        BotiquinRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class BotiquinModule { }
