import { Component, EventEmitter, Output } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-kits-botiquin-page',
    imports: [RouterOutlet],
    templateUrl: './layout-kits-botiquin-page.component.html',
    styleUrl: './layout-kits-botiquin-page.component.css'
})
export class LayoutKitsBotiquinPageComponent {

}
