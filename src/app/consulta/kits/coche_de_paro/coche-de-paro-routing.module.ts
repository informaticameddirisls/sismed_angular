import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';

import { ListarKitsCocheDeParoPageComponent } from './pages/listar-kits-coche-de-paro-page/listar-kits-coche-de-paro-page.component';

const routes: Routes = [
  {

    path: 'coche_de_paro',
    children:
      [
        {
          path: 'listar',
          component: ListarKitsCocheDeParoPageComponent,
          canActivate: [AuthGuard]
        }
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClavesRoutingModule { }
