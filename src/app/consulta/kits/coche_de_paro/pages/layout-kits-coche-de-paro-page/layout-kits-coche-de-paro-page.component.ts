import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-kits-coche-de-paro-page',
    imports: [RouterOutlet],
    templateUrl: './layout-kits-coche-de-paro-page.component.html',
    styleUrl: './layout-kits-coche-de-paro-page.component.css'
})
export class LayoutKitsCocheDeParoPageComponent {

}
