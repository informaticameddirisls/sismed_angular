import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-kits-dengue-page',
    imports: [RouterOutlet],
    templateUrl: './layout-kits-dengue-page.component.html',
    styleUrl: './layout-kits-dengue-page.component.css'
})
export class LayoutKitsDenguePageComponent {

}
