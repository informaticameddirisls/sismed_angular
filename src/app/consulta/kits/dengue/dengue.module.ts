import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DengueRoutingModule } from './dengue-routing.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';



@NgModule({ declarations: [], imports: [CommonModule,
        DengueRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class DengueModule { }
