import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClavesRoutingModule } from './claves-routing.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';



@NgModule({ declarations: [], imports: [CommonModule,
        ClavesRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class ClavesModule { }
