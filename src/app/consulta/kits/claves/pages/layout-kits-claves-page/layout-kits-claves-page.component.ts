import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-kits-claves-page',
    imports: [RouterOutlet],
    templateUrl: './layout-kits-claves-page.component.html',
    styleUrl: './layout-kits-claves-page.component.css'
})
export class LayoutKitsClavesPageComponent {

}
