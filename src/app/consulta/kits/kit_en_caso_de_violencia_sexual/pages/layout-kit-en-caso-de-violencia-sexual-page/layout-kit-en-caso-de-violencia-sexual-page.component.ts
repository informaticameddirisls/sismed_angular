import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-kit-en-caso-de-violencia-sexual-page',
    imports: [RouterOutlet],
    templateUrl: './layout-kit-en-caso-de-violencia-sexual-page.component.html',
    styleUrl: './layout-kit-en-caso-de-violencia-sexual-page.component.css'
})
export class LayoutKitEnCasoDeViolenciaSexualPageComponent {

}
