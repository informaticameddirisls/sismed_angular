import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KitEnCasoDeViolenciaSexualRoutingModule } from './kit-en-caso-de-violencia-sexual-routing.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';



@NgModule({ declarations: [], imports: [CommonModule,
        KitEnCasoDeViolenciaSexualRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class KitEnCasoDeViolenciaSexualModule { }
