import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';

import { ListarKitEnCasoDeViolenciaSexualPageComponent } from './pages/listar-kit-en-caso-de-violencia-sexual-page/listar-kit-en-caso-de-violencia-sexual-page.component';

const routes: Routes = [
  {

    path: 'kit_en_caso_de_violencia_sexual',
    children:
      [
        {
          path: 'listar',
          component: ListarKitEnCasoDeViolenciaSexualPageComponent,
          canActivate: [AuthGuard]
        }
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KitEnCasoDeViolenciaSexualRoutingModule { }
