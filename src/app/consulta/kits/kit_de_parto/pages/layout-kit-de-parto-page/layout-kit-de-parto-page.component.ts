import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-kit-de-parto-page',
    imports: [RouterOutlet],
    templateUrl: './layout-kit-de-parto-page.component.html',
    styleUrl: './layout-kit-de-parto-page.component.css'
})
export class LayoutKitDePartoPageComponent {

}
