import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';

import { ListarKitDePartoPageComponent } from './pages/listar-kit-de-parto-page/listar-kit-de-parto-page.component';

const routes: Routes = [
  {

    path: 'kit_de_parto',
    children:
      [
        {
          path: 'listar',
          component: ListarKitDePartoPageComponent,
          canActivate: [AuthGuard]
        }
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KitDePartoRoutingModule { }
