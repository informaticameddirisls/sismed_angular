import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KitDePartoRoutingModule } from './kit-de-parto-routing.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';



@NgModule({ declarations: [], imports: [CommonModule,
        KitDePartoRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class KitDePartoModule { }
