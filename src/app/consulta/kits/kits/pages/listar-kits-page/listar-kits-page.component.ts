import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../../../shared/services/data.service';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { CommonModule, DatePipe } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatExpansionModule } from '@angular/material/expansion';
import { ComboRisComponent } from '../../../../../adminsitrador/maestras/ris/components/combo-ris/combo-ris.component';
import { ComboAlmacenComponent } from '../../../../../adminsitrador/maestras/almacenes/components/combo-almacen/combo-almacen.component';
import { ComboMedicamentoComponent } from '../../../../../adminsitrador/maestras/medicamentos/components/combo-medicamento/combo-medicamento.component';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { ComboMaestraCategoriaComponent } from '../../../../../adminsitrador/maestras/maestra_categoria/components/combo-maestra-categoria/combo-maestra-categoria.component';
import { ClavesService } from '../../services/kits.service';
import { ExcelService } from '../../../../../shared/services/excel.service';

@Component({
    selector: 'app-listar-kits-page',
    imports: [
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatExpansionModule,
        ComboRisComponent,
        MatButtonModule,
        MatMenuModule,
        ComboMaestraCategoriaComponent,
        CommonModule
    ],
    templateUrl: './listar-kits-page.component.html',
    styleUrl: './listar-kits-page.component.css'
})

export class ListarKitsPageComponent implements OnInit {
  titulo: string = 'KITS: VIOLENCIA SEXUAL';
  selectedRis!: any;
  selectedMaestraCategoria!: any;
  selectedMedicamento!: any;
  isLoadingResults = true;
  cabeceraVacia: boolean = true;
  tipoMaestraCategoria = 'KIT';
  displayedColumns: string[] = [];
  displayedColumns2: any[] = [];
  displayedColumns3: any[] = [];
  dataSource = new MatTableDataSource<any>([]);

  constructor(
    private dataService: DataService,
    private dialog: MatDialog,
    private clavesService: ClavesService,
    private excelService: ExcelService
  ){}

  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);
    this.isLoadingResults = false;
  }

  selectedValue!: string;

  onMaestraCategoria(selectedValue: any) {
    console.log(selectedValue);
    this.selectedMaestraCategoria = selectedValue;
  }

  onRisSeleccionado(ris: any): void {
    if (ris == null) {
      this.selectedRis = null;
      return;
    }
    this.selectedRis = ris;
  }


  onMedicamentoSeleccionado(medicamento: any): void {
    this.selectedMedicamento = medicamento;
  }

  cuadroKit(): void {
    const categoria = this.selectedMaestraCategoria.id_maestra_categoria;
    const ris = this.selectedRis ? this.selectedRis.id_tipo : 0 ;
    this.displayedColumns3 = [];
    this.clavesService.claves_categoria_medicamento(categoria, ris).subscribe((resultado) => {
      this.displayedColumns = resultado.cabecera;

      const cantProductos = this.displayedColumns.length - 2;
      var arrStockRequiere: any[] = [];
      let contador = 0;

      //console.log(resultado.resultado);

      for (let i = 0; i < resultado.cabecera.length; i++) {
        if (i>=2) {
          arrStockRequiere.push({ id: `st_${contador}`, nombre: 'STOCK' });
          arrStockRequiere.push({ id: `rq_${contador}`, nombre: 'REQUIERE' });
        }
      }

      this.displayedColumns2 = arrStockRequiere;

      for (let i = 0; i < resultado.resultado.length; i++) {
        var arrStockRequiere3 = [];
        for (let j = 0; j < resultado.cabecera.length; j++) {

          if (j<2) {
            arrStockRequiere3.push({ id: `${resultado.cabecera[j]}`, nombre: `${resultado.cabecera[j]}`,valor:`${resultado.resultado[i][resultado.cabecera[j]]}` });

          } else {
            arrStockRequiere3.push({ id: `${resultado.cabecera[j]}`, nombre: `${resultado.cabecera[j]}`,
            valor:`${resultado.resultado[i][resultado.cabecera[j]]}`,clase:`${resultado.resultado[i][resultado.cabecera[j]]-resultado.resultado[i][resultado.cabecera[j].substring(0, 5)]<0 ? 'rojo' : 'verde'}` });
            arrStockRequiere3.push({ id: `${resultado.cabecera[j].substring(0, 5)}`, nombre: `${resultado.cabecera[j].substring(0, 5)}`,valor:`${resultado.resultado[i][resultado.cabecera[j].substring(0, 5)]}` });
          }
        }

        this.displayedColumns3.push(arrStockRequiere3)

      }

      console.log(this.displayedColumns3);
      this.cabeceraVacia = false;
    });
  }

  exportTableToExcel(): void {
    this.excelService.exportToExcel('tabla-botiquin', 'exported_data');
  }


}
