import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-kits-page',
    imports: [RouterOutlet],
    templateUrl: './layout-kits-page.component.html',
    styleUrl: './layout-kits-page.component.css'
})
export class LayoutKitsPageComponent {

}
