import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KitsRoutingModule } from './kits-routing.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';



@NgModule({ declarations: [], imports: [CommonModule,
        KitsRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class KitsModule { }
