import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';

import { ListarRecienNacidoPageComponent } from './pages/listar-kit-de-recien-nacido-page/listar-kit-de-recien-nacido-page.component';

const routes: Routes = [
  {

    path: 'kit_de_recien_nacido',
    children:
      [
        {
          path: 'listar',
          component: ListarRecienNacidoPageComponent,
          canActivate: [AuthGuard]
        }
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecienNacidoRoutingModule { }
