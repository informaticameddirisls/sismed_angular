import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-kit-de-recien-nacido-page',
    imports: [RouterOutlet],
    templateUrl: './layout-kit-de-recien-nacido-page.component.html',
    styleUrl: './layout-kit-de-recien-nacido-page.component.css'
})
export class LayoutRecienNacidoPageComponent {

}
