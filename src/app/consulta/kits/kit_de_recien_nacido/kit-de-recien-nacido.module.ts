import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecienNacidoRoutingModule } from './kit-de-recien-nacido-routing.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';



@NgModule({ declarations: [], imports: [CommonModule,
        RecienNacidoRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class RecienNacidoModule { }
