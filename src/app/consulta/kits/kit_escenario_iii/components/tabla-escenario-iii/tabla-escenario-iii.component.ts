import { Component } from '@angular/core';
import { DataService } from '../../../../../shared/services/data.service';
import { MatDialog } from '@angular/material/dialog';
import { KitEscenarioIIIService } from '../../services/kit-escenario-iii.service';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

@Component({
    selector: 'app-consulta-kits-tabla-escenario-iii',
    imports: [CommonModule],
    templateUrl: './tabla-escenario-iii.component.html',
    styleUrl: './tabla-escenario-iii.component.css'
})
export class TablaEscenarioIiiComponent {

  displayedColumns!: any[][];
  headPrimeraLinea!: any[];
  headSegundaLinea!: any[];
  headTerceraLinea!: any[];
  headCuartaLinea!: any[];
  cabecera!: any[];
  data!: any[];
  prueba: string = '';
  blanco: boolean = true;
  minimo: number = 0;
  constructor(
    private dataService: DataService,
    private dialog: MatDialog,
    private kitEscenarioIIIService: KitEscenarioIIIService,
  ){
    this.cuadroKit();
  }

  asignarMinimo(valor:number){
    this.minimo = valor;
  }

  activalo(){
    this.blanco = true;
  }
  desactivalo(){
    this.blanco = false;

  }


  getPrueba(value: string) {
    this.prueba = value;
  }


  cuadroKit(): void {

    this.kitEscenarioIIIService.kit_escenario_iii().subscribe((resultado) => {

      if (resultado.success == true) {
        this.headPrimeraLinea = resultado.primera;
        this.headSegundaLinea = resultado.segunda;
        this.headTerceraLinea = resultado.tercera;
        this.headCuartaLinea = resultado.cuarta;
        this.data = resultado.resultado;
        this.cabecera = resultado.cabecera;
      }

      return;
      let row = [];
      let cabecera = resultado.cabecera;
      let cabecera1=[];
      console.log(cabecera);
      cabecera.forEach((valor:any, indice:any)=> {
        if (indice<=4) {
          cabecera1.push('rowspan',4);
        }



      });
      console.log(cabecera);
      this.displayedColumns = resultado.resultado;




    });
  }



}
