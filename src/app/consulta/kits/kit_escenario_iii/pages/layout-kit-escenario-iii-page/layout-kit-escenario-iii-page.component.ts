import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-kit-escenario-iii-page',
    imports: [RouterOutlet],
    templateUrl: './layout-kit-escenario-iii-page.component.html',
    styleUrl: './layout-kit-escenario-iii-page.component.css'
})
export class LayoutKitEscenarioIIIPageComponent {

}
