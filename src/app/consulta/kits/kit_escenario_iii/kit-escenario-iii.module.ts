import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KitEscenarioIIIRoutingModule } from './kit-escenario-iii-routing.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';
import { BrowserModule } from '@angular/platform-browser';



@NgModule({ declarations: [], imports: [CommonModule,
        KitEscenarioIIIRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class KitEscenarioIIIModule { }
