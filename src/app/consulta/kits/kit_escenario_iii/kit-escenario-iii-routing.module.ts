import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';

import { ListarKitEscenarioIIIPageComponent } from './pages/listar-kit-escenario-iii-page/listar-kit-escenario-iii-page.component';

const routes: Routes = [
  {

    path: 'kit_escenario_iii',
    children:
      [
        {
          path: 'listar',
          component: ListarKitEscenarioIIIPageComponent,
          canActivate: [AuthGuard]
        }
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KitEscenarioIIIRoutingModule { }
