import { ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { DataService } from '../../../../../shared/services/data.service';
import { MatDialog } from '@angular/material/dialog';
import { CommonModule, DatePipe, JsonPipe } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatExpansionModule } from '@angular/material/expansion';
import { ComboRisComponent } from '../../../../../adminsitrador/maestras/ris/components/combo-ris/combo-ris.component';
import { ComboAlmacenComponent } from '../../../../../adminsitrador/maestras/almacenes/components/combo-almacen/combo-almacen.component';
import { ComboMedicamentoComponent } from '../../../../../adminsitrador/maestras/medicamentos/components/combo-medicamento/combo-medicamento.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { provideMomentDateAdapter } from '@angular/material-moment-adapter';
import { catchError, map, merge, of, startWith, switchMap } from 'rxjs';
import { MovAlmacenesService } from '../../services/mov-almacenes.service';
import jsPDF from 'jspdf';
import 'moment/locale/es';
import 'jspdf-autotable';
import * as XLSX from 'xlsx';
import { MatSelectModule } from '@angular/material/select';
import { ComboAlmacenFiltradoComponent } from "../../../../../adminsitrador/maestras/almacenes/components/combo-almacen-filtrado/combo-almacen-filtrado.component";

@Component({
    selector: 'app-listar-mov-almacenes-page',
    templateUrl: './listar-mov-almacenes-page.component.html',
    styleUrl: './listar-mov-almacenes-page.component.css',
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es' },
        provideMomentDateAdapter(),
    ],
    imports: [CommonModule,
        MatProgressSpinnerModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatExpansionModule,
        ComboAlmacenComponent,
        ComboMedicamentoComponent,
        MatIconModule,
        MatButtonModule, MatMenuModule, MatFormFieldModule, MatDatepickerModule,
        FormsModule, ReactiveFormsModule, MatInputModule, MatSelectModule]
})
export class ListarMovAlmacenesPageComponent {
  titulo: string = 'MOVIMIENTOS POR ALMACENES';

  selectedAlmacen!: any;
  AlmacenTipo!:string;
  selectedMedicamento!: any;
  selectedTipo!: any;
  resultsLength = 0;
  dataSource: any[] = [];
  range = new FormGroup({
    inicio: new FormControl<Date | null>(null),
    fin: new FormControl<Date | null>(null),
  });
  isLoadingResults = false;


  displayedColumns: string[] = ['codigo_establecimiento','nombre_establecimiento','movfechreg','hora','movcoditip','tipodoc','movnumedco','codigo_medicamento',
  'nombre_medicamento','movcantid','movsitua','movrefe','usrdescrip','destino'];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  isRateLimitReached = false;
  constructor(
    private dataService: DataService,
    private dialog: MatDialog,
    private movAlmacenes: MovAlmacenesService,
    private cdr: ChangeDetectorRef // Inyecta ChangeDetectorRef

  ){this.AlmacenTipo='A';}

  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);

    //this.llenarCuadroResumen();
  }

  onAlmacenSeleccionado(almacen: any): void {
    this.selectedAlmacen = almacen;
  }

  onMedicamentoSeleccionado(medicamento: any): void {
    this.selectedMedicamento = medicamento;
  }


  buscarMovimientos() {

    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          var campo = this.sort.active;
          var movcoditip = this.selectedTipo;
          var direccion = this.sort.direction;
          var pagina = this.paginator.pageIndex;
          var cantidad = this.paginator.pageSize;
          var almacen = this.selectedAlmacen;
          var medicamento = this.selectedMedicamento ? this.selectedMedicamento.id_medicamento : null;
          var inicio = this.range.value.inicio;
          var fin = this.range.value.fin;
          var parametros = {campo,movcoditip,direccion,pagina,cantidad,almacen,medicamento,inicio,fin}

          return this.movAlmacenes.listar(parametros).
          pipe(catchError(() => of(null)));
        }),
        map(data => {

          this.isLoadingResults = false;
          this.isRateLimitReached = data === null;

          if (data === null) {
            return [];
          }
          this.resultsLength = data.total;


          return data.resultado;
        }),
      )
      .subscribe(data => {

        this.dataSource = data
        if (this.paginator) {
          this.paginator.length = this.resultsLength; // Actualiza la longitud total para la paginación
        }

        this.cdr.detectChanges(); // Fuerza la detección de cambios

      });
  }

  generarPDF(){
    var almacen = this.selectedAlmacen;
    var medicamento = this.selectedMedicamento ? this.selectedMedicamento.id_medicamento : null;
    var inicio = this.range.value.inicio;
    var movcoditip = this.selectedTipo;
    var fin = this.range.value.fin;
    var parametros = {movcoditip,almacen,medicamento,inicio,fin}

    this.movAlmacenes.listar(parametros).subscribe((data)=>{
      const doc = new jsPDF({
        orientation: 'landscape'
      });

      data.resultado =this.filterColumns(data.resultado,this.displayedColumns);

      (doc as any).autoTable({
        head: [this.displayedColumns],
        body: data.resultado.map((row:any) => Object.values(row)), // Mapea los objetos a arrays para autoTable
      });
      console.log(doc);
      doc.save('movimientos.pdf');
    })
  }

  // Función para filtrar columnas específicas
filterColumns = (data:any, columnsToShow:any) => {
  return data.map((obj:any) => {
      let newObj:any = {};
      columnsToShow.forEach((column:any) => {
          if (obj.hasOwnProperty(column)) {
              newObj[column] = obj[column];
          }
      });
      return newObj;
  });
};

  generarExcel(): void {
    var fecha = new Date();

    var almacen = this.selectedAlmacen;
    var medicamento = this.selectedMedicamento ? this.selectedMedicamento.id_medicamento : null;
    var inicio = this.range.value.inicio;
    var movcoditip = this.selectedTipo;
    var fin = this.range.value.fin;
    var parametros = {movcoditip,almacen,medicamento,inicio,fin}

    this.movAlmacenes.listar(parametros)
    .subscribe(data => {
      const workbook = XLSX.utils.book_new();
      const worksheet = XLSX.utils.json_to_sheet(data.resultado);
      XLSX.utils.book_append_sheet(workbook, worksheet, 'Movmiento');
      XLSX.writeFile(workbook, `movimiento_almacenes_${fecha.getDate()}_${fecha.getMonth() + 1}_${fecha.getFullYear()}.xlsx`);
    });
  }
}
