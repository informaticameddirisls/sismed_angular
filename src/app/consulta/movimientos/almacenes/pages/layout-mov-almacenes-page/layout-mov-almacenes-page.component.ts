import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-mov-almacenes-page',
    imports: [RouterOutlet],
    templateUrl: './layout-mov-almacenes-page.component.html',
    styleUrl: './layout-mov-almacenes-page.component.css'
})
export class LayoutMovAlmacenesPageComponent {

}
