import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';
import { MovAlmacenesRoutingModule } from './mov-almacenes-routing.module';



@NgModule({ declarations: [], imports: [CommonModule,
        MaterialModule,
        MovAlmacenesRoutingModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class MovAlmacenesModule { }
