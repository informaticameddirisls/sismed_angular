import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-mov-establecimientos-page',
    imports: [RouterOutlet],
    templateUrl: './layout-mov-establecimientos-page.component.html',
    styleUrl: './layout-mov-establecimientos-page.component.css'
})
export class LayoutMovEstablecimientosPageComponent {

}
