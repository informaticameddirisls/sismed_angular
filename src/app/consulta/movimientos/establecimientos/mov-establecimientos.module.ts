import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';
import { MovEstablecimientosRoutingModule } from './mov-establecimientos-routing.module';



@NgModule({ declarations: [], imports: [CommonModule,
        MaterialModule,
        MovEstablecimientosRoutingModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class MovEstablecimientosModule { }
