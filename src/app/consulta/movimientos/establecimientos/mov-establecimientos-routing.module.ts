import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';
import { ListarMovEstablecimientosPageComponent } from './pages/listar-mov-establecimientos-page/listar-mov-establecimientos-page.component';

const routes: Routes = [
  {

    path: 'establecimientos',
    children:
      [
        {
          path: 'listar',
          component: ListarMovEstablecimientosPageComponent,
          canActivate: [AuthGuard]
        }
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovEstablecimientosRoutingModule { }
