import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IciDiarioRoutingModule } from './ici-diario-routing.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';



@NgModule({ declarations: [], imports: [CommonModule,
        IciDiarioRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class IciDiarioModule { }
