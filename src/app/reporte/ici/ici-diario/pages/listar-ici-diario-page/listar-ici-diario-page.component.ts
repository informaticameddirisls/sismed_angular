import { IciDiarioService } from './../../services/ici-diario.service';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../../../../shared/services/data.service';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatNativeDateModule, provideNativeDateAdapter } from '@angular/material/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComboAlmacenComponent } from '../../../../../adminsitrador/maestras/almacenes/components/combo-almacen/combo-almacen.component';
import * as XLSX from 'xlsx';

// the `default as` syntax.
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment} from 'moment';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter, provideMomentDateAdapter } from '@angular/material-moment-adapter';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { ModalIciDiarioComponent } from '../../components/modal-ici-diario/modal-ici-diario-form.component';
import { ChartModule } from 'primeng/chart';
import { ComboRisComponent } from "../../../../../adminsitrador/maestras/ris/components/combo-ris/combo-ris.component";
import { ComboAlmacenAutocompletadoComponent } from "../../../../../adminsitrador/maestras/almacenes/components/combo-almacen-autocompletado/combo-almacen-autocompletado.component";

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
    selector: 'app-listar-ici-diario-page',
    imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatButtonModule,
    ComboAlmacenAutocompletadoComponent,
    MatMenuModule,
    MatIconModule,
    MatFormFieldModule, MatInputModule, MatDatepickerModule, FormsModule, ReactiveFormsModule, MatNativeDateModule,
    MatPaginatorModule, MatTableModule,
    ComboRisComponent,
    ComboAlmacenAutocompletadoComponent
],
    templateUrl: './listar-ici-diario-page.component.html',
    styleUrl: './listar-ici-diario-page.component.css',
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-PE' },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    ]
})
export class ListarIciDiarioPageComponent  implements OnInit {
  titulo: string = 'ICI DIARIO';
  selectedRis!: any;
  selectedMaestraCategoria!: any;
  selectedMedicamento!: any;
  isLoadingResults = true;
  cabeceraVacia: boolean = true;
  displayedColumns: string[] = [];
  displayedColumns2: any[] = [];
  displayedColumns3: any[] = [];
  dataSource = new MatTableDataSource<any>([]);
  selectedAlmacen!: any;
  iciDiarioDatos: any;
  graficoIciDiario: any;
  date: FormControl = new FormControl(moment());
  disabledDate = new FormControl({value: '', disabled: true});




  data: any;
  options: any;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  iciColumnas: string[] = ['id_almacen','almacen','id_medicamento','medicamento','tipo','campo1','suma_total','cantidad','cpma','msd','suma_4_ultimos','disponibilidad','programacion_estrategica'];


  fechas: string[] = [];

  constructor(
    private dataService: DataService,
    private dialog: MatDialog,
    private iciDiarioService: IciDiarioService,
    private cdr: ChangeDetectorRef
  ){

    this.fechas = this.obtenerUltimos12Meses();

    this.iciColumnas = this.iciColumnas.concat(this.fechas);

    console.log(this.iciColumnas);
  }

  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);
    this.isLoadingResults = false;
  }

  onAlmacenSeleccionado(almacen: any): void {
    this.selectedAlmacen = almacen;
  }

  onRisSeleccionado(ris: any): void {
    if (ris == null) {
      this.selectedRis = null
      return
    }
    this.selectedRis = ris;
  }

  consultarIciDiario() {
    const date = moment(this.date.value).format('YYYY-MM-DD');
    console.log(this.selectedAlmacen,"this.selectedAlmacen");
    const arrAlmacen = this.selectedAlmacen || [];
    const parametros = { date, arrAlmacen };
    this.isLoadingResults = true;
    this.iciDiarioService.ici_diario(parametros).subscribe(
      (response) => {
        if (response.success) {
          this.dataSource = new MatTableDataSource(response.resultado);
          this.dataSource.paginator = this.paginator;
          this.iciDiarioDatos = response.resultado;
          // Si hay resultados, combinar columnas base con las fechas
          if (response.resultado.length > 0) {
            const columnasBase = ['id_almacen','almacen','id_medicamento','medicamento','tipo','evalua_digemid'];

            const columnasBase2 = ['campo1','cpma','msd','disponibilidad'];


            this.displayedColumns = [...columnasBase, ...this.fechas, ...columnasBase2]; // Mantén las columnas de fechas fijas
            console.log(this.displayedColumns);
            // Si necesitas verificar que las fechas están en la respuesta, puedes hacerlo aquí
          }
        } else {
          console.error('Error al listar ici diario:', response);
        }
        this.isLoadingResults = false;
      },
      (error) => {
        console.error('Error al listar ici diario:', error);
        this.isLoadingResults = false;
      }
    );
  }


  obtenerUltimos12Meses(): string[] {
    const fechas: string[] = [];
    const fechaActual = new Date();

    for (let i = 0; i < 12; i++) {
      const fecha = new Date(fechaActual.getFullYear(), fechaActual.getMonth() - i);
      const anio = fecha.getFullYear();
      const mes = String(fecha.getMonth() + 1).padStart(2, '0'); // Asegúrate de que el mes tenga dos dígitos
      fechas.push(`${anio}${mes}`); // Formato YYYYMM
    }

    return fechas.reverse(); // Para tener las fechas del más antiguo al más reciente
  }


  generarExcel(): void {
    var fecha = new Date();

    const date = moment(this.date.value).format('YYYY-MM-DD');
    const arrAlmacen = this.selectedAlmacen || [];
    const parametros = { date, arrAlmacen };


    this.iciDiarioService.ici_diario(parametros)
    .subscribe(data => {
      const workbook = XLSX.utils.book_new();
      const worksheet = XLSX.utils.json_to_sheet(data.resultado);
      XLSX.utils.book_append_sheet(workbook, worksheet, 'Movmiento');
      XLSX.writeFile(workbook, `movimiento_almacenes_${fecha.getDate()}_${fecha.getMonth() + 1}_${fecha.getFullYear()}.xlsx`);
    });
  }


  graficoDisponibilidad() {

    var normostockAcum: number=0;
    var sobrestockAcum: number=0;
    var substockAcum: number=0;
    var desabastecidoAcum: number=0;
    var totalAcum: number=0;

    console.log(this.iciDiarioDatos);

    const conteoPorAlmacen: Array<{
      id_almacen: string;
      almacen: string;
      total: number;
      normostock: number;
      normostockporc: number;
      sobrestock: number;
      sobrestockporc: number;
      substock: number;
      substockporc: number;
      desabastecido: number;
      desabastecidoporc: number;
      disponibilidad: number;
      nivel: string;
      color: string;

    }> = [];

    const almacenMap: { [key: string]: {
      id_almacen: string;
      almacen: string;
      total: number;
      normostock: number;
      sobrestock: number;
      substock: number;
      desabastecido: number;
      normostockporc: number;
      sobrestockporc: number;
      substockporc: number;
      desabastecidoporc: number;
      disponibilidad: number;
      nivel: string;
      color: string;
    } } = {};



    this.iciDiarioDatos.forEach((iciDiario: any) => {
      if (iciDiario.tipo !== "MEDICAMENTOS") {
        return; // Saltar si el tipo no es medicamento
      }

      const almacenId = iciDiario.id_almacen;
      console.log(iciDiario);
      if (!almacenMap[almacenId]) {
        almacenMap[almacenId] = {
          id_almacen: almacenId,
          almacen: iciDiario.almacen,
          total: 0,
          normostock: 0,
          sobrestock: 0,
          substock: 0,
          desabastecido: 0,
          normostockporc: 0,
          sobrestockporc: 0.00,
          substockporc: 0.00,
          desabastecidoporc: 0.00,
          disponibilidad: 0,
          nivel: '',
          color: '',
        };
        conteoPorAlmacen.push(almacenMap[almacenId]);
      }

      almacenMap[almacenId].total++;
      switch (iciDiario.disponibilidad) {
        case 'NORMOSTOCK':
          almacenMap[almacenId].normostock++;
          break;
        case 'SOBRESTOCK':
          almacenMap[almacenId].sobrestock++;
          break;
        case 'SUBSTOCK':
          almacenMap[almacenId].substock++;
          break;
        case 'DESABASTECIDO':
          almacenMap[almacenId].desabastecido++;
          break;
      }
    });

    conteoPorAlmacen.forEach(item => {
      item.normostockporc = Number(((item.normostock / item.total) * 100 || 0).toFixed(2));
      item.sobrestockporc = Number(((item.sobrestock / item.total) * 100 || 0).toFixed(2));
      item.substockporc = Number(((item.substock / item.total) * 100 || 0).toFixed(2));
      item.desabastecidoporc = Number(((item.desabastecido / item.total) * 100 || 0).toFixed(2));
      item.disponibilidad = Number((item.normostockporc+item.sobrestockporc).toFixed(2));

      normostockAcum = normostockAcum + item.normostock;
      sobrestockAcum = sobrestockAcum + item.sobrestock;
      substockAcum = substockAcum + item.substock;
      desabastecidoAcum = desabastecidoAcum + item.desabastecido;
      totalAcum = totalAcum + item.total;

      if ( item.disponibilidad>=70 && item.disponibilidad<80) {
        item.nivel = 'REGULAR';
        item.color = 'REGULAR';
      }
      if ( item.disponibilidad>=80 && item.disponibilidad<=90) {
        item.nivel = 'ALTO';
        item.color = 'ALTO';
      }
      if ( item.disponibilidad>90) {
        item.nivel = 'OPTIMO';
        item.color = 'OPTIMO';
      }
      if ( item.disponibilidad<70) {
        item.nivel = 'BAJO';
        item.color = 'BAJO';
      }

    });


    var consolidado = {
      id_almacen: '',
      almacen: '',
      total: totalAcum,
      normostock: normostockAcum,
      sobrestock: sobrestockAcum,
      substock: substockAcum,
      desabastecido: desabastecidoAcum,
      normostockporc: Number(((normostockAcum / totalAcum) * 100 || 0).toFixed(2)),
      sobrestockporc: Number(((sobrestockAcum / totalAcum) * 100 || 0).toFixed(2)),
      substockporc: Number(((substockAcum / totalAcum) * 100 || 0).toFixed(2)),
      desabastecidoporc: Number(((desabastecidoAcum / totalAcum) * 100 || 0).toFixed(2)),
      disponibilidad: Number((((normostockAcum+sobrestockAcum+substockAcum+desabastecidoAcum) / totalAcum) * 100 || 0).toFixed(2)),
      nivel: '',
      color: '',
    }

    var cantidad = conteoPorAlmacen.length;
    conteoPorAlmacen.push(consolidado);


    console.log(conteoPorAlmacen, "conteoPorAlmacen");
    console.log(this.date.value,'this.disabledDate');
    const dialogRef = this.dialog.open(ModalIciDiarioComponent, {
      data: { conteoPorAlmacen,consolidado,fecha:this.date,cantidad }
    });

    dialogRef.componentInstance.crearConfirmado.subscribe(confirmado => {
      if (confirmado) {
        // Acciones adicionales si es necesario
      }
    });

  }



}
