import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-ici-diario-page',
    imports: [RouterOutlet],
    templateUrl: './layout-ici-diario-page.component.html',
    styleUrl: './layout-ici-diario-page.component.css'
})
export class LayoutIciDiarioPageComponent {

}
