import { ChangeDetectorRef, Component, Inject, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ChartModule } from 'primeng/chart';

@Component({
    selector: 'app-grafico-ici-diario',
    templateUrl: './grafico-ici-diario.component.html',
    styleUrls: ['./grafico-ici-diario.component.css'],
    imports: [ReactiveFormsModule, ChartModule]
})
export class GraficoIciDiarioComponent implements OnInit {
    formularioModulo!: FormGroup;
    selectedSupermodulo: any;
    dataSource = new MatTableDataSource<any>([]);

    @Input() chartOptions!: any;
    data2: any;
    chartOptions2: any;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private fb: FormBuilder,
        private cdref: ChangeDetectorRef,
    ) {
        this.chartOptions = data.consolidado || {};
        console.log(data,"data.conteoPorAlmacen data.conteoPorAlmacen");
        this.data2 = {
            labels: ['SOBRESTOCK', 'NORMOSTOCK', 'SUBSTOCK', 'DESABASTECIDO'],
            datasets: [
                {
                    data: [
                        this.chartOptions.sobrestockporc || 0,
                        this.chartOptions.normostockporc || 0,
                        this.chartOptions.substockporc || 0,
                        this.chartOptions.desabastecidoporc || 0
                    ],
                    backgroundColor: [
                        "#1b84ff",
                        "#17c653",
                        "#f6c000",
                        "#f8285a"
                    ],
                    hoverBackgroundColor: [
                        "#1264c5",
                        "#009f36",
                        "#c39906",
                        "#c51e46"
                    ]
                }
            ]
        };

        this.chartOptions2 = {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
                legend: { position: 'right' },
                tooltip: {
                    callbacks: {
                        label: (context: any) => {
                            const label = context.label || '';
                            const value = context.raw || 0;
                            return `${label}: ${value}%`;
                        },
                    },
                },
            },
        };
    }

    ngOnInit(): void {
        // Additional initialization if needed
    }
}
