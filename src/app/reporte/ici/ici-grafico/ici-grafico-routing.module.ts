import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';
import { ListarIciGraficoPageComponent } from './pages/listar-ici-grafico-page/listar-ici-grafico-page.component';
//import { ListarIciDiarioPageComponent } from './pages/listar-ici-diario-page/listar-ici-diario-page.component';

const routes: Routes = [
  {

    path: 'ici_grafico',
    children:
      [
        {
          path: 'listar',
          component: ListarIciGraficoPageComponent,
          //canActivate: [AuthGuard]
        }
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IciGraficoRoutingModule { }
