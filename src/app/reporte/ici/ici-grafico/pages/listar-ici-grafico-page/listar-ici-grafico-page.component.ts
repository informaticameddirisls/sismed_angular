import { Medicamento } from '../../../../../adminsitrador/maestras/medicamentos/interfaces/Medicamento.interface';
import { IciGraficoService } from '../../services/ici-grafico.service';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../../../../shared/services/data.service';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatNativeDateModule, provideNativeDateAdapter } from '@angular/material/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComboMedicamentoComponent } from '../../../../../adminsitrador/maestras/medicamentos/components/combo-medicamento/combo-medicamento.component';
//import { ComboAlmacenComponent } from '../../../../../maestras/almacenes/components/combo-almacen/combo-almacen.component';
import * as XLSX from 'xlsx';

// the `default as` syntax.
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment} from 'moment';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter, provideMomentDateAdapter } from '@angular/material-moment-adapter';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
    selector: 'app-listar-ici-grafico-page',
    imports: [
        CommonModule,
        MatProgressSpinnerModule,
        MatExpansionModule,
        MatButtonModule,
        ComboMedicamentoComponent,
        MatMenuModule,
        MatIconModule,
        MatFormFieldModule, MatInputModule, MatDatepickerModule, FormsModule, ReactiveFormsModule, MatNativeDateModule,
        MatPaginatorModule, MatTableModule
    ],
    templateUrl: './listar-ici-grafico-page.component.html',
    styleUrl: './listar-ici-grafico-page.component.css',
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-PE' },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    ]
})
export class ListarIciGraficoPageComponent  implements OnInit {
  titulo: string = 'ICI Grafico';
  selectedRis!: any;
  selectedMaestraCategoria!: any;
  selectedMedicamento!: any;
  isLoadingResults = true;
  cabeceraVacia: boolean = true;
  displayedColumns: string[] = [];
  displayedColumns2: any[] = [];
  displayedColumns3: any[] = [];
  dataSource = new MatTableDataSource<any>([]);
  selectedAlmacen!: any;

  date: FormControl = new FormControl(moment());
  disabledDate = new FormControl({value: '', disabled: true});


  @ViewChild(MatPaginator) paginator!: MatPaginator;
  iciColumnas: string[] = ['id_almacen','almacen','id_medicamento','medicamento','tipo','campo1','suma_total','cantidad','cpma','msd','suma_4_ultimos','disponibilidad','programacion_estrategica'];


  fechas: string[] = [];

  constructor(
    private dataService: DataService,
    private dialog: MatDialog,
    private iciGraficoService: IciGraficoService,
    private cdr: ChangeDetectorRef
  ){



    this.iciColumnas = this.iciColumnas.concat(this.fechas);

    console.log(this.iciColumnas);
  }

  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);
    this.isLoadingResults = false;
  }

  onMedicamentoSeleccionado(medicamento: any): void {
    if (medicamento != null) {
      console.log(medicamento.id_medicamento);
      this.selectedMedicamento = medicamento.id_medicamento;
    }

  }


  generarExcel(): void {
    var fecha = new Date();

    const date = moment(this.date.value).format('YYYY-MM-DD');
    const arrAlmacen = this.selectedAlmacen || [];
    const parametros = { date, arrAlmacen };


    this.iciGraficoService.ici_por_producto(parametros)
    .subscribe(data => {
      const workbook = XLSX.utils.book_new();
      const worksheet = XLSX.utils.json_to_sheet(data.resultado);
      XLSX.utils.book_append_sheet(workbook, worksheet, 'Movmiento');
      XLSX.writeFile(workbook, `movimiento_almacenes_${fecha.getDate()}_${fecha.getMonth() + 1}_${fecha.getFullYear()}.xlsx`);
    });
  }

}
