import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-ici-grafico-page',
    imports: [RouterOutlet],
    templateUrl: './layout-ici-grafico-page.component.html',
    styleUrl: './layout-ici-grafico-page.component.css'
})
export class LayoutIciGraficoPageComponent {

}
