import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IciGraficoRoutingModule } from './ici-grafico-routing.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';



@NgModule({ declarations: [], imports: [CommonModule,
  IciGraficoRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class IciGraficoModule { }
