import { Injectable } from '@angular/core';
import { environments } from '../../../../../environments/environments';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, tap, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IciPorProductoService {


  private baseUrl = environments.baseUrl;
  private token = localStorage.getItem('token');
  private headers = new HttpHeaders;

  constructor(private http: HttpClient) {
    if (this.token) {
      // Encabezados de solicitud HTTP con el token
      this.headers = new HttpHeaders({
        'Authorization': `Bearer ${this.token}`
      });
    }
  }

  ici_por_producto(parametros?:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/reporte/ici/ici_por_producto/listar`,parametros,{ headers }).pipe(
        tap((porVencer) => {
          //console.log(porVencer)
        }),
        catchError(error => {
          console.error('Error al ici-por-productos:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }


}
