import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IciPorProductoRoutingModule } from './ici-por-producto-routing.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';



@NgModule({ declarations: [], imports: [CommonModule,
        IciPorProductoRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class IciPorProductoModule { }
