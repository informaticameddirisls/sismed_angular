import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-ici-por-producto-page',
    imports: [RouterOutlet],
    templateUrl: './layout-ici-por-producto-page.component.html',
    styleUrl: './layout-ici-por-producto-page.component.css'
})
export class LayoutIciPorProductoPageComponent {

}
