import { IciPorProductoService } from '../../services/ici-por-producto.service';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../../../../shared/services/data.service';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatNativeDateModule, provideNativeDateAdapter } from '@angular/material/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComboMedicamentoComponent } from '../../../../../adminsitrador/maestras/medicamentos/components/combo-medicamento/combo-medicamento.component';
import * as XLSX from 'xlsx';
import * as _moment from 'moment';
import {default as _rollupMoment} from 'moment';
import { MomentDateAdapter, provideMomentDateAdapter } from '@angular/material-moment-adapter';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { ChartModule } from 'primeng/chart';
import { Chart, registerables } from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';

Chart.register(...registerables);
Chart.register(ChartDataLabels);

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

type Disponibilidad = 'NORMOSTOCK' | 'SOBRESTOCK' | 'DESABASTECIDO' | 'SUBSTOCK' | 'SIN SALDO SIN ROTACION' | 'CON SALDO SIN ROTACION' | 'TOTAL';

@Component({
    selector: 'app-listar-ici-diario-page',
    imports: [
        CommonModule,
        MatProgressSpinnerModule,
        MatExpansionModule,
        MatButtonModule,
        ComboMedicamentoComponent,
        MatMenuModule,
        MatIconModule,
        MatFormFieldModule, MatInputModule, MatDatepickerModule, FormsModule, ReactiveFormsModule, MatNativeDateModule,
        MatPaginatorModule, MatTableModule, ChartModule
    ],
    templateUrl: './listar-ici-por-producto-page.component.html',
    styleUrl: './listar-ici-por-producto-page.component.css',
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-PE' },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    ]
})
export class ListarIciPorProductoPageComponent  implements OnInit {
  titulo: string = 'ICI POR PRODUCTO';
  selectedRis!: any;
  selectedMaestraCategoria!: any;
  selectedMedicamento!: any;
  isLoadingResults = true;
  hasSearched = false;
  cabeceraVacia: boolean = true;
  displayedColumns: string[] = [];
  displayedColumns2: any[] = [];
  displayedColumns3: any[] = [];
  dataSource = new MatTableDataSource<any>([]);
  selectedAlmacen!: any;
  datosAgrupados: { ris: string, campo1: number }[] = [];
  date: FormControl = new FormControl(moment());
  disabledDate = new FormControl({value: '', disabled: true});
  sumatoriaCampo1: number = 0;
  public chart: any;

  conteoCategorias: Record<Disponibilidad, number> = {
    'NORMOSTOCK': 0,
    'SOBRESTOCK': 0,
    'DESABASTECIDO': 0,
    'SUBSTOCK': 0,
    'SIN SALDO SIN ROTACION': 0,
    'CON SALDO SIN ROTACION': 0,
    'TOTAL': 0,
  };



  data: any;
  chartOptions: any;
  sumatoriaxmes: number[] = Array(12).fill(0);
  nomProducto: string = '';

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  iciColumnas: string[] = ['id_almacen','almacen','id_medicamento','medicamento','tipo','campo1','suma_total','cantidad','cpma','msd','suma_4_ultimos','disponibilidad','programacion_estrategica'];


  fechas: string[] = [];

  constructor(
    private dataService: DataService,
    private dialog: MatDialog,
    private iciPorProductoService: IciPorProductoService,
    private cdr: ChangeDetectorRef
  ){

    this.fechas = this.obtenerUltimos12Meses();

    this.iciColumnas = this.iciColumnas.concat(this.fechas);

    console.log(this.iciColumnas);
  }

  getLightTheme() {
    return {
      maintainAspectRatio: false,
      aspectRatio: 0.6,
      plugins: {
        datalabels: {
          color: '#000',
          anchor: 'end',
          align: 'top',
          formatter: (value: any) => value.toLocaleString(), // Muestra el valor con comas decimales
          font: {
            size: 12,
            weight: 'bold',
          }
        },
      },
      scales: {
        x: {
          ticks: {
          },
          grid: {
            drawBorder: false
          }
        },
        y: {
          ticks: {
            callback: function(value: number) {
              return value.toLocaleString(); // Agregar comas decimales
            }
          },
          grid: {
            drawBorder: false
          }
        }
      }
    };
  }

  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);
    this.isLoadingResults = false;


    this.data = {
      labels: this.obtenerUltimos12Meses(),
      datasets: [
          {
              label: `${this.nomProducto}`,
              data: this.sumatoriaxmes,
              fill: false,
              //borderColor: documentStyle.getPropertyValue('--p-cyan-500'),
              tension: 0.4
          }
      ]
  };

  this.chartOptions =  this.getLightTheme();
  }

  onMedicamentoSeleccionado(medicamento: any): void {
    if (medicamento != null) {
      console.log(medicamento, "medicamento");
      this.selectedMedicamento = medicamento.id_medicamento;
      this.nomProducto = `${medicamento.nombre} ${medicamento.concentracion}`;
    }

  }

  consultarIciDiario() {
    this.isLoadingResults = true; // Mostrar el indicador de carga
    this.hasSearched = true;
    const date = moment(this.date.value).format('YYYY-MM-DD');
    const arrMedicamento = this.selectedMedicamento || [];
    const parametros = { date, arrMedicamento };

    // Inicializar conteoCategorias antes de la llamada a subscribe
    this.conteoCategorias = {
      'NORMOSTOCK': 0,
      'SOBRESTOCK': 0,
      'DESABASTECIDO': 0,
      'SUBSTOCK': 0,
      'SIN SALDO SIN ROTACION': 0,
      'CON SALDO SIN ROTACION': 0,
      'TOTAL': 0,
    };

    this.sumatoriaxmes = Array(12).fill(0);
    this.datosAgrupados = [];
    this.sumatoriaCampo1 = 0;

    this.iciPorProductoService.ici_por_producto(parametros).subscribe(
      (response) => {
        if (response.success) {
          console.log(response.resultado, "response.resultado");




          // Filtrar los datos para incluir solo los elementos con tipo_alm igual a 'E'
          const filtrarEstablecimiento = response.resultado.filter((item: any) => item.tipo_alm === 'E');
          const filtrarAlmacen = response.resultado.filter((item: any) => item.tipo_alm === 'A');

          // Agrupar los elementos de tipo 'E' por 'ris' y sumar 'campo1'
          filtrarEstablecimiento.forEach((item: any) => {
            const existing = this.datosAgrupados.find(d => d.ris === item.ris);
            if (existing) {
              existing.campo1 += Number(item.campo1) || 0;
            } else {
              this.datosAgrupados.push({ ris: item.ris, campo1: Number(item.campo1) || 0 });
            }
          });
          filtrarAlmacen.forEach((item: any) => {
            const existing = this.datosAgrupados.find(d => d.ris === item.ris);
            if (existing) {
              existing.campo1 += Number(item.campo1) || 0;
            } else {
              this.datosAgrupados.push({ ris: item.almacen, campo1: Number(item.campo1) || 0 });
            }
          });

          // Ordenar alfabéticamente por 'ris'
          this.datosAgrupados.sort((a, b) => a.ris.localeCompare(b.ris));
          console.log(this.datosAgrupados, "datosAgrupados");

          // Calcular la sumatoria de campo1
          this.sumatoriaCampo1 = this.datosAgrupados.reduce((sum, item) => sum + item.campo1, 0);

          // Agregar la fila de sumatoria al final de datosAgrupados
          this.datosAgrupados.push({ ris: 'TOTAL', campo1: this.sumatoriaCampo1 });

          this.dataSource = new MatTableDataSource(filtrarEstablecimiento);
          this.dataSource.paginator = this.paginator;

          // Si hay resultados, combinar columnas base con las fechas
          if (response.resultado.length > 0) {
            const columnasBase = ['id_almacen','almacen','id_medicamento','medicamento','tipo'];

            const columnasBase2 = ['campo1','cpma','msd','disponibilidad'];


            this.displayedColumns = [...columnasBase, ...this.fechas, ...columnasBase2]; // Mantén las columnas de fechas fijas
            //console.log(response.resultado, "this.response.resultado");
            filtrarEstablecimiento.forEach((item: any) => {
              this.sumatoriaxmes[0] += Number(item.m1) || 0;
              this.sumatoriaxmes[1] += Number(item.m2) || 0;
              this.sumatoriaxmes[2] += Number(item.m3) || 0;
              this.sumatoriaxmes[3] += Number(item.m4) || 0;
              this.sumatoriaxmes[4] += Number(item.m5) || 0;
              this.sumatoriaxmes[5] += Number(item.m6) || 0;
              this.sumatoriaxmes[6] += Number(item.m7) || 0;
              this.sumatoriaxmes[7] += Number(item.m8) || 0;
              this.sumatoriaxmes[8] += Number(item.m9) || 0;
              this.sumatoriaxmes[9] += Number(item.m10) || 0;
              this.sumatoriaxmes[10] += Number(item.m11) || 0;
              this.sumatoriaxmes[11] += Number(item.m12) || 0;

              if (typeof item.disponibilidad === 'string' && item.disponibilidad in this.conteoCategorias) {
                this.conteoCategorias[item.disponibilidad as Disponibilidad]++;
              }
            });
            this.conteoCategorias['TOTAL'] = this.conteoCategorias['NORMOSTOCK'] + this.conteoCategorias['SOBRESTOCK'] + this.conteoCategorias['DESABASTECIDO'] + this.conteoCategorias['SUBSTOCK'] + this.conteoCategorias['SIN SALDO SIN ROTACION'] + this.conteoCategorias['CON SALDO SIN ROTACION'];
            this.updateChart();
            console.log(this.conteoCategorias, "this.conteoCategorias");

            // Si necesitas verificar que las fechas están en la respuesta, puedes hacerlo aquí
          }
        } else {
          console.error('Error al listar ici diario:', response);
        }
        this.isLoadingResults = false; // Ocultar el indicador de carga
      },
      (error) => {
        console.error('Error al listar ici diario:', error);
        this.isLoadingResults = false;
      }
    );
  }

  updateChart() {
    this.data = {
      labels: this.obtenerUltimos12Meses(),
      datasets: [
          {
              label: `${this.nomProducto}`,
              data: this.sumatoriaxmes,
              fill: false,
              //borderColor: documentStyle.getPropertyValue('--p-cyan-500'),
              tension: 0.4
          }
      ]
    };
    this.chartOptions = this.getLightTheme();
    this.cdr.detectChanges(); // Detectar cambios para actualizar el gráfico
  }

  obtenerUltimos12Meses(): string[] {
    const fechas: string[] = [];
    const fechaActual = new Date();

    for (let i = 0; i < 12; i++) {
      const fecha = new Date(fechaActual.getFullYear(), fechaActual.getMonth() - i);
      const anio = fecha.getFullYear();
      const mes = String(fecha.getMonth() + 1).padStart(2, '0'); // Asegúrate de que el mes tenga dos dígitos
      fechas.push(`${anio}${mes}`); // Formato YYYYMM
    }

    return fechas.reverse(); // Para tener las fechas del más antiguo al más reciente
  }


  generarExcel(): void {
    var fecha = new Date();

    const date = moment(this.date.value).format('YYYY-MM-DD');
    const arrAlmacen = this.selectedAlmacen || [];
    const parametros = { date, arrAlmacen };


    this.iciPorProductoService.ici_por_producto(parametros)
    .subscribe(data => {
      const workbook = XLSX.utils.book_new();
      const worksheet = XLSX.utils.json_to_sheet(data.resultado);
      XLSX.utils.book_append_sheet(workbook, worksheet, 'Movmiento');
      XLSX.writeFile(workbook, `movimiento_almacenes_${fecha.getDate()}_${fecha.getMonth() + 1}_${fecha.getFullYear()}.xlsx`);
    });
  }

}
