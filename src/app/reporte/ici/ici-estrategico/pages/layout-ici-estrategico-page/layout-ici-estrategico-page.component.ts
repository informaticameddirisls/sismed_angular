import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-layout-ici-estrategico-page',
  standalone: true,
  imports: [RouterOutlet],
  templateUrl: './layout-ici-estrategico-page.component.html',
  styleUrl: './layout-ici-estrategico-page.component.css'
})
export class LayoutIciEstrategicoPageComponent {

}
