import { IciEstrategicoService } from '../../services/ici-estrategico.service';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../../../../shared/services/data.service';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatNativeDateModule, provideNativeDateAdapter } from '@angular/material/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComboAlmacenComponent } from '../../../../../adminsitrador/maestras/almacenes/components/combo-almacen/combo-almacen.component';
import * as XLSX from 'xlsx';

// the `default as` syntax.
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment} from 'moment';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter, provideMomentDateAdapter } from '@angular/material-moment-adapter';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { ComboEstrategiaComponent } from "../../../../../adminsitrador/maestras/estrategia/components/combo-estrategia/combo-estrategia.component";

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
    selector: 'app-listar-ici-estrategico-page',
    imports: [
        CommonModule,
        MatProgressSpinnerModule,
        MatExpansionModule,
        MatButtonModule,

        MatMenuModule,
        MatIconModule,
        MatFormFieldModule, MatInputModule, MatDatepickerModule, FormsModule, ReactiveFormsModule, MatNativeDateModule,
        MatPaginatorModule, MatTableModule,
        ComboEstrategiaComponent
    ],
    templateUrl: './listar-ici-estrategico-page.component.html',
    styleUrl: './listar-ici-estrategico-page.component.css',
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-PE' },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    ]
})
export class ListarIciEstrategicoPageComponent  implements OnInit {
  titulo: string = 'ICI ESTRATEGIA';
  selectedRis!: any;
  selectedMaestraCategoria!: any;
  selectedMedicamento!: any;
  isLoadingResults = false; // Inicialmente no está cargando
  hasSearched = false; // Inicialmente no se ha realizado ninguna búsqueda
  cabeceraVacia: boolean = true;
  displayedColumns: string[] = [];
  displayedColumns2: any[] = [];
  displayedColumns3: any[] = [];
  dataSource = new MatTableDataSource<any>([]);
  selectedEstrategia!: any;

  date: FormControl = new FormControl(moment());
  disabledDate = new FormControl({value: '', disabled: true});


  @ViewChild(MatPaginator) paginator!: MatPaginator;
  iciColumnas: string[] = ['id_almacen','almacen','id_medicamento','medicamento','tipo','campo1','suma_total','cantidad','cpma','msd','suma_4_ultimos','disponibilidad','programacion_estrategica'];


  fechas: string[] = [];

  constructor(
    private dataService: DataService,
    private dialog: MatDialog,
    private iciDiarioService: IciEstrategicoService,
    private cdr: ChangeDetectorRef
  ){

    this.fechas = this.obtenerUltimos12Meses();

    this.iciColumnas = this.iciColumnas.concat(this.fechas);

    console.log(this.iciColumnas);
  }

  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);
    this.isLoadingResults = false;
  }

  onEstrategiaSeleccionado(estrategia: any): void {
    this.selectedEstrategia = estrategia;
  }

  consultarIciDiario() {
    this.isLoadingResults = true; // Inicia la carga
    this.hasSearched = true; // Marca que se ha realizado una búsqueda

    const date = moment(this.date.value).format('YYYY-MM-DD');
    const arrEstrategia = this.selectedEstrategia || [];
    const parametros = { date, arrEstrategia };

    this.iciDiarioService.ici_estrategico(parametros).subscribe(
      (response) => {
        if (response.success) {
          this.dataSource = new MatTableDataSource(response.resultado);
          this.dataSource.paginator = this.paginator;

          // Si hay resultados, combinar columnas base con las fechas
          if (response.resultado.length > 0) {
            const columnasBase = ['id_almacen','almacen','estrategias','id_medicamento','medicamento','tipo'];

            const columnasBase2 = ['campo1','cpma','msd','disponibilidad'];


            this.displayedColumns = [...columnasBase, ...this.fechas, ...columnasBase2]; // Mantén las columnas de fechas fijas
            console.log(this.displayedColumns);
            // Si necesitas verificar que las fechas están en la respuesta, puedes hacerlo aquí
          }
        } else {
          console.error('Error al listar ici diario:', response);
        }
        this.isLoadingResults = false; // Finaliza la carga
      },
      (error) => {
        console.error('Error al listar ici diario:', error);
        this.isLoadingResults = false; // Finaliza la carga en caso de error
      }
    );
  }


  obtenerUltimos12Meses(): string[] {
    const fechas: string[] = [];
    const fechaActual = new Date();

    for (let i = 0; i < 12; i++) {
      const fecha = new Date(fechaActual.getFullYear(), fechaActual.getMonth() - i);
      const anio = fecha.getFullYear();
      const mes = String(fecha.getMonth() + 1).padStart(2, '0'); // Asegúrate de que el mes tenga dos dígitos
      fechas.push(`${anio}${mes}`); // Formato YYYYMM
    }

    return fechas.reverse(); // Para tener las fechas del más antiguo al más reciente
  }


  generarExcel(): void {
    var fecha = new Date();

    const date = moment(this.date.value).format('YYYY-MM-DD');
    const arrAlmacen = this.selectedEstrategia || [];
    const parametros = { date, arrAlmacen };


    this.iciDiarioService.ici_estrategico(parametros)
    .subscribe(data => {
      const workbook = XLSX.utils.book_new();
      const worksheet = XLSX.utils.json_to_sheet(data.resultado);
      XLSX.utils.book_append_sheet(workbook, worksheet, 'Movmiento');
      XLSX.writeFile(workbook, `movimiento_almacenes_${fecha.getDate()}_${fecha.getMonth() + 1}_${fecha.getFullYear()}.xlsx`);
    });
  }



  graficoDisponibilidad(){
    console.log(this.dataSource);
  }

}
