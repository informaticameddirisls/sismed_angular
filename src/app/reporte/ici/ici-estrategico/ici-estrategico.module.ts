import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IciEstrategicoRoutingModule } from './ici-estrategico-routing.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';



@NgModule({ declarations: [], imports: [CommonModule,
        IciEstrategicoRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class IciEstrategicoModule { }
