import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';
import { ListarIciEstrategicoPageComponent } from './pages/listar-ici-estrategico-page/listar-ici-estrategico-page.component';

const routes: Routes = [
  {

    path: 'ici_estrategico',
    children:
      [
        {
          path: 'listar',
          component: ListarIciEstrategicoPageComponent,
          canActivate: [AuthGuard]
        }
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IciEstrategicoRoutingModule { }
