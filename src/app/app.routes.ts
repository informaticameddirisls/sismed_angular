
import { Routes } from '@angular/router';
import { LayoutPageComponent } from './shared/pages/layout-page/layout-page.component';



export const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./shared/shared.module').then(m => m.SharedModule),
        // Puedes agregar rutas adicionales a nivel de la aplicación aquí si es necesario
        // Ejemplo: { path: 'inicio', component: InicioComponent }
        //loadChildren: () => SharedModule,
    },
    { path: '**',
      redirectTo: '',
      pathMatch: 'full'
    },

];
