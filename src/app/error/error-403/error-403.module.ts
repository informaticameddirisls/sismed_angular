import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../material/material.module';
import { Error403RoutingModule } from './error-403-routing.module';



@NgModule({ declarations: [], imports: [CommonModule,
        Error403RoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class Error403Module { }
