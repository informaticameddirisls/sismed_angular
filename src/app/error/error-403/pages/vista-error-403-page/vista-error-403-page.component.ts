import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../../shared/services/data.service';

@Component({
    selector: 'app-vista-error-403-page',
    imports: [],
    templateUrl: './vista-error-403-page.component.html',
    styleUrl: './vista-error-403-page.component.css'
})
export class VistaError403PageComponent implements OnInit {

  titulo: string = 'ERROR 403';

  constructor(
    private dataService: DataService,
  ){}


  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);
  }

}
