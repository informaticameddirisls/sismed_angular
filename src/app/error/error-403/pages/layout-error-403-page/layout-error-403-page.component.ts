import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-error-403-page',
    imports: [RouterOutlet],
    templateUrl: './layout-error-403-page.component.html',
    styleUrl: './layout-error-403-page.component.css'
})
export class LayoutError403PageComponent {

}
