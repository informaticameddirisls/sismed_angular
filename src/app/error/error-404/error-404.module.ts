import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Error404RoutingModule } from './error-404-routing.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../material/material.module';



@NgModule({ declarations: [], imports: [CommonModule,
        Error404RoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class Error404Module { }
