import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../auth/guards/auth.guard';
import { VistaError404PageComponent } from './pages/vista-error-404-page/vista-error-404-page.component';
//import { AuthGuard } from '../../../auth/guards/auth.guard';

//import { ListarKitsCocheDeParoPageComponent } from './pages/listar-kits-coche-de-paro-page/listar-kits-coche-de-paro-page.component';

const routes: Routes = [
  {

    path: '404',
    children:
      [
        {
          path: 'page',
          component: VistaError404PageComponent
        },
        {
          path: '**',
          redirectTo: 'page', // Redirige a la página de error 404
          pathMatch: 'full'
        }
      ]
  },
  {
    path: '**',
    redirectTo: '404/page', // Redirige a la página de error 404
    pathMatch: 'full'
  }
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Error404RoutingModule { }
