import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-error-404-page',
    imports: [RouterOutlet],
    templateUrl: './layout-error-404-page.component.html',
    styleUrl: './layout-error-404-page.component.css'
})
export class LayoutError404PageComponent {

}
