import { Component, EventEmitter, Inject, Input, Output } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MAT_DIALOG_DATA, MatDialogActions, MatDialogClose, MatDialogContent, MatDialogRef, MatDialogTitle } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';

@Component({
    selector: 'shared-dialog-eliminar',
    imports: [MatButtonModule, MatCardModule, MatIconModule],
    templateUrl: './dialog-eliminar.component.html',
    styleUrl: './dialog-eliminar.component.css'
})
export class DialogEliminarComponent {

  @Input() registro: any;
  @Output() eliminarConfirmado: EventEmitter<boolean> = new EventEmitter<boolean>();


  constructor(
    public dialogRef: MatDialogRef<DialogEliminarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {

  }


  confirmarEliminar(): void {
    this.eliminarConfirmado.emit(true); // Emitir true para confirmar eliminación
    this.dialogRef.close(); // Cerrar el diálogo
  }

  cancelarEliminar(): void {
    this.eliminarConfirmado.emit(false); // Emitir false para cancelar eliminación
    this.dialogRef.close(); // Cerrar el diálogo
  }

}
