import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MaterialModule } from '../../../material/material.module';
import {MatIconModule} from '@angular/material/icon';
import { UserDropdownMenuComponent } from '../user-dropdown-menu/user-dropdown-menu.component';
import { SidebarService } from '../../services/sidebar.service';

@Component({
    selector: 'shared-toolbar-menu',
    imports: [MaterialModule, UserDropdownMenuComponent],
    templateUrl: './toolbar-menu.component.html',
    styleUrl: './toolbar-menu.component.css'
})
export class ToolbarMenuComponent {

  @Output() toggleSidebar = new EventEmitter<void>();

  constructor() { }

  onToggleSidebar(): void {
    this.toggleSidebar.emit();
  }

}
