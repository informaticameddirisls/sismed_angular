import { Component } from '@angular/core';
import { Router, RouterLink, RouterModule, RouterOutlet } from '@angular/router';
import { MaterialModule } from '../../../material/material.module';
import { AuthService } from '../../../auth/services/auth.service';
import { Usuario } from '../../../auth/interfaces/Usuario.interface';
import { CommonModule } from '@angular/common';
import { FirstLetterPipe } from '../../pipes/first-letter.pipe';

@Component({
    selector: 'shared-user-dropdown-menu',
    imports: [MaterialModule, CommonModule, FirstLetterPipe],
    templateUrl: './user-dropdown-menu.component.html',
    styleUrl: './user-dropdown-menu.component.css'
})
export class UserDropdownMenuComponent {
  usuario?:any;
  constructor(
    private authService: AuthService,
    private router: Router,
  ){

    this.usuario = this.authService.currentUser;

  }

  logout(){
    console.log("Cerrando sesión...");

    try {
      this.authService.logout();
      console.log("Sesión cerrada correctamente.");
      console.log("Antes de la redirección...");
      //window.location.href = '/sismed/auth/login';
      this.router.navigate(['sismed', 'auth', 'login']);

      window.location.reload();
      console.log("Después de la redirección...");
    } catch (error) {
      console.error("Error al cerrar sesión:", error);
    }
  }
}
