import { Component, HostListener, Renderer2, ElementRef, OnInit, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { MaterialModule } from '../../../material/material.module';
import { MatListModule } from "@angular/material/list";
import { MatExpansionModule } from '@angular/material/expansion';
import { CommonModule } from '@angular/common';
import { MenuService } from '../../services/menu.service';
import { Router, RouterModule } from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { DataService } from '../../services/data.service';


@Component({
    selector: 'shared-sidebar-menu',
    templateUrl: './sidebar-menu.component.html',
    styleUrl: './sidebar-menu.component.css',
    imports: [MaterialModule, MatListModule, MatExpansionModule, CommonModule, RouterModule]
})
export class SidebarMenuComponent implements OnInit {
  menu: any[] = [];
  showSubMenuIndex: number = -1;
  isSidebarExpanded: boolean = true;
  @Output() toggleEvent = new EventEmitter<boolean>();
  @Output() loadingEvent = new EventEmitter<boolean>();


  constructor(
    private menuService: MenuService,
    private router: Router,
    private dataService: DataService,
    private globalService:GlobalService
  ) {

    this.menuService.ListarMenu().subscribe(
      (menus) => {
        if (menus && menus != undefined) {
          this.menu = Object.values(menus)[0];

          var j = 0;
          //mostrar el menu desplegado al terminar de cargar la web
          this.menu.map((sp: any) => {
            sp.modulo.map((m: any, i: number) => {
              var path = m.ruta_modulo;
              var currentRoute = this.router.url;

              if (currentRoute.includes(path)) {

                this.toggleSubMenu(path,false);
              }
              j = j + 1;
            })
          })

        }
      },
      (err) => {
        console.error('Error al obtener los menús:', err);
      }
    );
  }

  obtenerRutaCompleta(ruta: string): string {
    //console.log(this.router.url);
    //console.log(ruta);
    return ruta;
  }

  isActive(ruta: string): boolean {
    //console.log(ruta,this.router.url.includes(ruta))
    return this.router.url.includes(ruta);
  }

  ngOnInit(): void {}

  toggleSubMenu(rutaModulo: string, act:boolean) {

    this.menu.forEach(supermodulo => {
      // Recorrer los elementos de modulo
      supermodulo.modulo.forEach((modulo:any) => {
        // Verificar si la ruta del modulo coincide con la ruta del modulo clickeado
        if (modulo.ruta_modulo === rutaModulo && act==true) {

          modulo.isActive = false;
        }else{
          modulo.isActive = (modulo.ruta_modulo === rutaModulo);

        }

      });
    });
  }

  expandSidebar() {
    this.isSidebarExpanded = true;
  }

  collapseSidebar(id?:any) {
    console.log(id)
    this.dataService.setLoadingEvent(true);
    this.toggleEvent.emit(true);
    this.isSidebarExpanded = false;
    console.log("12322")
  }
}
