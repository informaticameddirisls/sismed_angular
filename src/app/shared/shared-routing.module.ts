import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutPageComponent } from './pages/layout-page/layout-page.component';
import { LayoutAuthPageComponent } from '../auth/pages/layout-auth-page/layout-auth-page.component';
import { LayoutVerificadorPageComponent } from '../adminsitrador/maestras/verificador/pages/layout-verificador-page/layout-verificador-page.component';
import { LayoutSupermoduloPageComponent } from '../adminsitrador/maestras/supermodulos/pages/layout-supermodulo-page/layout-supermodulo-page.component';
import { LayoutModuloPageComponent } from '../adminsitrador/maestras/modulos/pages/layout-modulo-page/layout-modulo-page.component';
import { LayoutUsuarioPageComponent } from '../usuarios/pages/layout-usuario-page/layout-usuario-page.component';
import { LayoutSubmoduloPageComponent } from '../adminsitrador/maestras/submodulos/pages/layout-submodulo-page/layout-submodulo-page.component';
import { LayoutPermisoPageComponent } from '../adminsitrador/maestras/permisos/pages/layout-permiso-page/layout-permiso-page.component';
import { LayoutStockMedicamentoPageComponent } from '../consulta/stock/medicamento/pages/layout-stock-medicamento-page/layout-stock-medicamento-page.component';
import { LayoutCuadroResumenPageComponent } from '../consulta/stock/cuadro_resumen/pages/layout-cuadro-resumen-page/layout-cuadro-resumen-page.component';
import { LayoutLotesPorVencerPageComponent } from '../consulta/stock/lotes-por-vencer/pages/layout-lotes-por-vencer-page/layout-lotes-por-vencer-page.component';
import { LayoutMovAlmacenesPageComponent } from '../consulta/movimientos/almacenes/pages/layout-mov-almacenes-page/layout-mov-almacenes-page.component';
import { LayoutMovEstablecimientosPageComponent } from '../consulta/movimientos/establecimientos/pages/layout-mov-establecimientos-page/layout-mov-establecimientos-page.component';
import { LayoutKitsBotiquinPageComponent } from '../consulta/kits/botiquin/pages/layout-kits-botiquin-page/layout-kits-botiquin-page.component';
import { LayoutKitsClavesPageComponent } from '../consulta/kits/claves/pages/layout-kits-claves-page/layout-kits-claves-page.component';
import { LayoutKitsCocheDeParoPageComponent } from '../consulta/kits/coche_de_paro/pages/layout-kits-coche-de-paro-page/layout-kits-coche-de-paro-page.component';
import { LayoutKitDePartoPageComponent } from '../consulta/kits/kit_de_parto/pages/layout-kit-de-parto-page/layout-kit-de-parto-page.component';
import { LayoutRecienNacidoPageComponent } from '../consulta/kits/kit_de_recien_nacido/pages/layout-kit-de-recien-nacido-page/layout-kit-de-recien-nacido-page.component';
import { LayoutKitsPageComponent } from '../consulta/kits/kits/pages/layout-kits-page/layout-kits-page.component';
import { LayoutKitsDenguePageComponent } from '../consulta/kits/dengue/pages/layout-kits-dengue-page/layout-kits-dengue-page.component';
import { LayoutIciDiarioPageComponent } from '../reporte/ici/ici-diario/pages/layout-ici-diario-page/layout-ici-diario-page.component';
import { LayoutError404PageComponent } from '../error/error-404/pages/layout-error-404-page/layout-error-404-page.component';
import { LayoutError403PageComponent } from '../error/error-403/pages/layout-error-403-page/layout-error-403-page.component';
import { LoginPageComponent } from '../auth/pages/login-page/login-page.component';
import { PublicGuard } from '../auth/guards/public.guard';
import { LayoutMiCuentaUsuarioPerfilComponent } from '../mi-cuenta/usuario/perfil/pages/layout-mi-cuenta-usuario-perfil/layout-mi-cuenta-usuario-perfil.component';
import { LayoutKitEscenarioIIIPageComponent } from '../consulta/kits/kit_escenario_iii/pages/layout-kit-escenario-iii-page/layout-kit-escenario-iii-page.component';
import { LayoutKitEnCasoDeViolenciaSexualPageComponent } from '../consulta/kits/kit_en_caso_de_violencia_sexual/pages/layout-kit-en-caso-de-violencia-sexual-page/layout-kit-en-caso-de-violencia-sexual-page.component';
import { LayoutIciPorProductoPageComponent } from '../reporte/ici/ici-por-producto/pages/layout-ici-por-producto-page/layout-ici-por-producto-page.component';
import { LayoutResolucionPageComponent } from '../adminsitrador/evaluacion/resolucion/pages/layout-resolucion-page/layout-resolucion-page.component';
import { LayoutEvaluacionPageComponent } from '../adminsitrador/evaluacion/evaluacion/pages/layout-evaluacion-page/layout-evaluacion-page.component';
import { LayoutBienvenidosPageComponent } from '../bienvenidos/pages/layout-bienvenidos-page/layout-bienvenidos-page.component';
import { AuthGuard } from '../auth/guards/auth.guard';
import { LayoutIciGraficoPageComponent } from '../reporte/ici/ici-grafico/pages/layout-ici-grafico-page/layout-ici-grafico-page.component';



const routes: Routes = [
  {
    path: 'sismed/auth/login',
    component: LoginPageComponent,
    canActivate: [ PublicGuard ],
  },
  //  evaluacion
  {
    path: 'sismed/administrador/evaluacion',
    component: LayoutResolucionPageComponent,
    loadChildren: () => import('../adminsitrador/evaluacion/resolucion/resolucion.module').then(m => m.ResolucionModule),
  },

  {
    path: 'sismed',
    component: LayoutPageComponent,
    children: [
      {
        path: 'bienvenidos',
        component: LayoutBienvenidosPageComponent,
        loadChildren: () => import('../bienvenidos/bienvenidos.module').then(m => m.BienvenidosModule),
        //canActivate: [AuthGuard]
      },
      // {
      //   path: 'auth',
      //   component: LayoutAuthPageComponent,
      //   loadChildren: () => import('../auth/auth.module').then(m => m.AuthModule),
      // },
      {
        path: 'administrador/maestra',
        component: LayoutVerificadorPageComponent,
        loadChildren: () => import('../adminsitrador/maestras/verificador/verificador.module').then(m => m.VerificadorModule),
      },
      {
        path: 'administrador/maestra',
        component: LayoutSupermoduloPageComponent,
        loadChildren: () => import('../adminsitrador/maestras/supermodulos/supermodulo.module').then(m => m.SupermoduloModule),
      },
      {
        path: 'administrador/maestra',
        component: LayoutModuloPageComponent,
        loadChildren: () => import('../adminsitrador/maestras/modulos/modulo.module').then(m => m.ModuloModule),
      },
      {
        path: 'administrador/maestra',
        component: LayoutUsuarioPageComponent,
        loadChildren: () => import('../usuarios/usuario.module').then(m => m.UsuarioModule),
      },
      {
        path: 'administrador/maestra',
        component: LayoutSubmoduloPageComponent,
        loadChildren: () => import('../adminsitrador/maestras/submodulos/submodulo.module').then(m => m.SubmoduloModule),
      },
      {
        path: 'administrador/maestra',
        component: LayoutPermisoPageComponent,
        loadChildren: () => import('../adminsitrador/maestras/permisos/permiso.module').then(m => m.PermisoModule),
      },

      {
        path: 'administrador/evaluacion',
        component: LayoutEvaluacionPageComponent,
        loadChildren: () => import('../adminsitrador/evaluacion/evaluacion/evaluacion.module').then(m => m.EvaluacionModule),
      },


      {
        path: 'consulta/stock',
        component: LayoutStockMedicamentoPageComponent,
        loadChildren: () => import('../consulta/stock/medicamento/medicamento.module').then(m => m.MedicamentoModule),
      },
      {
        path: 'consulta/stock',
        component: LayoutCuadroResumenPageComponent,
        loadChildren: () => import('../consulta/stock/cuadro_resumen/cuadro-resumen.module').then(m => m.CuadroResumenModule),
      },
      {
        path: 'consulta/stock',
        component: LayoutLotesPorVencerPageComponent,
        loadChildren: () => import('../consulta/stock/lotes-por-vencer/lotes-por-vencer.module').then(m => m.LotesPorVencerModule),
      },
      {
        path: 'consulta/movimientos',
        component: LayoutMovAlmacenesPageComponent,
        loadChildren: () => import('../consulta/movimientos/almacenes/mov-almacenes.module').then(m => m.MovAlmacenesModule),
      },
      {
        path: 'consulta/movimientos',
        component: LayoutMovEstablecimientosPageComponent,
        loadChildren: () => import('../consulta/movimientos/establecimientos/mov-establecimientos.module').then(m => m.MovEstablecimientosModule),
      },
      {
        path: 'consulta/kits',
        component: LayoutKitsBotiquinPageComponent,
        loadChildren: () => import('../consulta/kits/botiquin/botiquin.module').then(m => m.BotiquinModule),
      },
      {
        path: 'consulta/kits',
        component: LayoutKitsClavesPageComponent,
        loadChildren: () => import('../consulta/kits/claves/claves.module').then(m => m.ClavesModule),
      },
      {
        path: 'consulta/kits',
        component: LayoutKitsCocheDeParoPageComponent,
        loadChildren: () => import('../consulta/kits/coche_de_paro/coche_de_paro.module').then(m => m.CocheDeParoModule),
      },
      {
        path: 'consulta/kits',
        component: LayoutKitDePartoPageComponent,
        loadChildren: () => import('../consulta/kits/kit_de_parto/kit-de-parto.module').then(m => m.KitDePartoModule),
      },
      {
        path: 'consulta/kits',
        component: LayoutRecienNacidoPageComponent,
        loadChildren: () => import('../consulta/kits/kit_de_recien_nacido/kit-de-recien-nacido.module').then(m => m.RecienNacidoModule),
      },
      {
        path: 'consulta/kits',
        component: LayoutKitEnCasoDeViolenciaSexualPageComponent,
        loadChildren: () => import('../consulta/kits/kit_en_caso_de_violencia_sexual/kit-en-caso-de-violencia-sexual.module').then(m => m.KitEnCasoDeViolenciaSexualModule),
      },
      {
        path: 'consulta/kits',
        component: LayoutKitsPageComponent,
        loadChildren: () => import('../consulta/kits/kits/kits.module').then(m => m.KitsModule),
      },
      {
        path: 'consulta/kits',
        component: LayoutKitsDenguePageComponent,
        loadChildren: () => import('../consulta/kits/dengue/dengue.module').then(m => m.DengueModule),
      },
      {
        path: 'consulta/kits',
        component: LayoutKitEscenarioIIIPageComponent,
        loadChildren: () => import('../consulta/kits/kit_escenario_iii/kit-escenario-iii.module').then(m => m.KitEscenarioIIIModule),
      },
      {
        path: 'reporte/ici',
        component: LayoutIciDiarioPageComponent,
        loadChildren: () => import('../reporte/ici/ici-diario/ici-diario.module').then(m => m.IciDiarioModule),
      },
      {
        path: 'reporte/ici',
        component: LayoutIciDiarioPageComponent,
        loadChildren: () => import('../reporte/ici/ici-estrategico/ici-estrategico.module').then(m => m.IciEstrategicoModule),
      },
      {
        path: 'reporte/ici',
        component: LayoutIciPorProductoPageComponent,
        loadChildren: () => import('../reporte/ici/ici-por-producto/ici-por-producto.module').then(m => m.IciPorProductoModule),
      },
      {
        path: 'reporte/ici',
        component: LayoutIciGraficoPageComponent,
        loadChildren: () => import('../reporte/ici/ici-grafico/ici-grafico.module').then(m => m.IciGraficoModule),
      },
      {
        path: 'libro/libro_de_control',
        component: LayoutIciDiarioPageComponent,
        loadChildren: () => import('../libro/libro-de-control/estupefaciente-iia/estupefaciente-iia.module').then(m => m.EstupefacienteIiaModule),
      },
      {
        path: 'libro/libro_de_control',
        component: LayoutIciDiarioPageComponent,
        loadChildren: () => import('../libro/libro-de-control/psicotropico-lista-ivb/psicotropico-lista-ivb.module').then(m => m.PsicotropicoListaIvbModule),
      },
      {
        path: 'libro/libro_de_control',
        component: LayoutIciDiarioPageComponent,
        loadChildren: () => import('../libro/libro-de-control/psicotropico-iiia-iib-iic/psicotropico-iiia-iib-iic.module').then(m => m.PsicotropicoIiiaIibIicModule),
      },
      {
        path: 'mi_cuenta/usuario',
        component: LayoutMiCuentaUsuarioPerfilComponent,
        loadChildren: () => import('../mi-cuenta/usuario/perfil/mi-cuenta-usuario-perfil.module').then(m => m.MiCuentaUsuarioPerfilModule),
      },
      {
        path: 'mi_cuenta/usuario',
        component: LayoutMiCuentaUsuarioPerfilComponent,
        loadChildren: () => import('../mi-cuenta/usuario/perfil/mi-cuenta-usuario-perfil.module').then(m => m.MiCuentaUsuarioPerfilModule),
      },


      {
        path: 'error',
        component: LayoutError403PageComponent,
        loadChildren: () => import('../error/error-403/error-403.module').then(m => m.Error403Module),
      },
      {
         path: 'error',
         component: LayoutError404PageComponent,
         loadChildren: () => import('../error/error-404/error-404.module').then(m => m.Error404Module),
      },

      //{ path: '**', redirectTo: 'auth', pathMatch: 'full' },
    ],
  },
   {
     path: '**',
     redirectTo: '/sismed/auth/login',
     pathMatch: 'full'
   }
  //{ path: '**', redirectTo: 'sismed', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedRoutingModule { }
