import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';

@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  constructor() { }

  exportToExcel(htmlTableId: string, fileName: string): void {
    const targetTable = document.getElementById(htmlTableId);
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(targetTable);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, `${fileName}.xlsx`);
  }

}
