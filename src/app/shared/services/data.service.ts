import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private pageTitleSource: BehaviorSubject<string> = new BehaviorSubject<string>('');
  private loadingEvent: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  setPageTitle(title: string): void {
    this.pageTitleSource.next(title);
  }

  getPageTitle(): Observable<string> {
    return this.pageTitleSource.asObservable();
  }


  setLoadingEvent(data: boolean) {
    this.loadingEvent.next(data);
  }

  getLoadingEvent() {
    return this.loadingEvent.asObservable();
  }
}
