import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  private idSubmodulo: number = 0;

  constructor() { }

  setSelectedItemId(itemId: number) {
    this.idSubmodulo = itemId;
  }

  getSelectedItemId(): number {
    return this.idSubmodulo;
  }
}
