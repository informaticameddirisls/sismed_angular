import { DebugElement, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import Dexie from 'dexie';

import { environments } from '../../../environments/environments';
import { Menu } from '../interfaces/Menu.interface';
import { AuthService } from '../../auth/services/auth.service';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class MenuService {

  private baseUrl = environments.baseUrl;
  private db: Dexie;
  private menuTable?: Dexie.Table<any, string>;

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private router: Router
  ) {
    this.db = new Dexie('myDatabase');

    this.db.version(1).stores({
      menuTable: '++id, menu'
    });
    this.menuTable = this.db.table('menuTable');

  }

  async guardarMenu(menuData: any[]): Promise<void> {

    try {
      //console.log(menuData);
      if (this.menuTable) {
        const existingMenu = await this.menuTable.toArray();

        if (existingMenu && existingMenu.length > 0) {
          console.log("Ya existe menú en la base de datos.");
        } else {
          console.log("No hay menú en la base de datos. Guardando...");
          await this.menuTable.add({ menu: menuData });
          console.log("Menú guardado correctamente.");
        }
      }
    } catch (error) {
      console.error('Error al guardar menú en IndexedDB:', error);
    }
  }


async asexisteMenu(): Promise<boolean> {
  try {
    if (this.menuTable) {
      const existingMenu = await this.menuTable.toArray();
      if (existingMenu && existingMenu.length > 0) {
        return true;
      }
    }
    return false;
  } catch (error) {
    console.error('Error al obtener los menús:', error);
    return false;
  }
}

  existeMenu(): Promise<boolean> {

    return this.asexisteMenu()
      .then(result => {
        return result;
      })
      .catch(error => {
        console.error('Error al verificar la existencia del menú:44', error);
        return false;
      });
  }


  obtenerMenu(): Promise<any[]> {

    if (this.menuTable) {
      return this.menuTable.toArray();
    } else {
      console.error('La tabla de menús no está inicializada correctamente.');
      return Promise.reject('Error: la tabla de menús no está inicializada correctamente.');
    }
  }


  private handleMenuError(error: any): Observable<any> {
    console.error('Error al obtener o agregar menú en IndexedDB:', error);
    return of({});
  }

  ListarMenu(): Observable<any[] | undefined> {
    if (this.authService.checkAuthentication() && typeof localStorage !== 'undefined') {
      const token = localStorage.getItem('token');
      if (token) {
        return this.http.post<any[]>(`${this.baseUrl}/api/sesion/listarMenu`, { token }).pipe(
          tap((menu) =>
            {
              this.guardarMenu(menu).catch(error => this.handleMenuError(error))
              return menu;

            }

            ),
          catchError(error => this.handleMenuError(error))
        );
      } else {
        this.router.navigate(['sismed', 'auth', 'login']);
        console.error('No se encontró el token de autenticación en el localStorage.');
        return of(undefined); // Devolver undefined si no hay token
      }
    } else {
      return of(undefined); // Devolver undefined si no se cumple la autenticación o no hay localStorage
    }
  }

}
