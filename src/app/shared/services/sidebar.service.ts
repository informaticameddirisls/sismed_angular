import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, tap, of, map, catchError, BehaviorSubject } from 'rxjs';

import { environments } from '../../../environments/environments';
import { isPlatformBrowser } from '@angular/common';
import { Menu } from '../interfaces/Menu.interface';

@Injectable({providedIn: 'root'})


export class SidebarService {
  private isSidebarOpenSubject = new BehaviorSubject<boolean>(false);
  isSidebarOpen$ = this.isSidebarOpenSubject.asObservable();

  constructor() { }

  toggleSidebar() {
    this.isSidebarOpenSubject.next(!this.isSidebarOpenSubject.value);
  }
}
