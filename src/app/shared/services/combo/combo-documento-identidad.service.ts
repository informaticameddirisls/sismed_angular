import { Injectable } from '@angular/core';
import { environments } from '../../../../environments/environments';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError, Observable, tap, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComboDocumentoIdentidadService {
  private baseUrl = environments.baseUrl;



  constructor(private http: HttpClient,
    private _snackBar: MatSnackBar
  )
  {

  }

  listarDocIdentidad(): Observable<any> {


      return this.http.get<any>(`${this.baseUrl}/api/administrador/maestra/maestra_categoria/lista_documento_identidad_combo`, {  }).pipe(
        tap((usuario) => {
        }),
        catchError(error => {
          console.error('Error al listar usuario:', error);
          return throwError(error);
        })
      );

  }



}
