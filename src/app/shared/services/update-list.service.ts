import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UpdateListService {

  private updateSubject = new Subject<void>();

  updateEvent$: Observable<void> = this.updateSubject.asObservable();

  emitUpdateEvent() {
    this.updateSubject.next();
  }
}
