import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { SharedRoutingModule } from './';
import { ReactiveFormsModule } from '@angular/forms';
import { VerificadorModule } from '../adminsitrador/maestras/verificador/verificador.module';
import { LayoutPageComponent } from './pages/layout-page/layout-page.component';
import { SharedRoutingModule } from './shared-routing.module';
import { MaterialModule } from '../material/material.module';
import { UsuarioModule } from '../usuarios/usuario.module';
import { ChartModule } from 'primeng/chart';


@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    SharedRoutingModule, //
    ReactiveFormsModule, // Por ejemplo, exportar ReactiveFormsModule
    UsuarioModule,
    MaterialModule,
    ChartModule
  ],
  exports: [
    CommonModule,
    //SharedRoutingModule,
    ReactiveFormsModule, // También exportar ReactiveFormsModule

  ],
})
export class SharedModule { }
