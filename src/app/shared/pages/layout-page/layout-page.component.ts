import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router, RouterOutlet } from '@angular/router';
import { ToolbarMenuComponent } from '../../components/toolbar-menu/toolbar-menu.component';
import { AuthService } from '../../../auth/services/auth.service';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../material/material.module';
import { SidebarMenuComponent } from '../../components/sidebar-menu/sidebar-menu.component';
import { MatSidenav } from '@angular/material/sidenav';
import { DataService } from '../../services/data.service';
import { Subscription } from 'rxjs';
import { LoadingComponent } from "../../components/loading/loading.component";
import { LoadingService } from '../../services/loading.service';

@Component({
    selector: 'app-layout-page',
    imports: [RouterOutlet, ToolbarMenuComponent, SidebarMenuComponent, CommonModule, MaterialModule, LoadingComponent],
    templateUrl: './layout-page.component.html',
    styleUrl: './layout-page.component.css'
})
export class LayoutPageComponent implements OnInit, OnDestroy {
  showFiller = false;
  mostrarMenu?:boolean=false;
  @ViewChild('drawer') sidenav?: MatSidenav;
  isSidenavOpen = false;
  show: boolean = false;
  tituloRecibido: string = '';
  private subscriptions: Subscription = new Subscription();

  loading = false;

  constructor(
    private authService: AuthService,
    private dataService: DataService,
    private router: Router,
    private loadingService: LoadingService,
    private cdr: ChangeDetectorRef
  ){
    this.mostrarMenu = this.authService.autenticado;
  }

  public get menu() : boolean|undefined {
    return this.mostrarMenu;
  }

  onToggle(toggled: boolean) {
    this.sidenav?.toggle();
  }

  ngOnInit() {
    this.subscriptions.add(
      this.router.events.subscribe(event => {
        if (event instanceof NavigationStart) {
          this.loadingService.setLoading(true);
        } else if (event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {
          this.loadingService.setLoading(false);
        }
      })
    );

    this.subscriptions.add(
      this.loadingService.loading$.subscribe(loading => {
        this.loading = loading;
      })
    );

    this.dataService.getPageTitle().subscribe(title => {
      this.recibirTitulo(title);
      this.cdr.detectChanges();
    });
  }

  isSidebarOpen = false;


  toggleSidebar(): void {
    this.sidenav?.toggle();
  }

  loadingEvent(evento:boolean): void {
    this.loading = evento;
  }

  recibirTitulo(titulo: string) {
    this.tituloRecibido = titulo;
  }

  recibirloading(loading: boolean) {
    this.loading = loading;
  }

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }

}
