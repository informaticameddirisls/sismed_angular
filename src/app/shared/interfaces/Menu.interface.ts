export interface Menu {
  supermodulo: Supermodulo[]; // Asegúrate de que 'supermodulo' esté definido en la interfaz 'Menu'
}

export interface Supermodulo {
  nombre: string;
  modulo: Modulo[];
}

export interface Modulo {
  nombre: string;
  submodulo: Submodulo[];
}

export interface Submodulo {
  nombre: string;
  ruta: string;
}
