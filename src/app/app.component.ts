import {} from '@angular/common/http';
import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { SharedModule } from './shared/shared.module';
import { VerificadorModule } from './adminsitrador/maestras/verificador/verificador.module';
import { environments } from '../environments/environments';


@Component({
    selector: 'app-root',
    standalone: true,
    imports: [RouterOutlet,
        // TODO: `HttpClientModule` should not be imported into a component directly.
        // Please refactor the code to add `provideHttpClient()` call to the provider list in the
        // application bootstrap logic and remove the `HttpClientModule` import from this component.
         SharedModule, VerificadorModule],
    templateUrl: './app.component.html',
    styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'Redisprod v2';
  isProduction = environments.production;
}
