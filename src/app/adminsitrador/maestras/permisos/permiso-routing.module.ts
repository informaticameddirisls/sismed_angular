import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';
import { ListarPermisoPageComponent } from './pages/listar-permiso-page/listar-permiso-page.component';

const routes: Routes = [
  {
    path: 'permiso',
    children:

      [{
        path: 'listar',
        component: ListarPermisoPageComponent,
        canActivate: [AuthGuard]
      }]
  }
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PermisoRoutingModule { }
