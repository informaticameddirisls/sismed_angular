import { Injectable } from '@angular/core';
import { environments } from '../../../../../environments/environments';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, tap, throwError } from 'rxjs';
import { Permiso } from '../interfaces/Permiso.interface';

@Injectable({
  providedIn: 'root'
})
export class PermisoService {
  private baseUrl = environments.baseUrl;
  private token = localStorage.getItem('token');
  private headers = new HttpHeaders;

  constructor(private http: HttpClient) {
    if (this.token) {
      // Encabezados de solicitud HTTP con el token
      this.headers = new HttpHeaders({
        'Authorization': `Bearer ${this.token}`
      });
    }
  }


  buscarporsubmodulo(id_submodulo: number): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      if (id_submodulo!=undefined) {
        return this.http.get<any>(`${this.baseUrl}/api/permiso/buscarporsubmodulo/${id_submodulo}`, { headers }).pipe(
          tap((supermodulo) => {
          }),
          catchError(error => {
            console.error('Error al listar modulos:', error);
            return throwError(error);
          })
        );
      } else{
        console.log('permiso no enviado.');
        return throwError('permiso no enviado');
      }
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  listar(parametros:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/permiso/listar`, parametros,{ headers }).pipe(
        tap((permiso) => {
        }),
        catchError(error => {
          console.error('Error al listar permiso:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }


  crear(permiso:Permiso): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/permiso/crear`,  permiso  , { headers }).pipe(
        tap((response) => {
        }),
        catchError(error => {
          console.error('Error al listar permiso:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  editar(permiso:Permiso): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.put<any>(`${this.baseUrl}/api/permiso/editar/${permiso.id_permiso}`,  permiso  , { headers }).pipe(
        tap((response) => {
          console.log(response,'editar');
        }),
        catchError(error => {
          console.error('Error al listar permiso:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  eliminar(permiso:Permiso): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/permiso/eliminar/${permiso.id_permiso}`, { headers }).pipe(
        tap((response) => {

        }),
        catchError(error => {
          console.error('Error al listar permiso:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  lista_permiso_combo(id_submodulo: number): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      if (id_submodulo!=undefined) {
        return this.http.get<any>(`${this.baseUrl}/api/administrador/maestra/permiso/lista_permiso_combo/${id_submodulo}`, { headers }).pipe(
          tap((supermodulo) => {
          }),
          catchError(error => {
            console.error('Error al listar modulos:', error);
            return throwError(error);
          })
        );
      } else{
        console.log('permiso no enviado.');
        return throwError('permiso no enviado');
      }
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

}
