import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';
import { PermisoRoutingModule } from './permiso-routing.module';



@NgModule({ declarations: [], imports: [CommonModule,
        MaterialModule,
        PermisoRoutingModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class PermisoModule { }
