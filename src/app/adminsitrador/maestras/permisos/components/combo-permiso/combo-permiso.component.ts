import { AsyncPipe, CommonModule } from '@angular/common';
import { AfterContentChecked, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Observable, map, startWith } from 'rxjs';
import { PermisoService } from '../../services/permiso.service';
import { Permiso } from '../../interfaces/Permiso.interface';
import { Submodulo } from '../../../submodulos/interfaces/Submodulo.interface';
import { MatSelectModule } from '@angular/material/select';

@Component({
    selector: 'app-combo-permiso',
    imports: [
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule,
        ReactiveFormsModule,
        CommonModule,
        AsyncPipe,
        MatFormFieldModule, MatSelectModule, FormsModule, ReactiveFormsModule
    ],
    templateUrl: './combo-permiso.component.html',
    styleUrl: './combo-permiso.component.css'
})
export class ComboPermisoComponent implements OnChanges, AfterContentChecked  {

  myControl = new FormControl();
  options: Permiso[] = [];
  filteredOptions!: Observable<Permiso[]>;
  selectedSubmodulo: Submodulo | undefined;

  @Input() submoduloSeleccionado!: Submodulo;
  @Input() permisosInput!: Permiso;

  @Output() permisoSeleccionado: EventEmitter<any> = new EventEmitter<any>();

  constructor(private permisoService: PermisoService,private cd: ChangeDetectorRef) {}

  ngAfterContentChecked(): void {
    this.cd.detectChanges();
  }

  someMethod(event: any): void {
    const selectedValues = event.value;
    const itemClickeado = event.source._keyManager._activeItem.value;

    if (selectedValues.includes(0) && itemClickeado==0) {
      // Si se selecciona "Todos", seleccionar todas las opciones
      const allAlmacenIds = this.options.map(option => option.id_permiso);
      const selectedOptions = [...allAlmacenIds, 0]; // Incluir también la opción "TODOS" (0)
      this.myControl.setValue(selectedOptions);
    }
    // Si se seleccionan otras opciones, emitir los valores seleccionados
    if (selectedValues.includes(0)==false && itemClickeado!=0) {
      this.myControl.value.filter((option:any) => option!== selectedValues.value)
    }
    if (selectedValues.includes(0)==false && itemClickeado==0) {
      this.myControl.setValue([]);
    }
    this.permisoSeleccionado.emit(this.myControl.value);
  }



  ngOnChanges(changes: SimpleChanges): void {

    if ('submoduloSeleccionado' in changes) {

      this.myControl.setValue('');
      const newSubmodulo = changes['submoduloSeleccionado'].currentValue;


      if (newSubmodulo) {
        this.loadModulosBySupermodulo(newSubmodulo);
      }
    }
  }

  private loadModulosBySupermodulo(submodulo: any): void {
    this.permisoService.lista_permiso_combo(submodulo).subscribe((response) => {
      this.options = response.data;

      this.selectPermisoByInput(this.permisosInput);
      this.updateFilteredOptions();
    });
  }

  private updateFilteredOptions(): void {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.nombre),
      map(nombre => this.options.slice())
    );
  }

  displayFn(option: Permiso): string {

    return option && option.nombre ? option.nombre : '';
  }


  onSelectionChange(event: MatAutocompleteSelectedEvent): void {

    const selectedPermiso = event.option.value;



    this.myControl.setValue(selectedPermiso.nombre);

    this.permisoSeleccionado.emit(selectedPermiso);

  }

  private selectPermisoByInput(permisosInput: Permiso): void {
    if (permisosInput!=undefined) {
      const selectedPermiso = this.options.find(option => option.id_permiso === permisosInput.id_permiso);
      console.log(this.options,permisosInput,selectedPermiso,"selectedPermiso");
      if (selectedPermiso) {
        this.myControl = new FormControl([selectedPermiso.id_permiso]);
        this.permisoSeleccionado.emit(this.myControl.value);
      }
    }
  }
}
