import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PermisoService } from '../../services/permiso.service';
import { UpdateListService } from '../../../../../shared/services/update-list.service';
import { FormValidationService } from '../../validators/form-validation.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';
import { ComboSupermoduloComponent } from '../../../supermodulos/components/combo-supermodulo/combo-supermodulo.component';
import { ComboModuloComponent } from '../../../modulos/components/combo-modulo/combo-modulo.component';
import { ComboSubmoduloComponent } from "../../../submodulos/components/combo-submodulo/combo-submodulo.component";

@Component({
    selector: 'app-modal-permiso-form',
    templateUrl: './modal-permiso-form.component.html',
    styleUrl: './modal-permiso-form.component.css',
    imports: [MatFormFieldModule,
        MatInputModule,
        MatIconModule, MatCardModule,
        MatButtonModule, ReactiveFormsModule,
        CommonModule, ComboSupermoduloComponent,
        ComboModuloComponent, ComboSubmoduloComponent]
})
export class ModalPermisoFormComponent implements OnInit, AfterViewInit {

  formularioPermiso!: FormGroup;
  selectedSupermodulo: any = null;
  selectedModulo: any = null;
  selectedSubmodulo: any = null;

  @Output() crearConfirmado: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    public dialogRef: MatDialogRef<ModalPermisoFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private permisoService: PermisoService,
    private updateListService: UpdateListService,
    private formValidationService: FormValidationService,
    private cdref: ChangeDetectorRef
  ){
  }
  ngAfterViewInit(): void {
  }

  ngAfterContentChecked() {
    //this.formularioSubmodulo.patchValue({id_supermodulo: this.selectedSubmodulo});
    //this.formularioSubmodulo.patchValue({id_modulo: this.selectedModulo})
    this.cdref.detectChanges();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.formularioPermiso = this.fb.group({
      id_permiso: [this.data.permiso?.id_permiso || 0],
      id_submodulo: [this.data.permiso?.id_submodulo || null, Validators.required],
      id_modulo: [this.data.permiso?.submodulo.id_modulo || null],
      id_supermodulo: [this.data.permiso?.submodulo.modulo.id_supermodulo || null],
      nombre: [this.data.permiso?.nombre || '', Validators.required],
      descripcion: [this.data.permiso?.descripcion || '', Validators.required],
      ruta: [this.data.permiso?.ruta || '', [Validators.required]]
    });
    if (!!this.data.permiso) {
      this.onSupermoduloSeleccionado(this.data.permiso.submodulo.modulo.supermodulo);
      this.onModuloSeleccionado(this.data.permiso.submodulo.modulo);
      this.onSubmoduloSeleccionado(this.data.permiso.submodulo);
    }
  }

  guardarPermiso(): void{
    if (this.formularioPermiso.valid) {
      const permiso = this.formularioPermiso.value;
      this.permisoService.crear(permiso).subscribe(
        (response)=>{
          if (response.success) {
            this.confirmarCreacion();
          }
        }
      )

      this.updateListService.emitUpdateEvent();
      this.dialogRef.close(true);
    } else {
      this.formularioPermiso.markAllAsTouched();
    }
  }

  editarPermiso(): void{
    console.log(this.formularioPermiso);
    return
    if (this.formularioPermiso.valid) {
      const permiso = this.formularioPermiso.value;
      this.permisoService.editar(permiso).subscribe(
        (response)=>{
          if (response.success) {
            this.confirmarCreacion();
          }
        }
      )
      this.updateListService.emitUpdateEvent();
      this.dialogRef.close(true);
    } else {
      this.formularioPermiso.markAllAsTouched();
    }
  }

  onSupermoduloSeleccionado(supermodulo: any): void {
    this.onModuloSeleccionado(null);
    this.selectedSupermodulo = supermodulo.id_supermodulo;
    //this.selectedModulo = null;
    this.formularioPermiso.patchValue({id_supermodulo: this.selectedSupermodulo})


  }

  onModuloSeleccionado(modulo: any): void {

    if (modulo == null) {
      this.onSubmoduloSeleccionado(null);
      this.selectedModulo=null
      this.formularioPermiso.patchValue({id_modulo: null})
      return
    }
    this.selectedModulo = modulo.id_modulo;
    this.formularioPermiso.patchValue({id_modulo: this.selectedModulo})

  }


  onSubmoduloSeleccionado(submodulo: any): void {
    if (submodulo == null) {
      this.selectedSubmodulo=null
      this.formularioPermiso.patchValue({id_submodulo: null})
      return
    }
    this.selectedSubmodulo = submodulo.id_submodulo;
    this.formularioPermiso.patchValue({id_submodulo: this.selectedSubmodulo})

  }







  esCampoValido( field: string ): boolean | null {

    return this.formValidationService.esCampoValido( this.formularioPermiso,field);
  }

  mensajeError( field: string ): string | null {
    return this.formValidationService.mensajeError(this.formularioPermiso,field);
  }


  confirmarCreacion(): void {
    this.crearConfirmado.emit(true); // Emitir true para confirmar eliminación
    this.dialogRef.close(); // Cerrar el diálogo
  }

  cancelarCreacion(): void {
    this.crearConfirmado.emit(false); // Emitir false para cancelar eliminación
    this.dialogRef.close(); // Cerrar el diálogo
  }


}
