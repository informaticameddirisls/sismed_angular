import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { DataService } from '../../../../../shared/services/data.service';
import { PermisoService } from '../../services/permiso.service';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { ComboModuloComponent } from '../../../modulos/components/combo-modulo/combo-modulo.component';
import { ComboSupermoduloComponent } from '../../../supermodulos/components/combo-supermodulo/combo-supermodulo.component';
import { ComboSubmoduloComponent } from '../../../submodulos/components/combo-submodulo/combo-submodulo.component';
import { ModalPermisoFormComponent } from '../../components/modal-permiso-form/modal-permiso-form.component';
import { Permiso } from '../../interfaces/Permiso.interface';
import { DialogEliminarComponent } from '../../../../../shared/components/dialog-eliminar/dialog-eliminar.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'app-listar-permiso-page',
    imports: [CommonModule, FormsModule, MatIconModule, MatPaginatorModule, MatTableModule, MatExpansionModule,
        MatFormFieldModule, MatSelectModule, ComboModuloComponent, ComboSupermoduloComponent,
        ComboSubmoduloComponent],
    templateUrl: './listar-permiso-page.component.html',
    styleUrl: './listar-permiso-page.component.css'
})
export class ListarPermisoPageComponent {
  titulo: string = 'PERMISO';


  registroAEliminar: Permiso | null = null;
  //creacion de la tabla
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  dataSource!: MatTableDataSource<any>;
  submoduloColumnas: string[] = ['id_permiso', 'nombre', 'ruta', 'descripcion', 'submodulo', 'modulo', 'supermodulo', 'estado', 'acciones'];

  selectedSupermodulo!: any;
  selectedModulo!: any;
  selectedSubmodulo!: any;
  selectedEstado!: any;


  @Output() tituloEnviado = new EventEmitter<string>();

  constructor(
    private permisoService: PermisoService,
    private dataService: DataService,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar
  ){}

  ngOnInit(): void {

    this.dataService.setPageTitle(this.titulo);
    this.buscarFiltrado();
  }

  abrirFormulario(permisoEditar?: any) {
    const dialogRef = this.dialog.open(ModalPermisoFormComponent, {

      data: {permiso: permisoEditar}
    });

     dialogRef.componentInstance.crearConfirmado.subscribe(confirmado => {
       if (confirmado) {
         this.buscarFiltrado();
       }
     });

  }

  editarPermiso(permisoEditar: Permiso): void {
    const dialogRef = this.dialog.open(ModalPermisoFormComponent, {
      data: {permiso: permisoEditar}
    });
    dialogRef.componentInstance.crearConfirmado.subscribe( confirmado => {
      if (confirmado) {
        this.buscarFiltrado();
      }
    })
  }

  abrirDialogoEliminar(permiso: any): void {
    this.registroAEliminar = permiso;
    const dialogRef = this.dialog.open(DialogEliminarComponent, {
      data: {
        registro: this.registroAEliminar,
        mensaje: "Desea eliminar el registro",
        nombre: "Permiso"
      }
    });
    dialogRef.componentInstance.eliminarConfirmado.subscribe(confirmado => {
      console.log(confirmado)
      if (confirmado) {
        this.eliminarPermiso(this.registroAEliminar);
      }
    });
  }

  eliminarPermiso(permiso: any): void {
    this.permisoService.eliminar(permiso).subscribe(
      (response) => {
        if (response.success) {
          this._snackBar.open(`REGISTRO ELIMINADO CORRECTAMENTE`, 'ok', {
            horizontalPosition: 'start',
            verticalPosition: 'bottom',
            duration: 10 * 1000,
          });
          this.buscarFiltrado();
        console.log(response,'permiso eliminado exitosamente.');
        }else{
          this._snackBar.open(`REGISTRO NO ELIMINADO`, 'ok', {
            horizontalPosition: 'start',
            verticalPosition: 'bottom',
            duration: 10 * 1000,
          });
        }

      },
      error => {
        console.error('Error al eliminar permiso:', error);
      }
    );
  }

  onSupermoduloSeleccionado(supermodulo: any): void {
    if (supermodulo == null) {
      this.selectedSupermodulo = null
      return
    }
    this.selectedSupermodulo = supermodulo.id_supermodulo;

  }
  onModuloSeleccionado(modulo: any): void {
    if (modulo == null) {
      this.selectedModulo = null
      return
    }
    this.selectedModulo = modulo.id_modulo;
  }

  onSubmoduloSeleccionado(submodulo: any): void {
    if (submodulo == null) {
      this.selectedSubmodulo = null
      return
    }
    this.selectedSubmodulo = submodulo.id_submodulo;


  }


  listarPermiso(parametros:any = {}): void {
    this.permisoService.listar(parametros).subscribe(
      (response) => {
        console.log(response)
        if (response.success) {

          this.dataSource = new MatTableDataSource(response.data);
          this.dataSource.paginator = this.paginator;
        } else {
          console.error('Error al listar supermódulos:', response);
        }
      },
      (error) => {
        console.error('Error al listar supermódulos:', error);
      }
    );
  }

  buscarFiltrado(){
    var selectedSupermodulo = this.selectedSupermodulo;
    var selectedModulo = this.selectedModulo;
    var selectedSubmodulo = this.selectedSubmodulo;
    var selectedEstado = this.selectedEstado;
    var parametros = {selectedSupermodulo,selectedModulo,selectedEstado,selectedSubmodulo}

    this.listarPermiso(parametros);

  }
}
