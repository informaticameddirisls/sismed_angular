import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-permiso-page',
    imports: [RouterOutlet],
    templateUrl: './layout-permiso-page.component.html',
    styleUrl: './layout-permiso-page.component.css'
})
export class LayoutPermisoPageComponent {

}
