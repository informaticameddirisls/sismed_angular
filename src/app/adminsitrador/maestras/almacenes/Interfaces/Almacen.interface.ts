export interface Almacen {
  id_almacen:            string;
  nombre:                string;
  fecha_venta_exp?:       Date;
  instalado?:             boolean;
  id_farmacia?:           string;
  fecha_ultima_conexion?: Date;
  tipo?:                  string;
  lindicador?:            boolean;
  estado?:                boolean;
  id_ipress?:             null;
  resp_farm?:             null;
  telf_resp?:             null;
  mail_resp?:             null;
  filter_cenares?:        null;
  id_ris?:                string;
  id_categoria?:          string;
  id_alterno?:            string;
  renaes?:                string;
}
