import { AsyncPipe, CommonModule } from '@angular/common';
import { AfterContentChecked, AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Observable, debounceTime, distinctUntilChanged, map, of, startWith, switchMap } from 'rxjs';
import { AlmacenService } from '../../services/almacen.service';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';

@Component({
    selector: 'app-combo-almacen-autocompletado',
    standalone: true,
    imports: [
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule,
        ReactiveFormsModule,
        CommonModule,
        AsyncPipe,
        MatSelectModule,
        MatOptionModule
    ],
    templateUrl: './combo-almacen-autocompletado.component.html',
    styleUrl: './combo-almacen-autocompletado.component.css'
})
export class ComboAlmacenAutocompletadoComponent implements OnChanges, OnInit, AfterViewInit, AfterContentChecked {
  myControl = new FormControl('');
  options: any[] = [];
  filteredOptions!: Observable<any[]>;
  @Input() risSeleccionado!: number | string;
  @Input() AlmacenTipo!: string;
  @Output() almacenSeleccionado: EventEmitter<any> = new EventEmitter<any>();
  @Input() almacenesInput!: any;

  ngAfterContentChecked(): void {
    this.cd.detectChanges();
  }
  constructor(
    private almacenService: AlmacenService,
    private cd: ChangeDetectorRef
  ) { }

  ngAfterViewInit(): void {
    console.log("2223")
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("changes", changes);
    if (changes && 'risSeleccionado' in changes) {
      var tipo = this.AlmacenTipo;
      var ris = changes['risSeleccionado'].currentValue?.id_tipo;

      var parametros = { ris, tipo };
      this.almacenService.listar(parametros).subscribe(response => {
        this.options = response.data;
        this.selectPermisoByInput(this.almacenesInput);
        this.updateFilteredOptions();
        this.myControl = new FormControl('');
        this.almacenSeleccionado.emit(this.myControl.value);
      });
    }
  }

  ngOnInit(): void {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''), // Emitir un valor inicial vacío
      debounceTime(500), // Esperar 500ms después de que el usuario deje de escribir
      distinctUntilChanged(), // Solo emitir si el valor cambió
      switchMap(value => this.search(value || '')) // Realizar la búsqueda en el servicio
    );
  }

  search(search: string): Observable<any[]> {
    var tipo = this.AlmacenTipo;
    var ris = this.risSeleccionado;
    console.log(search, "search1");

    // Verificar si el valor es texto
    if (typeof search !== 'string') {
      return of([]); // Devolver un observable vacío si no es texto
    }

    const parametros = { search, tipo, ris };
    return this.almacenService.listar(parametros).pipe(
      map((response) => {
        if (search === "") {
          this.almacenSeleccionado.emit(null);
        }
        return response.data;
      }) // Extraer los datos de la respuesta
    );
  }

  onOptionSelected(event: any): void {
    console.log("event", event);
    const selectedOption: any = event;
    const idAlmacen = selectedOption.id_almacen;
    this.myControl.setValue(`${idAlmacen} - ${selectedOption.nombre}`);
    this.almacenSeleccionado.emit([idAlmacen]);
  }

  onInput(event: Event): void {
    console.log("onInput");
    console.log("this.myControl",this.myControl.value);
    var search = this.myControl.value;
    var tipo = this.AlmacenTipo;
    var ris = this.risSeleccionado;
    var parametros = { ris, tipo,search };
    this.almacenService.listar(parametros).subscribe(response => {
      this.options = response.data;
      console.log(this.options, "this.options");
      this.selectPermisoByInput(this.almacenesInput);
      this.updateFilteredOptions();
      this.almacenSeleccionado.emit(this.myControl.value);

    });


    const input = event.target as HTMLInputElement;

    if (input.value === '') {
      this.myControl.setValue(null);
      this.almacenSeleccionado.emit(null);
    }
  }

  filterOptions(value: any): any[] {
    console.log("filterOptions", value);
    const filterValue = (value && typeof value === 'string') ? value.toLowerCase() : '';
    console.log(filterValue,"filterValue");
    let qqq = this.options.filter(option => option.nombre.toLowerCase().includes(filterValue));
    console.log(qqq,"qqq");
    return qqq;
  }

  private updateFilteredOptions(): void {
    console.log("updateFilteredOptions");
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this.filterOptions(value))
    );
  }

  private selectPermisoByInput(almacenesInput: any): void {
    console.log("selectPermisoByInput");
    if (almacenesInput != undefined) {
      const selectedPermiso = this.options.find(option => option.id_almacen === almacenesInput.id_almacen);
      if (selectedPermiso) {
        console.log("selectedPermiso", selectedPermiso);
        this.myControl.setValue(`${selectedPermiso.id_almacen} - ${selectedPermiso.nombre}`); // Asignar un string
        this.almacenSeleccionado.emit(this.myControl.value);
      }
    }
  }

  displayFn(almacen?: any): string {
    console.log("displayFn");
    return almacen ? `${almacen.id_almacen} - ${almacen.nombre}` : '';
  }
}
