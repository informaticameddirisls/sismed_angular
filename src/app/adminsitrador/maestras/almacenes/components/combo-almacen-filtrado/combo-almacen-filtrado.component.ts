import { AsyncPipe, CommonModule } from '@angular/common';
import { AfterContentChecked, AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ControlValueAccessor, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { Almacen } from '../../Interfaces/Almacen.interface';
import { Observable, map, startWith } from 'rxjs';
import { AlmacenService } from '../../services/almacen.service';


@Component({
    selector: 'app-combo-almacen-filtrado',
    imports: [FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule,
        ReactiveFormsModule,
        CommonModule,
        AsyncPipe,
        MatSelectModule],
    templateUrl: './combo-almacen-filtrado.component.html',
    styleUrl: './combo-almacen-filtrado.component.css'
})
export class ComboAlmacenFiltradoComponent implements OnInit {

  myControl = new FormControl();
  options: Almacen[] = [];
  filteredOptions!: Observable<any[]>;



  @Input() almacenesInput!: Almacen;
  @Input() AlmacenTipo!: string;
  @Input() filtraUsuario!: boolean;
  @Input() nombreSubmodulo!: string;
  @Input() risSeleccionado!: number | string;
  @Output() almacenSeleccionado: EventEmitter<any> = new EventEmitter<any>();

  someMethod(event: any): void {
    const selectedValue = event.value;
    this.myControl.setValue(selectedValue);
    this.almacenSeleccionado.emit(selectedValue);
  }

  ngOnInit(): void {
    this.fetchData();
  }


  private fetchData(): void {
    const parametros = { filtraUsuario: this.filtraUsuario,nombreSubmodulo: this.nombreSubmodulo };
    console.log(parametros);
    this.almacenService.listar_almacen_combo(parametros).subscribe(response => {
      this.options = response.data;
      this.selectPermisoByInput(this.almacenesInput);
      this.updateFilteredOptions();
    });
  }


  private updateFilteredOptions(): void {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(null),
      map(value => this.filterOptions(value))
    );
  }


  filterOptions(value: any): Almacen[] {
    const filterValue = (value && typeof value === 'string') ? value.toLowerCase() : '';
    return this.options.filter(option => option.nombre.toLowerCase().includes(filterValue));
  }


  private selectPermisoByInput(almacenesInput: Almacen): void {
    if (almacenesInput !== undefined) {
      const selectedPermiso = this.options.find(option => option.id_almacen === almacenesInput.id_almacen);

      if (selectedPermiso) {
        this.myControl.setValue(selectedPermiso); // No es necesario el array
        this.almacenSeleccionado.emit(selectedPermiso.id_almacen); // Emitir solo el ID si es necesario
      }
    }
  }

  constructor(private almacenService: AlmacenService,private cd: ChangeDetectorRef) {}

  ngOnChanges(changes: SimpleChanges): void {

    if (changes && 'risSeleccionado' in changes) {

      var tipo = this.AlmacenTipo;
      var ris = changes['risSeleccionado'].currentValue?.id_tipo;
      var parametros = {ris,tipo}
      //console.log(changes,"changeschangeschanges")
      this.almacenService.listar(parametros).subscribe(response => {
        ///console.log(response)
        this.options = response.data;
        this.selectPermisoByInput(this.almacenesInput);

        this.updateFilteredOptions();

        this.myControl = new FormControl([]);
        this.almacenSeleccionado.emit(this.myControl.value);
      });
    }
  }

}
