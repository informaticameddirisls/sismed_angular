import { AfterContentChecked, AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { AlmacenService } from '../../services/almacen.service';
import { ControlValueAccessor, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, map, startWith } from 'rxjs';
import { Almacen } from '../../Interfaces/Almacen.interface';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { AsyncPipe, CommonModule } from '@angular/common';
import { MatSelectModule } from '@angular/material/select';

@Component({
    selector: 'app-combo-almacen',
    imports: [
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule,
        ReactiveFormsModule,
        CommonModule,
        AsyncPipe,
        MatFormFieldModule, MatSelectModule, FormsModule, ReactiveFormsModule,
    ],
    templateUrl: './combo-almacen.component.html',
    styleUrl: './combo-almacen.component.css'
})
export class ComboAlmacenComponent implements ControlValueAccessor, OnChanges, OnInit, AfterViewInit, AfterContentChecked {
  myControl = new FormControl();
  options: Almacen[] = [];
  filteredOptions!: Observable<any[]>;

  @Input() almacenesInput!: Almacen;
  @Input() AlmacenTipo!: string;
  @Input() risSeleccionado!: number | string;
  @Output() almacenSeleccionado: EventEmitter<any> = new EventEmitter<any>();

  someMethod(event: any): void {
    //console.log(1);
    const selectedValues = event.value;
    const itemClickeado = event.source.selected ? event.source.selected[0] : event.source.deselected[0];
    //console.log('Item clickeado:', itemClickeado);
    //console.log(event);
    //debugger
    if (!selectedValues.includes(0) && itemClickeado != 0) {
      //this.myControl.setValue(selectedValues.filter((value: any) => value !== 0));
      //this.myControl.setValue(selectedValues.filter((value: any) => value !== itemClickeado));
    }
    if (selectedValues.includes(0) && itemClickeado != 0) {
      //this.myControl.setValue(selectedValues.filter((value: any) => value !== 0));
      //this.myControl.setValue(selectedValues.filter((value: any) => value !== itemClickeado));
    }
    // if (!selectedValues.includes(0) && itemClickeado == 0) {
    //   this.myControl.setValue(selectedValues.filter((value: any) => value !== 0));
    // }
    //console.log(1.1);
    //console.log(this.myControl.value,"this.myControl.value");
    this.almacenSeleccionado.emit(this.myControl.value);
    return

  }

  onOptionClick(value: string): void {
    //console.log(2);
    //console.log('Option clicked:', value);
    //console.log(this.myControl.value.includes(0),"this.myControl.value");


    if (value == '0' && this.myControl.value.includes('0') == true) {

      const allAlmacenIds = this.options.map(option => option.id_almacen);
      this.myControl.setValue(['0', ...allAlmacenIds]);
    }

    if (value == '0' && this.myControl.value.includes('0') == false) {
      this.myControl.setValue([]);
    }

    if (value != '0' && this.myControl.value.includes('0') == true) {
      this.myControl.setValue(this.myControl.value.filter((value: any) => value !== '0'));
    }

    this.almacenSeleccionado.emit(this.myControl.value);
    return



    if (value != '0' && this.myControl.value.includes('0') == false) {
      this.myControl.setValue(this.myControl.value.filter((value: any) => value !== '0'));
    }

  }

  constructor(private almacenService: AlmacenService, private cd: ChangeDetectorRef) {}

  writeValue(value: any): void {
    if (value) {
      this.myControl.setValue(value);
    } else {
      this.myControl.setValue(null);
    }
  }

  registerOnChange(fn: any): void {
    this.myControl.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.myControl.valueChanges.subscribe(fn);
  }

  ngAfterViewInit(): void {}

  ngAfterContentChecked(): void {
    this.cd.detectChanges();
  }

  ngOnInit(): void {
    var tipo = this.AlmacenTipo;
    var parametros = { tipo };
    this.almacenService.listar(parametros).subscribe(response => {
      this.options = response.data;
      this.selectPermisoByInput(this.almacenesInput);
      this.updateFilteredOptions();
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && 'risSeleccionado' in changes) {
      var tipo = this.AlmacenTipo;
      var ris = changes['risSeleccionado'].currentValue?.id_tipo;
      var parametros = { ris, tipo };
      this.almacenService.listar(parametros).subscribe(response => {
        this.options = response.data;
        this.selectPermisoByInput(this.almacenesInput);
        this.updateFilteredOptions();
        this.myControl = new FormControl([]);
        this.almacenSeleccionado.emit(this.myControl.value);
      });
    }
  }

  onSelectionChange(event: MatAutocompleteSelectedEvent): void {}

  filterOptions(value: any): Almacen[] {
    const filterValue = (value && typeof value === 'string') ? value.toLowerCase() : '';
    return this.options.filter(option => option.nombre.toLowerCase().includes(filterValue));
  }

  private updateFilteredOptions(): void {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(null),
      map(value => this.filterOptions(value))
    );
  }

  private selectPermisoByInput(almacenesInput: Almacen): void {
    if (almacenesInput != undefined) {
      const selectedPermiso = this.options.find(option => option.id_almacen === almacenesInput.id_almacen);
      if (selectedPermiso) {
        this.myControl = new FormControl([selectedPermiso.id_almacen]);
        this.almacenSeleccionado.emit(this.myControl.value);
      }
    }
  }
}
