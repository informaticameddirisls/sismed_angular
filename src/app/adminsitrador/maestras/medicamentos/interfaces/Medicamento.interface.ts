export interface Medicamento {
  id_medicamento:          string;
  nombre:                  string;
  presentacion:            string;
  concentracion:           string;
  forma_farmaceutica:      string;
  medtip:                  null;
  medpet:                  null;
  medest:                  null;
  medsm:                   boolean;
  medsituacion:            boolean;
  cod_siga:                null;
  id_programa_estrategico: null;
  evalua_digemid:          string;
  estado:                  null;
}
