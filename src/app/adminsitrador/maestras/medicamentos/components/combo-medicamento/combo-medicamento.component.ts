import { AsyncPipe, CommonModule } from '@angular/common';
import { AfterContentChecked, AfterViewInit, ChangeDetectorRef, Component, EventEmitter, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { Observable, debounceTime, distinctUntilChanged, map, startWith, switchMap } from 'rxjs';
import { MedicamentoService } from '../../services/medicamento.service';
import { Medicamento } from '../../interfaces/Medicamento.interface';


@Component({
    selector: 'app-combo-medicamento',
    imports: [
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule,
        ReactiveFormsModule,
        CommonModule,
        AsyncPipe,
        MatSelectModule
    ],
    templateUrl: './combo-medicamento.component.html',
    styleUrl: './combo-medicamento.component.css',

})
export class ComboMedicamentoComponent  implements OnChanges, OnInit, AfterViewInit, AfterContentChecked {
  myControl = new FormControl();
  options: Medicamento[] = [];
  filteredOptions!: Observable<Medicamento[]>;


  @Output() medicamentoSeleccionado: EventEmitter<any> = new EventEmitter<any>();

  ngAfterContentChecked(): void {
    this.cd.detectChanges();
  }
  constructor(private medicamentoService: MedicamentoService,private cd: ChangeDetectorRef) { }


  ngAfterViewInit(): void {
    console.log("2223")
  }
  ngOnChanges(changes: SimpleChanges): void {

  }

  ngOnInit(): void {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''), // Emitir un valor inicial vacío
      debounceTime(500), // Esperar 300ms después de que el usuario deje de escribir
      distinctUntilChanged(), // Solo emitir si el valor cambió
      map(search => search.trim()), // Eliminar espacios en blanco al inicio y al final
      switchMap(search => this.search(search)) // Realizar la búsqueda en el servicio
    );
  }

  search(search: string): Observable<Medicamento[]> {
    return this.medicamentoService.listar(search).pipe(
      map((response) => {
        if (search=="") {
          this.medicamentoSeleccionado.emit(null);
        }
        //
        return response.data
      }) // Extraer los datos de la respuesta
    );
  }

  onOptionSelected(event: any): void {
    const selectedOption: Medicamento = event;
    const idMedicamento = selectedOption.id_medicamento;
    this.myControl.setValue(idMedicamento+" - "+selectedOption.nombre+" "+selectedOption.presentacion+" "+selectedOption.concentracion);
    this.medicamentoSeleccionado.emit(selectedOption);
  }


  filterOptions(value: any): Medicamento[] {

    const filterValue = (value && typeof value === 'string') ? value.toLowerCase() : '';
    return this.options.filter(option => option.nombre.toLowerCase().includes(filterValue));
  }

  private _filter(value: string): Medicamento[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.nombre.toLowerCase().includes(filterValue));
  }

  someMethod(event: any): void {

  }


}
