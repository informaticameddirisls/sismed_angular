import { Injectable } from '@angular/core';
import { environments } from '../../../../../environments/environments';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, tap, throwError } from 'rxjs';
import { Modulo } from '../interfaces/Modulo.interface';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class ModuloService {
  private baseUrl = environments.baseUrl;
  private token = localStorage.getItem('token');
  private headers = new HttpHeaders;

  constructor(private http: HttpClient,
    private _snackBar: MatSnackBar
  ) {
    if (this.token) {
      // Encabezados de solicitud HTTP con el token
      this.headers = new HttpHeaders({
        'Authorization': `Bearer ${this.token}`
      });
    }
  }

  crear(modulo:Modulo): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/modulo/crear`,  modulo  , { headers }).pipe(
        tap((response) => {
          console.log(response,'crear');
        }),
        catchError(error => {
          console.error('Error al listar supermódulos:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }


  editar(modulo:Modulo): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.put<any>(`${this.baseUrl}/api/modulo/editar/${modulo.id_modulo}`,  modulo  , { headers }).pipe(
        tap((response) => {
          console.log(response,'editar');
        }),
        catchError(error => {
          console.error('Error al listar modulo:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }


  listar(parametros:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/modulo/listar`, parametros,{ headers }).pipe(
        tap((supermodulo) => {
        }),
        catchError(error => {
          console.error('Error al listar modulo:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  eliminar(modulo:Modulo): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/modulo/eliminar/${modulo.id_modulo}`, { headers }).pipe(
        tap((response) => {
          this._snackBar.open(`REGISTRO ELIMINADO CORRECTAMENTE`, 'ok', {
            horizontalPosition: 'start',
            verticalPosition: 'bottom',
            duration: 10 * 1000,
          });
        }),
        catchError(error => {
          console.error('Error al listar supermódulos:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  buscarporsprmodulo(id_supermodulo: number): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      if (id_supermodulo!=undefined) {
        return this.http.get<any>(`${this.baseUrl}/api/modulo/buscarporsprmodulo/${id_supermodulo}`, { headers }).pipe(
          tap((supermodulo) => {
          }),
          catchError(error => {
            console.error('Error al listar modulos:', error);
            return throwError(error);
          })
        );
      } else{
        console.log('supermodulo no enviado.');
        return throwError('supermodulo no enviado');
      }
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }


  lista_modulo_combo(id_supermodulo: number): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      if (id_supermodulo!=undefined) {
        return this.http.get<any>(`${this.baseUrl}/api/administrador/maestra/modulo/lista_modulo_combo/${id_supermodulo}`, { headers }).pipe(
          tap((supermodulo) => {
          }),
          catchError(error => {
            console.error('Error al listar modulos:', error);
            return throwError(error);
          })
        );
      } else{
        console.log('supermodulo no enviado.');
        return throwError('supermodulo no enviado');
      }
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }




}
