export interface Modulo {
  id_modulo: number;
  nombre: string;
  ruta: string;
  descripcion: string;
  orden: number;
  id_supermodulo: number,
  estado: boolean;
  eliminado: boolean;
  fecha_creacion: string; // Cambiado a tipo string para mantener el formato original
  usuario_creacion: number | null; // Puede ser una cadena o nulo
  fecha_modificacion: string | null; // Puede ser una cadena o nulo
  usuario_modificacion: number | null; // Puede ser una cadena o nulo
  fecha_elimino: string; // Cambiado a tipo string para mantener el formato original
  usuario_elimino: number; // Cambiado a tipo string para mantener el formato original
}

