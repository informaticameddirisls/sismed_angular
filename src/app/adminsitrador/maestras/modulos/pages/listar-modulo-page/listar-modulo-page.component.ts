import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { DataService } from '../../../../../shared/services/data.service';
import { MatDialog } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { ModuloService } from '../../services/modulo.service';
import { ComboSupermoduloComponent } from '../../../supermodulos/components/combo-supermodulo/combo-supermodulo.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { ModalModuloFormComponent } from '../../components/modal-modulo-form/modal-modulo-form.component';
import { Modulo } from '../../interfaces/Modulo.interface';
import { DialogEliminarComponent } from '../../../../../shared/components/dialog-eliminar/dialog-eliminar.component';


@Component({
    selector: 'app-listar-modulo-page',
    templateUrl: './listar-modulo-page.component.html',
    styleUrl: './listar-modulo-page.component.css',
    imports: [CommonModule, ComboSupermoduloComponent, FormsModule, MatIconModule, MatPaginatorModule, MatTableModule, MatExpansionModule,
        MatFormFieldModule, MatSelectModule
    ]
})
export class ListarModuloPageComponent {
  titulo: string = 'MODULO';
  @Output() tituloEnviado = new EventEmitter<string>();

  registroAEliminar: Modulo | null = null;
  //creacion de la tabla
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  dataSource!: MatTableDataSource<any>;
  moduloColumnas: string[] = ['id_modulo', 'nombre', 'ruta', 'descripcion', 'supermodulo','estado','acciones'];

  selectedSupermodulo!: any;
  selectedEstado!: any;
  constructor(
    private dataService: DataService,
    private dialog: MatDialog,
    private moduloService:ModuloService
  ){}

  ngOnInit(): void {
    this.buscarFiltrado();
    this.dataService.setPageTitle(this.titulo);
  }

  listarModulo(parametros: any = {}): void {
    this.moduloService.listar(parametros).subscribe(
      (response) => {
        console.log(response)
        if (response.success) {
          this.dataSource = new MatTableDataSource(response.data);
          this.dataSource.paginator = this.paginator;
        } else {
          console.error('Error al listar usuario:', response);
        }
      },
      (error) => {
        console.error('Error al listar usuario:', error);
      }
    );
  }


  editarSupermodulo(modulo: Modulo): void {
    const dialogRef = this.dialog.open(ModalModuloFormComponent, {

      data: {modulo: modulo}
    });

    dialogRef.componentInstance.crearConfirmado.subscribe( confirmado => {
      if (confirmado) {
        this.buscarFiltrado();
      }
    })
  }


  abrirDialogoEliminar(supermodulo: any): void {
    this.registroAEliminar = supermodulo;
    const dialogRef = this.dialog.open(DialogEliminarComponent, {
      data: {
        registro: this.registroAEliminar,
        mensaje: "Desea eliminar el registro",
        nombre: "Supermodulo"
      }
    });
    dialogRef.componentInstance.eliminarConfirmado.subscribe(confirmado => {
      console.log(confirmado)
      if (confirmado) {
        this.eliminarSupermodulo(this.registroAEliminar);
      }
    });
  }

  eliminarSupermodulo(supermodulo: any): void {
    this.moduloService.eliminar(supermodulo).subscribe(
      (response) => {
        this.buscarFiltrado();
        console.log(response,'Supermódulo eliminado exitosamente.');
      },
      error => {
        console.error('Error al eliminar supermódulo:', error);
      }
    );
  }


  abrirFormulario(moduloEditar: any = {}) {
    const dialogRef = this.dialog.open(ModalModuloFormComponent, {

      data: {modulo: moduloEditar}
    });

    dialogRef.componentInstance.crearConfirmado.subscribe(confirmado => {
      console.log(confirmado,"actualizado")
      if (confirmado) {
        this.buscarFiltrado();
      }
    });

  }

  onSupermoduloSeleccionado(supermodulo: any): void {
    this.selectedSupermodulo = supermodulo.id_supermodulo;

  }

  buscarFiltrado(){
    var selectedSupermodulo = this.selectedSupermodulo;
    var selectedEstado = this.selectedEstado;
    var parametros = {selectedSupermodulo,selectedEstado}

    this.listarModulo(parametros);

  }


}
