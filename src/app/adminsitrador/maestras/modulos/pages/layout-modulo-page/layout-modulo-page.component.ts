import { Component } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-modulo-page',
    imports: [RouterOutlet],
    templateUrl: './layout-modulo-page.component.html',
    styleUrl: './layout-modulo-page.component.css'
})
export class LayoutModuloPageComponent {

}
