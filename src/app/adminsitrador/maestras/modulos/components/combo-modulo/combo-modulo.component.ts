import { AsyncPipe, CommonModule } from '@angular/common';
import { AfterContentChecked, AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, forwardRef } from '@angular/core';
import { ControlValueAccessor, FormControl, FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Modulo } from '../../interfaces/Modulo.interface';
import { Observable, map, startWith, tap } from 'rxjs';
import { ModuloService } from '../../services/modulo.service';
import { Supermodulo } from '../../../supermodulos/interfaces/Supermodulo.interface';
import { MatAutocompleteModule, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
@Component({
    selector: 'app-combo-modulo',
    imports: [
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule,
        ReactiveFormsModule,
        CommonModule,
        AsyncPipe
    ],
    templateUrl: './combo-modulo.component.html',
    styleUrl: './combo-modulo.component.css'
})
export class ComboModuloComponent implements ControlValueAccessor,OnChanges,OnInit,AfterViewInit {
  myControl = new FormControl();
  options: Modulo[] = [];
  filteredOptions!: Observable<Modulo[]>;
  selectedSupermodulo: Supermodulo | null = null;

  @Input() supermoduloSeleccionado!: Supermodulo | null;
  @Input() modulosInput!: number;

  @Output() moduloSeleccionado: EventEmitter<any> = new EventEmitter<any>();

  constructor(private moduloService: ModuloService,private cdref: ChangeDetectorRef) {
  }

  writeValue(value: any): void {

    if (value) {
      this.myControl.setValue(value);
    } else {
      this.myControl.setValue(null);
    }
  }
  registerOnChange(fn: any): void {
    this.myControl.valueChanges.subscribe(fn);
  }
  registerOnTouched(fn: any): void {
    this.myControl.valueChanges.subscribe(fn);
  }
  ngAfterViewInit(): void {

  }
  ngAfterContentChecked(): void {

        this.cdref.detectChanges();
  }

  ngOnInit(): void {
    if (!this.supermoduloSeleccionado) {
      return
    }
    this.loadModulosBySupermodulo(this.supermoduloSeleccionado);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && 'supermoduloSeleccionado' in changes) {

      this.myControl.setValue('');
      const newSupermodulo = changes['supermoduloSeleccionado'].currentValue;
      if (newSupermodulo) {
        this.loadModulosBySupermodulo(newSupermodulo,changes);
      }
    }
  }

  private loadModulosBySupermodulo(supermodulo: any,changes?:any): void {
    this.moduloService.lista_modulo_combo(supermodulo).subscribe((response) => {

      this.options = response.data;

      const selectedModulo = this.options.find(option => option.id_modulo === this.modulosInput);

      if (selectedModulo) {


        if (!!changes && 'modulosInput' in changes && changes['modulosInput']['firstChange']) {

          const newModulo = changes['modulosInput'].currentValue;

          const selectedModulo = this.options.find(option => option.id_modulo === newModulo);

          this.myControl.setValue(selectedModulo!.nombre);
          this.moduloSeleccionado.emit(selectedModulo);
        }else{
          this.myControl.setValue(selectedModulo!.nombre);
          this.moduloSeleccionado.emit(selectedModulo);
        }
      }else{
        this.myControl.setValue("");
        this.moduloSeleccionado.emit(null);
      }

      this.updateFilteredOptions();
    });
  }

  private updateFilteredOptions(): void {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.nombre),
      map(nombre => this._filter(nombre))
    );
  }

  private _filter(value: string): Modulo[] {

    if (value=="") {
      //this.moduloSeleccionado.emit("");
    }
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.nombre.toLowerCase().includes(filterValue));
  }

  displayFn(option: Modulo): string {
    return option && option.nombre ? option.nombre : '';
  }


  onSelectionChange(event: MatAutocompleteSelectedEvent): void {
    const selectedModulo = event.option.value as Modulo;

    console.log(selectedModulo,"selectedModulo");

    this.myControl.setValue(selectedModulo.nombre);
    this.moduloSeleccionado.emit(selectedModulo);

  }
}
