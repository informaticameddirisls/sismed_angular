import { ChangeDetectorRef, Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ModuloService } from '../../services/modulo.service';
import { UpdateListService } from '../../../../../shared/services/update-list.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';
import { ComboSupermoduloComponent } from "../../../supermodulos/components/combo-supermodulo/combo-supermodulo.component";
import { FormValidationService } from '../../validators/form-validation.service';

@Component({
    selector: 'app-modal-modulo-form',
    templateUrl: './modal-modulo-form.component.html',
    styleUrls: ['./modal-modulo-form.component.css'],
    imports: [MatFormFieldModule, MatInputModule, MatIconModule, MatCardModule, MatButtonModule, ReactiveFormsModule, CommonModule, ComboSupermoduloComponent]
})
export class ModalModuloFormComponent implements OnInit {
  formularioModulo!: FormGroup;
  selectedSupermodulo: any;

  @Output() crearConfirmado: EventEmitter<boolean> = new EventEmitter<boolean>();



  constructor(
    public dialogRef: MatDialogRef<ModalModuloFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private moduloService: ModuloService,
    private updateListService: UpdateListService,
    private formValidationService: FormValidationService,
    private cdref: ChangeDetectorRef
  ){
    console.log(this.data,"this.data")
    this.formularioModulo = this.fb.group({
      id_modulo: [this.data?.modulo.id_modulo || 0],
      id_supermodulo: [this.data?.modulo.id_supermodulo || null,Validators.required],
      nombre: [this.data?.modulo.nombre || '', Validators.required],
      descripcion: [this.data?.modulo.descripcion || '', Validators.required],
      ruta: [this.data?.modulo.ruta || '', [Validators.required]]
    });
    this.selectedSupermodulo = this.data?.modulo.id_supermodulo;
  }

  ngAfterContentChecked() {
    console.log(this.selectedSupermodulo)
    this.formularioModulo.patchValue({id_supermodulo: this.selectedSupermodulo})
    this.cdref.detectChanges();
 }

  ngOnInit(): void {


  }

  onSupermoduloSeleccionado(supermodulo: any): void {
    this.selectedSupermodulo = supermodulo.id_supermodulo;
    this.formularioModulo.patchValue({id_supermodulo:this.selectedSupermodulo})
    console.log(this.formularioModulo.value)
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  initializeForm(): void {
    this.formularioModulo = this.fb.group({
      id_modulo: [this.data?.id_modulo || 0],
      id_supermodulo: [this.data?.id_supermodulo || null,Validators.required],
      nombre: [this.data?.nombre || '', Validators.required],
      descripcion: [this.data?.descripcion || '', Validators.required],
      ruta: [this.data?.ruta || '', Validators.required]
    });
  }

  guardarSupermodulo(): void{
    console.log(this.formularioModulo);
    if (this.formularioModulo.valid) {
      const modulo = this.formularioModulo.value;
      this.moduloService.crear(modulo).subscribe(
        (response)=>{
          if (response.success) {
            this.confirmarCreacion();
          }
        }
      )
      console.log('Datos a guardar:', modulo);

      this.updateListService.emitUpdateEvent();
      this.dialogRef.close(true);
    } else {
      this.formularioModulo.markAllAsTouched();
    }
  }

  editarSupermodulo(): void{

    if (this.formularioModulo.valid) {
      const modulo = this.formularioModulo.value;
      this.moduloService.editar(modulo).subscribe(
        (response)=>{
          if (response.success) {
            this.confirmarCreacion();
          }
        }
      )
      console.log('Datos a guardar:', modulo);

      this.updateListService.emitUpdateEvent();
      this.dialogRef.close(true);
    } else {
      this.formularioModulo.markAllAsTouched();
    }
  }

  esCampoValido( field: string ): boolean | null {

    return this.formValidationService.esCampoValido( this.formularioModulo,field);
  }

  mensajeError( field: string ): string | null {
    return this.formValidationService.mensajeError(this.formularioModulo,field);
  }


  confirmarCreacion(): void {
    this.crearConfirmado.emit(true); // Emitir true para confirmar eliminación
    this.dialogRef.close(); // Cerrar el diálogo
  }

  cancelarCreacion(): void {
    this.crearConfirmado.emit(false); // Emitir false para cancelar eliminación
    this.dialogRef.close(); // Cerrar el diálogo
  }
}
