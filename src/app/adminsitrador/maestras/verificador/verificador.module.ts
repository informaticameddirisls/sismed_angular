import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';
import { VerificadorRoutingModule } from './verificador-routing.module';



@NgModule({ declarations: [], imports: [CommonModule,
        CommonModule,
        MaterialModule,
        VerificadorRoutingModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class VerificadorModule { }
