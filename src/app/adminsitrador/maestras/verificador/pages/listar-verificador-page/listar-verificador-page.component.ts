import { Component } from '@angular/core';
import { DataService } from '../../../../../shared/services/data.service';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
    selector: 'verificador-listar-verificador-page',
    imports: [],
    templateUrl: './listar-verificador-page.component.html',
    styleUrl: './listar-verificador-page.component.css'
})
export class ListarVerificadorPageComponent {


  titulo: string = 'VERIFICADOR';

  constructor(

    private dataService: DataService,
    private dialog: MatDialog,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);
  }


  verStockVerificar():void {
    this.router.navigate(['sismed', 'administrador', 'maestra', 'verificador', 'stock-verificar']);
  }

}
