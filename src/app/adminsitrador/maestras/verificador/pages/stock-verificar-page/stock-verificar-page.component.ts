import { Component } from '@angular/core';
import { DataService } from '../../../../../shared/services/data.service';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
    selector: 'verificador-stock-verificar-page',
    imports: [],
    templateUrl: './stock-verificar-page.component.html',
    styleUrl: './stock-verificar-page.component.css'
})
export class StockVerificarPageComponent {


  titulo: string = 'VERIFICAR STOCK';

  constructor(

    private dataService: DataService,
    private dialog: MatDialog,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);
  }


}
