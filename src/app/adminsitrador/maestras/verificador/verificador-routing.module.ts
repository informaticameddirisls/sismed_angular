import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarVerificadorPageComponent } from './pages/listar-verificador-page/listar-verificador-page.component';
import { AuthGuard } from '../../../auth/guards/auth.guard';
import { StockVerificarPageComponent } from './pages/stock-verificar-page/stock-verificar-page.component';

const routes: Routes = [
  {
    path: 'verificador',
    children:

      [
        {
          path: 'listar',
          component: ListarVerificadorPageComponent,
          //canActivate: [AuthGuard]
        },{
          path: 'stock-verificar',
          component: StockVerificarPageComponent,
          //canActivate: [AuthGuard]
      }
      ]
  }
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerificadorRoutingModule { }
