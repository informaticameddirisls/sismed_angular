import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormsModule,ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatOption, MatSelectModule } from '@angular/material/select';
import { MaestraCategoriaService } from '../../services/maestraCategoria.service';
import { CommonModule } from '@angular/common';
import { AsyncPipe } from '@angular/common';

@Component({
    selector: 'maestra-combo-maestra-categoria',
    imports: [FormsModule, MatOption, MatFormFieldModule, MatSelectModule, MatInputModule, ReactiveFormsModule, CommonModule],
    templateUrl: './combo-maestra-categoria.component.html',
    styleUrl: './combo-maestra-categoria.component.css'
})
export class ComboMaestraCategoriaComponent implements OnInit {
  selectedValue!: string;
  selectedCar!: string;
  @Input() tipoMaestraCategoria!: string;
  @Output() maestraCategoriaSeleccionado: EventEmitter<any> = new EventEmitter<any>();

  cargarCategorias!: any[];
  MaestraCategoriaControl = new FormControl('');
  constructor(private maestraCategoriaService: MaestraCategoriaService) {}

  ngOnInit(): void {
    this.cargarMaestraCategoria(this.tipoMaestraCategoria)
  }




  cargarMaestraCategoria(mae_Cat:string) {

    var parametros = {"tipo":mae_Cat}
    this.maestraCategoriaService.listar(parametros).subscribe((response)=>{

      this.cargarCategorias = response.resultado;

    })
  }

  onSelectionChange(event: any) {
    this.maestraCategoriaSeleccionado.emit(event.value);
  }

}
