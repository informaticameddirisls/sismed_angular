import { MaestraCategoriaMedicamento } from "../../maestra_categoria_medicamento/interfaces/MaestraCategoriaMedicamento.interface";

export interface MaestraCategoria {
  id_maestra_categoria:          number;
  nombre:                        string;
  tipo:                          string;
  almacen_categoria:             string;
  grupo:                         string;
  abreviado:                     null;
  rango:                         null;
  maestra_categoria_medicamento: MaestraCategoriaMedicamento[];
}

