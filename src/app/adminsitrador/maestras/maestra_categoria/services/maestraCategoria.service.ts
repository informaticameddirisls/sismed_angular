import { MaestraCategoria } from './../interfaces/MaestraCategoria.interface';
import { Injectable } from '@angular/core';
import { environments } from '../../../../../environments/environments';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, tap, throwError } from 'rxjs';
import { Ris } from '../../ris/interfaces/ris.interface';

@Injectable({
  providedIn: 'root'
})
export class MaestraCategoriaService {

  private baseUrl = environments.baseUrl;
  private token = localStorage.getItem('token');
  private headers = new HttpHeaders;

  constructor(private http: HttpClient) {
    if (this.token) {
      // Encabezados de solicitud HTTP con el token
      this.headers = new HttpHeaders({
        'Authorization': `Bearer ${this.token}`
      });
    }
   }

   listar(parametros:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/administrador/maestra/maestra_categoria/lista_maestra_categoria`,parametros, { headers }).pipe(
        tap((maestraCategoria) => {
        }),
        catchError(error => {
          console.error('Error al listar maestraCategoria:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }
}
