export interface Supermodulo {
  id_supermodulo: number;
  nombre: string;
  ruta: string;
  descripcion: string;
  fecha_creacion: string; // Cambiado a tipo string para mantener el formato original
  fecha_modificacion: string | null; // Puede ser una cadena o nulo
  usuario_creacion: string | null; // Puede ser una cadena o nulo
  usuario_modificacion: string | null; // Puede ser una cadena o nulo
  estado: boolean;
  eliminado: boolean;
  created_at: string; // Cambiado a tipo string para mantener el formato original
  updated_at: string; // Cambiado a tipo string para mantener el formato original
  orden: number;
}

