import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';
import { SupermoduloRoutingModule } from './supermodulo-routing.module';



@NgModule({ declarations: [], imports: [CommonModule,
        MaterialModule,
        SupermoduloRoutingModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class SupermoduloModule { }
