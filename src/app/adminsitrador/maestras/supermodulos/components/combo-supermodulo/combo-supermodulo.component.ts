import { AsyncPipe, CommonModule } from '@angular/common';
import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, forwardRef } from '@angular/core';
import { ControlValueAccessor, FormControl, FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Observable, map, startWith, tap } from 'rxjs';
import { SupermoduloService } from '../../services/supermodulo.service';
import { Supermodulo } from '../../interfaces/Supermodulo.interface';
import { MatSelectModule } from '@angular/material/select';

@Component({
    selector: 'app-combo-supermodulo',
    imports: [FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule,
        ReactiveFormsModule, CommonModule, AsyncPipe],
    templateUrl: './combo-supermodulo.component.html',
    styleUrl: './combo-supermodulo.component.css'
})

export class ComboSupermoduloComponent implements ControlValueAccessor, OnInit, AfterViewInit,OnChanges {
  myControl = new FormControl();
  options: Supermodulo[] = [];
  filteredOptions!: Observable<Supermodulo[]>;

  @Output() supermoduloSeleccionado: EventEmitter<any> = new EventEmitter<any>();
  @Input() supermodulosInput!: number | string;

  constructor(private supermoduloService: SupermoduloService,private cdref: ChangeDetectorRef) {
  }

  writeValue(value: any): void {

    if (value) {
      this.myControl.setValue(value);
    } else {
      this.myControl.setValue(null);
    }
  }
  registerOnChange(fn: any): void {
    this.myControl.valueChanges.subscribe(fn);
  }
  registerOnTouched(fn: any): void {
    this.myControl.valueChanges.subscribe(fn);
  }
  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }
  ngAfterViewInit(): void {

  }

  ngOnInit(): void {

    //this.myControl.setValue(this.supermodulosInput);
    this.supermoduloService.listarSupermoduloCombo().subscribe(response => {
    this.options = response.data;
    const selectedSupermodulo = this.options.find(option => option.id_supermodulo === this.supermodulosInput);
      if (selectedSupermodulo) {
        // Establecer el valor del control con el supermódulo encontrado
        this.myControl.setValue(selectedSupermodulo.nombre);
      }

      this.filteredOptions = this.myControl.valueChanges.pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.nombre),
        map(nombre => this._filter(nombre))
      );
    });
  }

  onSelectionChange(event: MatAutocompleteSelectedEvent) {

    const selectedSupermodulo = event.option.value  as Supermodulo;

    this.myControl.setValue(selectedSupermodulo.nombre);
    this.supermoduloSeleccionado.emit(selectedSupermodulo);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('supermodulosInput' in changes && this.supermodulosInput == null) {
      this.myControl.setValue('');
      const newSupermodulo = changes['supermodulosInput'].currentValue;
      if (newSupermodulo) {
        this.selectOptionIfExactMatch();
        //this.loadModulosBySupermodulo(newSupermodulo);

      }
    }
  }


  displayFn(option: Supermodulo): string {
    return option && option.nombre ? option.nombre : '';
  }

  private _filter(value: string): Supermodulo[] {
    if (value=="") {
      //this.supermoduloSeleccionado.emit("");
    }
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.nombre.toLowerCase().includes(filterValue));
  }

  selectOptionIfExactMatch() {
    const userInput = this.myControl.value;
    const matchingOption = this.options.find(option => option.nombre.toLowerCase() === userInput.toLowerCase());
    if (matchingOption) {
      this.myControl.setValue(matchingOption);
      this.supermoduloSeleccionado.emit(matchingOption);
    }
  }

}
