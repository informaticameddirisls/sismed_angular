import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { SupermoduloService } from '../../services/supermodulo.service';
import { UpdateListService } from '../../../../../shared/services/update-list.service';
import { CommonModule } from '@angular/common';

@Component({
    selector: 'app-supermodulo-form',
    imports: [MatFormFieldModule, MatInputModule, MatIconModule, MatCardModule, MatButtonModule, ReactiveFormsModule, CommonModule],
    templateUrl: './supermodulo-form.component.html',
    styleUrl: './supermodulo-form.component.css'
})
export class SupermoduloFormComponent implements OnInit{
  formularioSupermodulo!: FormGroup;

  @Output() crearConfirmado: EventEmitter<boolean> = new EventEmitter<boolean>();



  constructor(
    public dialogRef: MatDialogRef<SupermoduloFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private supermoduloService: SupermoduloService,
    private updateListService: UpdateListService
  ){
    this.formularioSupermodulo = this.fb.group({
      id_supermodulo: [this.data.id_supermodulo || 0],
      nombre: [this.data.nombre || '', Validators.required],
      descripcion: [this.data.descripcion || '', Validators.required],
      ruta: [this.data.ruta || '', [Validators.required]]
    });
  }
  ngOnInit(): void {
    this.initializeForm();

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  initializeForm(): void {
    this.formularioSupermodulo = this.fb.group({
      id_supermodulo: [this.data?.id_supermodulo || 0],
      nombre: [this.data?.nombre || '', Validators.required],
      descripcion: [this.data?.descripcion || '', Validators.required],
      ruta: [this.data?.ruta || '', Validators.required]
    });
  }

  guardarSupermodulo(): void{
    console.log(this.formularioSupermodulo.valid);
    if (this.formularioSupermodulo.valid) {
      const supermodulo = this.formularioSupermodulo.value;
      this.supermoduloService.crear(supermodulo).subscribe(
        (response)=>{
          if (response.success) {
            this.confirmarCreacion();
          }
        }
      )
      console.log('Datos a guardar:', supermodulo);

      this.updateListService.emitUpdateEvent();
      this.dialogRef.close(true);
    } else {
      this.formularioSupermodulo.markAllAsTouched();
    }
  }

  editarSupermodulo(): void{
    console.log(this.formularioSupermodulo.valid);
    if (this.formularioSupermodulo.valid) {
      const supermodulo = this.formularioSupermodulo.value;
      this.supermoduloService.editar(supermodulo).subscribe(
        (response)=>{
          if (response.success) {
            this.confirmarCreacion();
          }
        }
      )
      console.log('Datos a guardar:', supermodulo);

      this.updateListService.emitUpdateEvent();
      this.dialogRef.close(true);
    } else {
      this.formularioSupermodulo.markAllAsTouched();
    }
  }

  confirmarCreacion(): void {
    this.crearConfirmado.emit(true); // Emitir true para confirmar eliminación
    this.dialogRef.close(); // Cerrar el diálogo
  }

  cancelarCreacion(): void {
    this.crearConfirmado.emit(false); // Emitir false para cancelar eliminación
    this.dialogRef.close(); // Cerrar el diálogo
  }
}
