import { Component } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-supermodulo-page',
    imports: [RouterOutlet],
    templateUrl: './layout-supermodulo-page.component.html',
    styleUrl: './layout-supermodulo-page.component.css'
})
export class LayoutSupermoduloPageComponent {

  constructor(private router: Router ) {

   }

}
