import { AfterViewInit, Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { SupermoduloService } from '../../services/supermodulo.service';
import { CommonModule } from '@angular/common';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { CdkDragDrop, CdkDragStart, moveItemInArray, DragDropModule } from '@angular/cdk/drag-drop';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { Supermodulo } from '../../interfaces/Supermodulo.interface';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import { SupermoduloFormComponent } from '../../components/supermodulo-form/supermodulo-form.component';
import { UpdateListService } from '../../../../../shared/services/update-list.service';
import { DialogEliminarComponent } from '../../../../../shared/components/dialog-eliminar/dialog-eliminar.component';
import { DataService } from '../../../../../shared/services/data.service';
@Component({
    selector: 'app-listar-supermodulo-page',
    imports: [CommonModule, MatTableModule, MatPaginatorModule, DragDropModule, MatFormFieldModule, MatIconModule, MatMenuModule, MatButtonModule],
    templateUrl: './listar-supermodulo-page.component.html',
    styleUrl: './listar-supermodulo-page.component.css'
})
export class ListarSupermoduloPageComponent implements OnInit {
  dataSource!: MatTableDataSource<Supermodulo>;
  supermoduloColumnas: string[] = ['id_supermodulo', 'nombre', 'ruta', 'estado', 'orden', 'acciones'];

  registroAEliminar: Supermodulo | null = null;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  titulo: string = 'SUPERMODULO';
  @Output() tituloEnviado = new EventEmitter<string>();

  @Output() toggleEvent = new EventEmitter<boolean>();
  constructor(
    private supermoduloService: SupermoduloService,
    private dataService: DataService,
    private dialog: MatDialog) { }

  ngOnInit(): void {
    this.listarSupermodulo();
    this.dataService.setPageTitle(this.titulo);
  }

  drop(event: CdkDragDrop<Supermodulo[]>) {

    moveItemInArray(this.dataSource.data, event.previousIndex, event.currentIndex);

    this.reordenarSupermodulo(this.dataSource.data);

    this.dataSource.data = [...this.dataSource.data]; // Para forzar la actualización de la vista
  }

  ngAfterViewInit() {
  }

  listarSupermodulo(): void {
    this.supermoduloService.listar().subscribe(
      (response) => {
        if (response.success) {
          this.dataSource = new MatTableDataSource(response.data);
          this.dataSource.paginator = this.paginator;
        } else {
          console.error('Error al listar supermódulos:', response);
        }
      },
      (error) => {
        console.error('Error al listar supermódulos:', error);
      }
    );
  }

  toogleEstado(toogleEstado: any, i: number): void {
    console.log(toogleEstado,i);

    this.supermoduloService.cambiarEstado(toogleEstado).subscribe(
      (response) => {
        console.log(response);
        this.dataSource.data[i] = response.data;
        this.dataSource.data = [...this.dataSource.data];
        return;

      }
    )
    console.log(toogleEstado)
  }

  reordenarSupermodulo(supermodulo: Supermodulo[]): void {
    this.supermoduloService.reordenar(supermodulo).subscribe(
      (response) => {
        this.dataSource = new MatTableDataSource(response.data);
        this.dataSource.paginator = this.paginator;
      },
      (error) => {
        console.error('Error al listar supermódulos:', error);
      }
    );
  }


  editarSupermodulo(supermodulo: Supermodulo): void {
    const dialogRef = this.dialog.open(SupermoduloFormComponent, {

      data: supermodulo || {}
    });

    dialogRef.componentInstance.crearConfirmado.subscribe( confirmado => {
      if (confirmado) {
        this.listarSupermodulo();
      }

    })

  }

  abrirDialogoEliminar(supermodulo: any): void {
    this.registroAEliminar = supermodulo;
    const dialogRef = this.dialog.open(DialogEliminarComponent, {
      data: {
        registro: this.registroAEliminar,
        mensaje: "Desea eliminar el registro",
        nombre: "Supermodulo"
      }
    });
    dialogRef.componentInstance.eliminarConfirmado.subscribe(confirmado => {
      console.log(confirmado)
      if (confirmado) {
        this.eliminarSupermodulo(this.registroAEliminar);
      }
    });
  }


  eliminarSupermodulo(supermodulo: any): void {
    this.supermoduloService.eliminar(supermodulo).subscribe(
      (response) => {
        this.listarSupermodulo();
        console.log(response,'Supermódulo eliminado exitosamente.');
      },
      error => {
        console.error('Error al eliminar supermódulo:', error);
      }
    );
  }

  abrirFormulario(supermoduloEditar?: any) {
    const dialogRef = this.dialog.open(SupermoduloFormComponent, {

      data: {supermodulo: supermoduloEditar}
    });

    dialogRef.componentInstance.crearConfirmado.subscribe(confirmado => {
      console.log(confirmado,"actualizado")
      if (confirmado) {
        this.listarSupermodulo();
      }
    });

  }
}
