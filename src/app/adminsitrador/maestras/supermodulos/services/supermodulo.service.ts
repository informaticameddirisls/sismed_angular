import { Injectable } from '@angular/core';
import { environments } from '../../../../../environments/environments';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, of, tap, throwError } from 'rxjs';
import { Supermodulo } from '../interfaces/Supermodulo.interface';

@Injectable({
  providedIn: 'root'
})
export class SupermoduloService {

  private baseUrl = environments.baseUrl;
  private token = localStorage.getItem('token');
  private headers = new HttpHeaders;


  constructor(private http: HttpClient) {
    if (this.token) {
      // Encabezados de solicitud HTTP con el token
      this.headers = new HttpHeaders({
        'Authorization': `Bearer ${this.token}`
      });
    }
   }

  listar(): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/supermodulo/listar`, { headers }).pipe(
        tap((supermodulo) => {
        }),
        catchError(error => {
          console.error('Error al listar supermódulos:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  listarSupermoduloCombo(): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/administrador/maestra/supermodulo/lista_supermodulo_combo`, { headers }).pipe(
        tap((supermodulo) => {
        }),
        catchError(error => {
          console.error('Error al listar Supermodulo Combo supermódulos:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  reordenar(supermodulo:Supermodulo[]): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/supermodulo/reordenar`, {supermodulo} , { headers }).pipe(
        tap((supermodulo) => {
        }),
        catchError(error => {
          console.error('Error al listar supermódulos:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  cambiarEstado(supermodulo:Supermodulo): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/supermodulo/cambiarEstado`, {supermodulo} , { headers }).pipe(
        tap((supermodulo) => {
        }),
        catchError(error => {
          console.error('Error al listar supermódulos:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }


  crear(supermodulo:Supermodulo): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/supermodulo/crear`, { supermodulo } , { headers }).pipe(
        tap((response) => {
          console.log(response,'crear');
        }),
        catchError(error => {
          console.error('Error al listar supermódulos:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  editar(supermodulo:Supermodulo): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.put<any>(`${this.baseUrl}/api/supermodulo/editar/${supermodulo.id_supermodulo}`, { supermodulo } , { headers }).pipe(
        tap((response) => {
          console.log(response,'editar');
        }),
        catchError(error => {
          console.error('Error al listar supermódulos:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  eliminar(supermodulo:Supermodulo): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/supermodulo/eliminar/${supermodulo.id_supermodulo}`, {} , { headers }).pipe(
        tap((response) => {
          console.log(response,'eliminar');
        }),
        catchError(error => {
          console.error('Error al listar supermódulos:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }
}
