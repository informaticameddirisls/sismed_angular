import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarSupermoduloPageComponent } from './pages/listar-supermodulo-page/listar-supermodulo-page.component';
import { AuthGuard } from '../../../auth/guards/auth.guard';

const routes: Routes = [
  {

    path: 'supermodulo',
    children:
      [
        {
          path: 'listar',
          component: ListarSupermoduloPageComponent,
          canActivate: [AuthGuard]
        }
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupermoduloRoutingModule { }
