import { AfterContentChecked, AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { EstrategiaService } from '../../services/estrategia.service';
import { ControlValueAccessor, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, map, startWith } from 'rxjs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { AsyncPipe, CommonModule } from '@angular/common';
import { MatSelectModule } from '@angular/material/select';

@Component({
    selector: 'app-combo-estrategia',
    imports: [
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule,
        ReactiveFormsModule,
        CommonModule,
        AsyncPipe,
        MatFormFieldModule, MatSelectModule, FormsModule, ReactiveFormsModule,
    ],
    templateUrl: './combo-estrategia.component.html',
    styleUrl: './combo-estrategia.component.css'
})
export class ComboEstrategiaComponent implements ControlValueAccessor, OnChanges, OnInit, AfterViewInit, AfterContentChecked {
  //selectedOptions = new FormControl();
  myControl = new FormControl();
  options: any[] = [];
  filteredOptions!: Observable<any[]>;

  @Input() estrategiaInput!: any;
  @Input() EstrategiaTipo!: string;
  @Input() risSeleccionado!: number | string;
  @Output() estrategiaSeleccionado: EventEmitter<any> = new EventEmitter<any>();

  someMethod(event: any): void {

    const selectedValues = event.value;
    const itemClickeado = event.source._keyManager._activeItem.value;

    if (selectedValues.includes(0) && itemClickeado == 0) {

      console.log(selectedValues.includes(0));
      console.log(itemClickeado);
      // Si se selecciona "Todos", seleccionar todas las opciones
      const allEstrategiaIds = this.options.map(option => option.id_progr_estrat);

      console.log(this.options);
      const selectedOptions = [...allEstrategiaIds, 0]; // Incluir también la opción "TODOS" (0)

      console.log(this.myControl);
      this.myControl.setValue(selectedOptions);
      console.log(this.myControl);
    }
    // Si se seleccionan otras opciones, emitir los valores seleccionados
    if (selectedValues.includes(0) == false && itemClickeado != 0) {
      this.myControl.value.filter((option: any) => option !== selectedValues.value)
    }
    if (selectedValues.includes(0) == false && itemClickeado == 0) {
      this.myControl.setValue([]);
    }
    this.estrategiaSeleccionado.emit(this.myControl.value);
  }


  constructor(private estrategiaService: EstrategiaService,private cd: ChangeDetectorRef) {}

  writeValue(value: any): void {

    if (value) {
      this.myControl.setValue(value);
    } else {
      this.myControl.setValue(null);
    }
  }
  registerOnChange(fn: any): void {
    this.myControl.valueChanges.subscribe(fn);
  }
  registerOnTouched(fn: any): void {
    this.myControl.valueChanges.subscribe(fn);
  }
  ngAfterViewInit(): void {

  }
  ngAfterContentChecked(): void {
    this.cd.detectChanges();
  }

  ngOnInit(): void {
    var tipo = this.EstrategiaTipo;
    var parametros = {tipo}
    this.estrategiaService.listar(parametros).subscribe(response => {
      this.options = response.resultado;

      this.selectPermisoByInput(this.estrategiaInput);

      this.updateFilteredOptions();
    });
    // Configuración de opciones filtradas
  }

  ngOnChanges(changes: SimpleChanges): void {

    if (changes && 'risSeleccionado' in changes ) {

      var tipo = this.EstrategiaTipo;
      var ris = changes['risSeleccionado'].currentValue?.id_tipo;
      var parametros = {ris,tipo}
      //console.log(changes,"changeschangeschanges")
      this.estrategiaService.listar(parametros).subscribe(response => {
        ///console.log(response)
        this.options = response.data;
        this.selectPermisoByInput(this.estrategiaInput);

        this.updateFilteredOptions();

        this.myControl = new FormControl([]);
        this.estrategiaSeleccionado.emit(this.myControl.value);
      });
    }
  }

  onSelectionChange(event: MatAutocompleteSelectedEvent): void {

  }

  filterOptions(value: any): any[] {

    const filterValue = (value && typeof value === 'string') ? value.toLowerCase() : '';
    return this.options.filter(option => option.nombre.toLowerCase().includes(filterValue));
  }


  private updateFilteredOptions(): void {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(null),
      map(value => this.filterOptions(value))
    );
  }

  private selectPermisoByInput(estrategiaInput: any): void {
    console.log(estrategiaInput);
    if (estrategiaInput != undefined) {
      const selectedPermiso = this.options.find(option => option.id_progr_estrat === estrategiaInput.id_progr_estrat);

      if (selectedPermiso) {
        this.myControl = new FormControl([selectedPermiso.id_progr_estrat]);
        this.estrategiaSeleccionado.emit(this.myControl.value);
      }
    }

  }

}
