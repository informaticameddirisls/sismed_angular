import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarEstrategiaPageComponent } from './pages/listar-estrategia-page/listar-estrategia-page.component';
import { AuthGuard } from '../../auth/guards/auth.guard';

const routes: Routes = [
  {
    path: 'estrategia',
    children:

      [{
        path: 'listar',
        component: ListarEstrategiaPageComponent,
        canActivate: [AuthGuard]
      }]
  }
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstrategiaRoutingModule { }
