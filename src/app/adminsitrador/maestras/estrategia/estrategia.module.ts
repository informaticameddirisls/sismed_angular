import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../../material/material.module';
import { EstrategiaRoutingModule } from './estrategia-routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    CommonModule,
    MaterialModule,
    EstrategiaRoutingModule

  ]
})
export class EstrategiaModule { }
