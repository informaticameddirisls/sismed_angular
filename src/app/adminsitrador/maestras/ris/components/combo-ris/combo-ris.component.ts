import { AsyncPipe, CommonModule } from '@angular/common';
import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ControlValueAccessor, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Ris } from '../../interfaces/ris.interface';
import { Observable, map, startWith } from 'rxjs';
import { RisService } from '../../services/ris.service';

@Component({
    selector: 'app-combo-ris',
    imports: [
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule,
        ReactiveFormsModule,
        CommonModule
    ],
    templateUrl: './combo-ris.component.html',
    styleUrl: './combo-ris.component.css'
})
export class ComboRisComponent implements   OnInit {
  myControl = new FormControl();
  options: Ris[] = [];

  @Output() risSeleccionado: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private risService: RisService,private cd: ChangeDetectorRef
  ){}

  ngAfterContentChecked() {
    this.cd.detectChanges();
  }


  trackOption(index: number, option: any): string {

    return option ? option.id : undefined;
  }

  ngOnInit(): void {
    this.risService.listar().subscribe((response)=>{
      this.options = response.data;
    })
  }

  onSelectionChange(event: MatAutocompleteSelectedEvent): void {

    const selectedRis = event.option.value as Ris;
    this.myControl.setValue(selectedRis.descripcion);
    this.risSeleccionado.emit(selectedRis);
  }
}
