import { Injectable } from '@angular/core';
import { environments } from '../../../../../environments/environments';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, tap, throwError } from 'rxjs';
import { Ris } from '../interfaces/ris.interface';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class RisService {
  private baseUrl = environments.baseUrl;
  private token = localStorage.getItem('token');
  private headers = new HttpHeaders;

  constructor(private http: HttpClient
  ) {
    if (this.token) {
      // Encabezados de solicitud HTTP con el token
      this.headers = new HttpHeaders({
        'Authorization': `Bearer ${this.token}`
      });
    }
  }


  listar(): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/administrador/maestra/almacen/listar_ris_combo`,{ headers }).pipe(
        tap((supermodulo) => {
        }),
        catchError(error => {
          console.error('Error al listar ris:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

}
