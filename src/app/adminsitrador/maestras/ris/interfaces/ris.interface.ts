export interface Ris {
  id:          number;
  id_tabla:    string;
  id_tipo:     string;
  descripcion: string;
  abreviado:   null;
}
