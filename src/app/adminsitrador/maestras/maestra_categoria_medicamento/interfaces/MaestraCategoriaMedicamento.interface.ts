import { Medicamento } from "../../medicamentos/interfaces/Medicamento.interface";


export interface MaestraCategoriaMedicamento {
  id_maestra_categoria_medicamento: number;
  id_maestra_categoria:             number;
  id_medicamento:                   string;
  cantidad:                         number;
  medicamento: Medicamento[];
}
