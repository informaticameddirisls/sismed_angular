import { CommonModule } from '@angular/common';
import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { ComboSupermoduloComponent } from '../../../supermodulos/components/combo-supermodulo/combo-supermodulo.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ModuloService } from '../../../modulos/services/modulo.service';
import { UpdateListService } from '../../../../../shared/services/update-list.service';
import { FormValidationService } from '../../../submodulos/validators/form-validation.service';
import { ComboModuloComponent } from "../../../modulos/components/combo-modulo/combo-modulo.component";
import { SubmoduloService } from '../../services/submodulo.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatCheckboxModule } from '@angular/material/checkbox';

@Component({
    selector: 'app-modal-submodulo-form',
    templateUrl: './modal-submodulo-form.component.html',
    styleUrl: './modal-submodulo-form.component.css',
    imports: [MatFormFieldModule, MatInputModule, MatIconModule, MatCardModule, MatButtonModule, ReactiveFormsModule, CommonModule, ComboSupermoduloComponent, ComboModuloComponent, MatCheckboxModule]
})
export class ModalSubmoduloFormComponent implements OnInit, AfterViewInit  {
  formularioSubmodulo!: FormGroup;
  selectedSupermodulo: any = null;
  selectedModulo: any = null;
  selectedSubmodulo: any = null;

  @Output() crearConfirmado: EventEmitter<boolean> = new EventEmitter<boolean>();




  constructor(
    public dialogRef: MatDialogRef<ModalSubmoduloFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private submoduloService: SubmoduloService,
    private updateListService: UpdateListService,
    private formValidationService: FormValidationService,
    private cdref: ChangeDetectorRef,
    private _snackBar: MatSnackBar
  ){


  }

  ngAfterContentChecked() {
    //this.formularioSubmodulo.patchValue({id_supermodulo: this.selectedSubmodulo});
    //this.formularioSubmodulo.patchValue({id_modulo: this.selectedModulo})
    this.cdref.detectChanges();
  }

  ngAfterViewInit(): void {


  }

  ngOnInit(): void {
    this.formularioSubmodulo = this.fb.group({
      id_submodulo: [this.data.submodulo?.id_submodulo || 0],
      id_supermodulo: [this.data.submodulo?.modulo.id_supermodulo || null],
      id_modulo: [this.data.submodulo?.id_modulo || null, Validators.required],
      nombre: [this.data.submodulo?.nombre || '', Validators.required],
      descripcion: [this.data.submodulo?.descripcion || '', Validators.required],
      ruta: [this.data.submodulo?.ruta || '', [Validators.required]],
      almacenes: [this.data.submodulo?.almacenes ]
    });

    if (!!this.data.submodulo) {
      this.onSupermoduloSeleccionado(this.data.submodulo?.modulo.supermodulo);
      //console.log(this.data.submodulo.id_modulo,"this.data.submodulo.id_modulo");
      this.onModuloSeleccionado(this.data.submodulo);
      //this.onModuloSeleccionado(this.data.submodulo.modulo);
    }

  }

  onSupermoduloSeleccionado(supermodulo: any): void {

    this.selectedSupermodulo = supermodulo.id_supermodulo;
    //this.selectedModulo = null;
    this.formularioSubmodulo.patchValue({id_supermodulo: this.selectedSupermodulo})

  }

  onModuloSeleccionado(modulo: any): void {

    if (modulo == null) {
      this.selectedModulo=null
      this.formularioSubmodulo.patchValue({id_modulo: null})
      return
    }
    this.selectedModulo = modulo.id_modulo;
    this.formularioSubmodulo.patchValue({id_modulo: this.selectedModulo})

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  guardarSupermodulo(): void{

    if (this.formularioSubmodulo.valid) {
      const submodulo = this.formularioSubmodulo.value;
      this.submoduloService.crear(submodulo).subscribe(
        (response)=>{
          if (response.success) {
            this._snackBar.open(`REGISTRO GUARDADO CORRECTAMENTE`, 'ok', {
              horizontalPosition: 'start',
              verticalPosition: 'bottom',
              duration: 10 * 1000,
            });
            this.confirmarCreacion();
          }
        }
      )
      console.log('Datos a guardar:', submodulo);

      this.updateListService.emitUpdateEvent();
      this.dialogRef.close(true);
    } else {
      this.formularioSubmodulo.markAllAsTouched();
    }
  }

  editarSupermodulo(): void{

    console.log(this.formularioSubmodulo);

    if (this.formularioSubmodulo.valid) {
      const submodulo = this.formularioSubmodulo.value;
      this.submoduloService.editar(submodulo).subscribe(
        (response)=>{
          if (response.success) {
            this._snackBar.open(`REGISTRO ACTUALIZADO CORRECTAMENTE`, 'ok', {
              horizontalPosition: 'start',
              verticalPosition: 'bottom',
              duration: 10 * 1000,
            });
            this.confirmarCreacion();
          }
        }
      )
      console.log('Datos a guardar:', submodulo);

      this.updateListService.emitUpdateEvent();
      this.dialogRef.close(true);
    } else {
      this.formularioSubmodulo.markAllAsTouched();
    }
  }

  esCampoValido( field: string ): boolean | null {

    return this.formValidationService.esCampoValido( this.formularioSubmodulo,field);
  }

  mensajeError( field: string ): string | null {
    return this.formValidationService.mensajeError(this.formularioSubmodulo,field);
  }


  confirmarCreacion(): void {
    this.crearConfirmado.emit(true); // Emitir true para confirmar eliminación
    this.dialogRef.close(); // Cerrar el diálogo
  }

  cancelarCreacion(): void {
    this.crearConfirmado.emit(false); // Emitir false para cancelar eliminación
    this.dialogRef.close(); // Cerrar el diálogo
  }
}
