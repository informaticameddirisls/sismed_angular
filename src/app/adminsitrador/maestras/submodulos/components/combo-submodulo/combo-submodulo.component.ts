import { AfterContentChecked, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { SubmoduloService } from '../../services/submodulo.service';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Submodulo } from '../../interfaces/Submodulo.interface';
import { Observable, map, startWith } from 'rxjs';
import { Modulo } from '../../../modulos/interfaces/Modulo.interface';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { AsyncPipe, CommonModule } from '@angular/common';

@Component({
    selector: 'app-combo-submodulo',
    imports: [FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule,
        ReactiveFormsModule,
        CommonModule,
        AsyncPipe],
    templateUrl: './combo-submodulo.component.html',
    styleUrl: './combo-submodulo.component.css'
})
export class ComboSubmoduloComponent implements OnChanges, AfterContentChecked {
  myControl = new FormControl();
  options: Submodulo[] = [];
  filteredOptions!: Observable<Submodulo[]>;
  selectedModulo: Modulo | undefined;

  @Input() moduloSeleccionado!: Modulo;
  @Input() submodulosInput!: number | string;

  @Output() submoduloSeleccionado: EventEmitter<any> = new EventEmitter<any>();


  constructor(private submoduloService: SubmoduloService,private cd: ChangeDetectorRef
  ) {
  }
  ngAfterContentChecked(): void {
    this.cd.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges): void {

    if ('moduloSeleccionado' in changes) {
      this.myControl.setValue('');
      const newModulo = changes['moduloSeleccionado'].currentValue;
      if (newModulo) {

        this.loadModulosBySupermodulo(newModulo);

      }
    }

  }

  private loadModulosBySupermodulo(modulo: any): void {

    this.submoduloService.lista_submodulo_combo(modulo).subscribe((response) => {

      this.options = response.data;

      const selectedSubmodulo = this.options.find(option => option.id_submodulo === this.submodulosInput);
      if (selectedSubmodulo) {
        // Establecer el valor del control con el supermódulo encontrado
        this.myControl.setValue(selectedSubmodulo.nombre);
      }


      this.updateFilteredOptions();
    });
  }

  private updateFilteredOptions(): void {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.nombre),
      map(nombre => this.options.slice())
    );
  }

  displayFn(option: Submodulo): string {
    return option && option.nombre ? option.nombre : '';
  }


  onSelectionChange(event: MatAutocompleteSelectedEvent): void {
    const selectedSubmodulo = event.option.value;
    console.log(selectedSubmodulo);
    this.myControl.setValue(selectedSubmodulo.nombre);
    this.submoduloSeleccionado.emit(selectedSubmodulo);
  }
}
