import { Injectable } from '@angular/core';
import { environments } from '../../../../../environments/environments';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, tap, throwError } from 'rxjs';
import { Submodulo } from '../interfaces/Submodulo.interface';

@Injectable({
  providedIn: 'root'
})
export class SubmoduloService {
  private baseUrl = environments.baseUrl;
  private token = localStorage.getItem('token');
  private headers = new HttpHeaders;

  constructor(private http: HttpClient) {
    if (this.token) {
      // Encabezados de solicitud HTTP con el token
      this.headers = new HttpHeaders({
        'Authorization': `Bearer ${this.token}`
      });
    }
  }


  listar(parametros:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/submodulo/listar`, parametros,{ headers }).pipe(
        tap((supermodulo) => {
        }),
        catchError(error => {
          console.error('Error al listar submodulo:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  crear(submodulo:Submodulo): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/submodulo/crear`,  submodulo  , { headers }).pipe(
        tap((response) => {
        }),
        catchError(error => {
          console.error('Error al listar submodulo:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  editar(submodulo:Submodulo): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.put<any>(`${this.baseUrl}/api/submodulo/editar/${submodulo.id_submodulo}`,  submodulo  , { headers }).pipe(
        tap((response) => {
          console.log(response,'editar');
        }),
        catchError(error => {
          console.error('Error al listar modulo:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }


  buscarpormodulo(id_modulo: number): Observable<any> {
    var headers = this.headers;
    if (this.token) {

      if (id_modulo!=undefined) {
        return this.http.get<any>(`${this.baseUrl}/api/submodulo/buscarpormodulo/${id_modulo}`, { headers }).pipe(
          tap((supermodulo) => {
          }),
          catchError(error => {
            console.error('Error al listar modulos:', error);
            return throwError(error);
          })
        );
      } else{
        console.log('modulo no enviado.');
        return throwError('modulo no enviado');
      }
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  eliminar(submodulo:Submodulo): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/submodulo/eliminar/${submodulo.id_submodulo}`, { headers }).pipe(
        tap((response) => {

        }),
        catchError(error => {
          console.error('Error al listar supermódulos:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  lista_submodulo_combo(id_modulo: number): Observable<any> {
    var headers = this.headers;
    if (this.token) {

      if (id_modulo!=undefined) {
        return this.http.get<any>(`${this.baseUrl}/api/administrador/maestra/submodulo/lista_submodulo_combo/${id_modulo}`, { headers }).pipe(
          tap((supermodulo) => {
          }),
          catchError(error => {
            console.error('Error al listar modulos:', error);
            return throwError(error);
          })
        );
      } else{
        console.log('modulo no enviado.');
        return throwError('modulo no enviado');
      }
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }



}
