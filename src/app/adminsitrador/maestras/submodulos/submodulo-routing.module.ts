import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';
import { ListarSubmoduloPageComponent } from './pages/listar-submodulo-page/listar-submodulo-page.component';

const routes: Routes = [
  {
    path: 'submodulo',
    children:

      [{
        path: 'listar',
        component: ListarSubmoduloPageComponent,
        canActivate: [AuthGuard]
      }]
  }
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubmoduloRoutingModule { }
