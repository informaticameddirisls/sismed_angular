import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';
import { SubmoduloRoutingModule } from './submodulo-routing.module';



@NgModule({ declarations: [], imports: [CommonModule,
        MaterialModule,
        SubmoduloRoutingModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class SubmoduloModule { }
