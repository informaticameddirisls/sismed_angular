import { Component, ViewChild } from '@angular/core';
import { DataService } from '../../../../../shared/services/data.service';
import { MatDialog } from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { Submodulo } from '../../interfaces/Submodulo.interface';
import { SubmoduloService } from '../../services/submodulo.service';
import { ComboModuloComponent } from "../../../modulos/components/combo-modulo/combo-modulo.component";
import { ModalSubmoduloFormComponent } from '../../components/modal-submodulo-form/modal-submodulo-form.component';
import { ComboSupermoduloComponent } from "../../../supermodulos/components/combo-supermodulo/combo-supermodulo.component";
import { DialogEliminarComponent } from '../../../../../shared/components/dialog-eliminar/dialog-eliminar.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'app-listar-submodulo-page',
    templateUrl: './listar-submodulo-page.component.html',
    styleUrl: './listar-submodulo-page.component.css',
    imports: [CommonModule, FormsModule, MatIconModule, MatPaginatorModule, MatTableModule, MatExpansionModule,
        MatFormFieldModule, MatSelectModule, ComboModuloComponent, ComboSupermoduloComponent]
})
export class ListarSubmoduloPageComponent {
    titulo: string = 'SUBMODULO';

    registroAEliminar: Submodulo | null = null;
    //creacion de la tabla
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    dataSource!: MatTableDataSource<any>;
    submoduloColumnas: string[] = ['id_submodulo', 'nombre', 'ruta', 'descripcion', 'modulo', 'supermodulo',  'almacenes', 'estado', 'acciones'];

    selectedSupermodulo!: any;
    selectedModulo: any;
    selectedEstado!: any;

  constructor(
    private submoduloService: SubmoduloService,
    private dataService: DataService,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar) { }


  ngOnInit(): void {
    this.buscarFiltrado();
    this.dataService.setPageTitle(this.titulo);
  }

  abrirFormulario(submoduloEditar?: any) {
    const dialogRef = this.dialog.open(ModalSubmoduloFormComponent, {

      data: {submodulo: submoduloEditar}
    });

    dialogRef.componentInstance.crearConfirmado.subscribe(confirmado => {
      if (confirmado) {
        this.buscarFiltrado();
      }
    });

  }

  editarSubmodulo(submoduloEditar: Submodulo): void {

    const dialogRef = this.dialog.open(ModalSubmoduloFormComponent, {

      data: {submodulo: submoduloEditar}
    });

    dialogRef.componentInstance.crearConfirmado.subscribe( confirmado => {
      if (confirmado) {
        this.buscarFiltrado();
      }

    })

  }

  listarSubmodulo(parametros:any = {}): void {
    this.submoduloService.listar(parametros).subscribe(
      (response) => {
        if (response.success) {
          this.dataSource = new MatTableDataSource(response.data);
          this.dataSource.paginator = this.paginator;
        } else {
          console.error('Error al listar supermódulos:', response);
        }
      },
      (error) => {
        console.error('Error al listar supermódulos:', error);
      }
    );
  }


  abrirDialogoEliminar(Submodulo: any): void {
    this.registroAEliminar = Submodulo;
    const dialogRef = this.dialog.open(DialogEliminarComponent, {
      data: {
        registro: this.registroAEliminar,
        mensaje: "Desea eliminar el registro",
        nombre: "Submodulo"
      }
    });
    dialogRef.componentInstance.eliminarConfirmado.subscribe(confirmado => {
      console.log(confirmado)
      if (confirmado) {
        this.eliminarSubmodulo(this.registroAEliminar);
      }
    });
  }

  eliminarSubmodulo(submodulo: any): void {
    this.submoduloService.eliminar(submodulo).subscribe(
      (response) => {
        if (response.success) {
          this._snackBar.open(`REGISTRO ELIMINADO CORRECTAMENTE`, 'ok', {
            horizontalPosition: 'start',
            verticalPosition: 'bottom',
            duration: 10 * 1000,
          });
          this.buscarFiltrado();
        console.log(response,'Supermódulo eliminado exitosamente.');
        }else{
          this._snackBar.open(`REGISTRO NO ELIMINADO`, 'ok', {
            horizontalPosition: 'start',
            verticalPosition: 'bottom',
            duration: 10 * 1000,
          });
        }

      },
      error => {
        console.error('Error al eliminar supermódulo:', error);
      }
    );
  }

  onSupermoduloSeleccionado(supermodulo: any): void {
    if (supermodulo == null) {
      this.selectedSupermodulo = null
      return
    }
    this.selectedSupermodulo = supermodulo.id_supermodulo;

  }

  onModuloSeleccionado(modulo: any): void {
    if (modulo == null) {
      this.selectedModulo = null
      return
    }
    this.selectedModulo = modulo.id_modulo;


  }


  buscarFiltrado(){
    var selectedSupermodulo = this.selectedSupermodulo;
    var selectedModulo = this.selectedModulo;
    var selectedEstado = this.selectedEstado;
    var parametros = {selectedSupermodulo,selectedModulo,selectedEstado}

    this.listarSubmodulo(parametros);

  }

}
