import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-submodulo-page',
    imports: [RouterOutlet],
    templateUrl: './layout-submodulo-page.component.html',
    styleUrl: './layout-submodulo-page.component.css'
})
export class LayoutSubmoduloPageComponent {

}
