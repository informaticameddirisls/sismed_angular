import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';

import { ListarResolucionPageComponent } from './pages/listar-resolucion-page/listar-resolucion-page.component';

const routes: Routes = [
  {

    path: 'resolucion',
    children:
      [
        {
          path: 'eval/:id',
          component: ListarResolucionPageComponent
        }
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResolucionRoutingModule { }
