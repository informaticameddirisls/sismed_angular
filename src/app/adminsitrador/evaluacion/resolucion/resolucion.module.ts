import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';
import { ResolucionRoutingModule } from './resolucion-routing.module';



@NgModule({ declarations: [], imports: [CommonModule,
        ResolucionRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class ResolucionModule { }
