import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'adm-eval-resol-layout-resolucion-page',
    imports: [RouterOutlet],
    templateUrl: './layout-resolucion-page.component.html',
    styleUrl: './layout-resolucion-page.component.css'
})
export class LayoutResolucionPageComponent {

}
