import { CommonModule, DatePipe, JsonPipe } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, inject, ViewChild } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { DataService } from '../../../../../shared/services/data.service';
import { ExcelService } from '../../../../../shared/services/excel.service';
import { MatDialog } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { provideMomentDateAdapter } from '@angular/material-moment-adapter';
import 'moment/locale/es';
import {  ResolucionService } from '../../services/resolucion.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { ComboDocumentoIdentidadService } from '../../../../../shared/services/combo/combo-documento-identidad.service';
import { map, Observable } from 'rxjs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { FormValidationService } from '../../validators/form-validation.service';
import { UsuarioService } from '../../../../../usuarios/services/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {ToastModule} from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { RippleModule } from 'primeng/ripple';


@Component({
    selector: 'adm-eval-resol-listar-resolucion-page',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es' },
        provideMomentDateAdapter(),
        MessageService
    ],
    templateUrl: './listar-resolucion-page.component.html',
    styleUrl: './listar-resolucion-page.component.css',
    imports: [
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatExpansionModule,
        MatButtonModule,
        MatMenuModule,
        CommonModule, MatCardModule, MatDatepickerModule, FormsModule, ReactiveFormsModule, MatCheckboxModule, MatRadioModule,
        MatFormFieldModule, MatSelectModule, MatInputModule, ToastModule, RippleModule
    ]
})
export class ListarResolucionPageComponent {



  formularioPersona: FormGroup;
  comboDocumentoIdentidad$: Observable<any[]>;
  titulo: string = 'RESOLUCION';
  id_evaluacion = this.activateRoute.snapshot.params['id'];
  pregunta_alternativa: any;



  ver_formulario_datos: boolean = false;
  boton_buscar_persona: boolean = true;
  boton_editar_parametros_busqueda: boolean = false;
  boton_editar_datos: boolean = false;
  boton_guardar_datos: boolean = false;
  boton_resolver_evaluacion: boolean = false;
  ver_evaluacion: boolean = false;
  evaluacion!:any;
  isButtonDisabled:Boolean = false;
  evaluacion_activa:Boolean =true;
  constructor(
    private dataService: DataService,
    private dialog: MatDialog,
    private excelService: ExcelService,
    private cdr: ChangeDetectorRef,
    private activateRoute: ActivatedRoute,
    private resolucionService: ResolucionService,
    private formValidationService: FormValidationService,
    private router: Router,
    private fb: FormBuilder,
    private comboDocumentoIdentidadService: ComboDocumentoIdentidadService,
    private _snackBar: MatSnackBar
    ){

      this.formularioPersona = this.fb.group({
        id: [0],
        nombres: ['', Validators.required],
        ape_paterno: ['', Validators.required],
        ape_materno: ['', Validators.required],
        id_tipo_documento: ['', Validators.required],
        numero_documento: ['', Validators.required],
        sexo: [''],
        fecha_nac: [''],
        email: ['', [Validators.required, Validators.email]],
        direccion: [''],
        celular: [''],
      });


      this.comboDocumentoIdentidad$ = this.resolucionService.listarDocIdentidad().pipe(
        map(response => response.resultado) // Ajusta según la estructura de tu respuesta
      );

  }

  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);


    this.resolucionService.buscar_evaluacion(this.id_evaluacion).subscribe(
      (response)=>{
        if (response.success) {
          this.evaluacion = response.data;
          //this.evaluacion_activa = this.evaluacion.estado.valor == 'ACTIVA'
          this.cdr.detectChanges(); // Forzar la actualización de la vista

          //this.router.navigate(['sismed', 'administrador', 'maestra', 'usuario', 'editar', response.data["id_usuario"]]);
        }
      }
    )


    this.resolucionService.preguntas_x_evaluacion(this.id_evaluacion).subscribe(
      (response)=>{


        if (response.success) {
          this.pregunta_alternativa = response.data;
          this.cdr.detectChanges(); // Forzar la actualización de la vista

          //this.router.navigate(['sismed', 'administrador', 'maestra', 'usuario', 'editar', response.data["id_usuario"]]);
        }
      }
    )
  }

  esCampoValido(field: string): boolean | null {
    return this.formValidationService.esCampoValido(this.formularioPersona, field);
  }

  mensajeError(field: string): string | null {
    return this.formValidationService.mensajeError(this.formularioPersona, field);
  }

  // Método para validar y mostrar el mensaje de error
  mostrarError(field: string): boolean | null {
    if (!this.formularioPersona.controls[field]) return false;
    return this.formValidationService.esCampoValido(this.formularioPersona, field);
  }

  buscar_persona(){

    if (this.formularioPersona.value.id_tipo_documento == "") {
      this.formularioPersona.get('id_tipo_documento')?.markAsTouched();
      //this.formularioUsuario.value.id_tipo_documento.markAsTouched();
      return;
    }
    if(this.formularioPersona.value.numero_documento == ''){
      this.formularioPersona.get('numero_documento')?.markAsTouched();
      return;
    }

    this.resolucionService.buscar_x_documento(this.formularioPersona.value.id_tipo_documento,this.formularioPersona.value.numero_documento).subscribe(
      (response)=>{


        if (response.data == null) {
          this.ver_formulario_datos = true;

          this.boton_buscar_persona = false;
          this.boton_editar_parametros_busqueda = false;
          this.boton_editar_datos = false;
          this.boton_guardar_datos = true;
          this.boton_resolver_evaluacion =false;
          this.ver_evaluacion =false;
          this.desbloquearDatosPersonales();
          this.bloquearBuscador();
          return;
        }

        this.formularioPersona.patchValue({
          id: response.data.id,
          nombres: response.data.nombres,
          ape_paterno: response.data.ape_paterno,
          ape_materno: response.data.ape_materno,
          id_tipo_documento: response.data.id_tipo_documento,
          numero_documento: response.data.numero_documento,
          sexo: response.data.sexo,
          fecha_nac: response.data.fecha_nac,
          email: response.data.email,
          direccion: response.data.direccion,
          celular: response.data.celular,
        });
        this.bloquearDatosPersonales();
        this.bloquearBuscador();
        this.ver_formulario_datos = true;

        this.boton_buscar_persona = false;
        this.boton_editar_parametros_busqueda = true;
        this.boton_editar_datos = true;
        this.boton_guardar_datos = false;
        this.boton_resolver_evaluacion =true;
        this.ver_evaluacion =false;
      }
    )
  }

  editar_datos(){
    this.desbloquearDatosPersonales();
    this.ver_formulario_datos = true;

    this.boton_buscar_persona = false;
    this.boton_editar_parametros_busqueda = true;
    this.boton_editar_datos = false;
    this.boton_guardar_datos = true;
    this.boton_resolver_evaluacion = false;
    this.ver_evaluacion =false;
  }

  guardar_datos(){

    this.formularioPersona.markAllAsTouched();

    if (this.formularioPersona.valid) {
        this.desbloquearBuscador();
      var id = this.formularioPersona.value.id;
      var nombres = this.formularioPersona.value.nombres;
      var ape_paterno = this.formularioPersona.value.ape_paterno;
      var ape_materno = this.formularioPersona.value.ape_materno;
      var id_tipo_documento = this.formularioPersona.value.id_tipo_documento;
      var numero_documento = this.formularioPersona.value.numero_documento;
      var sexo = this.formularioPersona.value.sexo;
      var fecha_nac = this.formularioPersona.value.fecha_nac;
      var email = this.formularioPersona.value.email;
      var direccion = this.formularioPersona.value.direccion;
      var celular = this.formularioPersona.value.celular;
      this.bloquearBuscador();
      var parametros = {id,nombres,ape_paterno,ape_materno,id_tipo_documento,numero_documento,sexo,fecha_nac,email,direccion,celular}


      this.resolucionService.guardar_actualizar_persona(parametros).subscribe(
        (response)=>{
          this.formularioPersona.patchValue({
            id: response.data
          });
          if (response.success) {
            this.bloquearDatosPersonales();
            this.bloquearBuscador();
            this.ver_formulario_datos = true;

            this.boton_buscar_persona = false;
            this.boton_editar_parametros_busqueda = false;
            this.boton_editar_datos = false;
            this.boton_guardar_datos = false;
            this.boton_resolver_evaluacion =false;
            this.ver_evaluacion =true;


          }
        }
      )
    }
  }

  editar_documento(){
    this.limpiarFormulario();
    this.desbloquearBuscador();
    this.ver_formulario_datos = false;

    this.boton_buscar_persona = true;
    this.boton_editar_parametros_busqueda = false;
    this.boton_editar_datos = false;
    this.boton_guardar_datos = false;
    this.boton_resolver_evaluacion = false;
  }

  resolver_evaluacion(){
    this.bloquearDatosPersonales();
    this.bloquearBuscador();
    this.ver_formulario_datos = true;

    this.boton_buscar_persona = false;
    this.boton_editar_parametros_busqueda = false;
    this.boton_editar_datos = false;
    this.boton_guardar_datos = false;
    this.boton_resolver_evaluacion =false;
    this.ver_evaluacion =true;
  }

  limpiarFormulario(){
    this.formularioPersona.patchValue({
      id: 0,
      nombres: '',
      ape_paterno: '',
      ape_materno: '',
      id_tipo_documento: '',
      numero_documento: '',
      sexo: '',
      fecha_nac: '',
      email: '',
      direccion: '',
      celular: '',
    });
  }

  bloquearDatosPersonales(){
    this.formularioPersona.get('nombres')?.disable();
    this.formularioPersona.get('ape_paterno')?.disable();
    this.formularioPersona.get('ape_materno')?.disable();
    this.formularioPersona.get('sexo')?.disable();
    this.formularioPersona.get('fecha_nac')?.disable();
    this.formularioPersona.get('email')?.disable();
    this.formularioPersona.get('direccion')?.disable();
    this.formularioPersona.get('celular')?.disable();
  }
  bloquearBuscador(){
    this.formularioPersona.get('id_tipo_documento')?.disable();
    this.formularioPersona.get('numero_documento')?.disable();
  }
  desbloquearDatosPersonales(){
    this.formularioPersona.get('nombres')?.enable();
    this.formularioPersona.get('ape_paterno')?.enable();
    this.formularioPersona.get('ape_materno')?.enable();
    this.formularioPersona.get('sexo')?.enable();
    this.formularioPersona.get('fecha_nac')?.enable();
    this.formularioPersona.get('email')?.enable();
    this.formularioPersona.get('direccion')?.enable();
    this.formularioPersona.get('celular')?.enable();
  }
  desbloquearBuscador(){
    this.formularioPersona.get('id_tipo_documento')?.enable();
    this.formularioPersona.get('numero_documento')?.enable();
  }

  enviar_evaluacion(){
    const alternativaId = document.querySelectorAll('[data-id-alternativa]')

  }


  onSubmit() {
    if (this.formularioPersona.valid) {
      // Procesar el formulario si es válido
      const usuario = this.formularioPersona.value;



      // Llamar al servicio para guardar el usuario
      // this.usuarioService.guardarUsuario(usuario).subscribe(...);
    } else {
      // Marcar todos los campos como tocados para mostrar errores
      this.formularioPersona.markAllAsTouched();
    }
  }

  respuestasFinales: any[] = [];


  enviarRespuestas(): void {
    this.isButtonDisabled = true;
    this.respuestasFinales = [];

    // Seleccionamos todos los checkboxes, radio buttons y textareas en el formulario
    const checkboxes = document.querySelectorAll('mat-checkbox');
    const radios = document.querySelectorAll('mat-radio-button');
    const textareas = document.querySelectorAll('textarea');

    // Recorrer todos los checkboxes seleccionados y capturar su id-alternativa
    checkboxes.forEach((checkbox: any) => {

      const id_alternativa = checkbox.getAttribute('data-id-alternativa');
      const id_pregunta = checkbox.getAttribute('data-id-pregunta');
      const respuestas = checkbox.classList.contains('mat-mdc-checkbox-checked');

      this.respuestasFinales.push({id_alternativa,id_pregunta,respuestas});

    });

    // Recorrer todos los radio buttons seleccionados y capturar su id-alternativa

    radios.forEach((radio: any) => {
      const id_alternativa = radio.getAttribute('data-id-alternativa');
      const id_pregunta = radio.getAttribute('data-id-pregunta');
      const respuestas = radio.classList.contains('mat-mdc-radio-checked');

      this.respuestasFinales.push({id_alternativa,id_pregunta,respuestas});
    });


    // Recoger las respuestas de los textareas
    textareas.forEach((textarea: any) => {
      const id_alternativa = textarea.getAttribute('data-id-alternativa');
      const id_pregunta = textarea.getAttribute('data-id-pregunta');
      const respuestas = textarea.value;
      this.respuestasFinales.push({id_alternativa,id_pregunta,respuestas});
    });

    var parametros ={respuestasFinales:this.respuestasFinales,id_persona:this.formularioPersona.value.id,id_evaluacion:this.id_evaluacion};


    this.resolucionService.enviar_respuestas(parametros).subscribe(
      (response)=>{


        if (response.success) {
          this.openSnackBar();
        }else{
          const snackBarRef = this._snackBar.open('ERROR AL GUARDAR LA INFORMACION POR FAVOR VOLVER A LLENARLO O COMUNICARSE CON SISTEMAS', 'ACEPTAR', {
            horizontalPosition: 'start',
            verticalPosition: 'bottom',
            duration: 10 * 1000,
            panelClass: ['success-snackbar']
          });

          window.location.reload();
        }

      }
    )
  }

  openSnackBar() {
    this.isButtonDisabled = true;
    const snackBarRef = this._snackBar.open('REGISTRO GUARDADO CORRECTAMENTE', 'ok', {
      horizontalPosition: 'start',
      verticalPosition: 'bottom',
      duration: 10 * 1000,
      panelClass: ['success-snackbar']
    });

    // Ejecutar una acción cuando el SnackBar se cierre
    snackBarRef.afterDismissed().subscribe(() => {
      // Aquí puedes poner el código que deseas ejecutar cuando se cierra el SnackBar.
      window.location.reload();
    });
  }

}
