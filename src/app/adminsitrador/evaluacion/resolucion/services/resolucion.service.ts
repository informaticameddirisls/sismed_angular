import { Injectable } from '@angular/core';
import { environments } from '../../../../../environments/environments';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, tap, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ResolucionService {


  private baseUrl = environments.baseUrl;
  // private token = localStorage.getItem('token');
   private headers = new HttpHeaders;

  constructor(private http: HttpClient) {
    // if (this.token) {
    //   // Encabezados de solicitud HTTP con el token
       this.headers = new HttpHeaders({
    //     'Authorization': `Bearer ${this.token}`
       });
    // }
  }

  buscar_evaluacion(parametros?:any): Observable<any> {


      return this.http.get<any>(`${this.baseUrl}/api/administrador/evaluacion/buscar_evaluacion/${parametros}`, {  }).pipe(
        tap((response) => {
          //console.log(response)
        }),
        catchError(error => {
          console.error('Error al lotes por vencer:', error);
          return throwError(error);
        })
      );

  }


  preguntas_x_evaluacion(parametros?:any): Observable<any> {


      return this.http.get<any>(`${this.baseUrl}/api/administrador/evaluacion/preguntas_x_evaluacion/${parametros}`, {  }).pipe(
        tap((response) => {
          //console.log(response)
        }),
        catchError(error => {
          console.error('Error al lotes por vencer:', error);
          return throwError(error);
        })
      );
  }

  guardar_actualizar_persona(parametros?:any): Observable<any> {


    return this.http.post<any>(`${this.baseUrl}/api/administrador/evaluacion/guardar_actualizar_persona`,{parametros}, {  }).pipe(
      tap((persona) => {
      }),
      catchError(error => {
        console.error('Error al listar persona:', error);
        return throwError(error);
      })
    );


    return this.http.get<any>(`${this.baseUrl}/api/administrador/evaluacion/preguntas_x_evaluacion/${parametros}`, {  }).pipe(
      tap((response) => {
        //console.log(response)
      }),
      catchError(error => {
        console.error('Error al lotes por vencer:', error);
        return throwError(error);
      })
    );

  }


  buscar_x_documento(tipo_documento:number,numero_documento:string): Observable<any> {


      return this.http.get<any>(`${this.baseUrl}/api/usuario/buscar_x_documento/${tipo_documento}/${numero_documento}`, {  }).pipe(
        tap((response) => {
          //console.log(response,"responseresponseresponse");
        }),
        catchError(error => {
          console.error('Error al buscar usuario:', error);
          return throwError(error);
        })
      );

  }



  listarDocIdentidad(): Observable<any> {


      return this.http.get<any>(`${this.baseUrl}/api/administrador/maestra/maestra_categoria/lista_documento_identidad_combo`, {  }).pipe(
        tap((usuario) => {
        }),
        catchError(error => {
          console.error('Error al listar usuario:', error);
          return throwError(error);
        })
      );

  }

  enviar_respuestas(parametros?:any): Observable<any> {


    return this.http.post<any>(`${this.baseUrl}/api/administrador/evaluacion/resolucion/enviar_prespuesta`, { parametros },{}).pipe(
      tap((usuario) => {
      }),
      catchError(error => {
        console.error('Error al listar usuario:', error);
        return throwError(error);
      })
    );

}
/*
  eliminar(modulo:Modulo): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/modulo/eliminar/${modulo.id_modulo}`, { headers }).pipe(
        tap((response) => {
          this._snackBar.open(`REGISTRO ELIMINADO CORRECTAMENTE`, 'ok', {
            horizontalPosition: 'start',
            verticalPosition: 'bottom',
            duration: 10 * 1000,
          });
        }),
        catchError(error => {
          console.error('Error al listar supermódulos:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }
*/
}
