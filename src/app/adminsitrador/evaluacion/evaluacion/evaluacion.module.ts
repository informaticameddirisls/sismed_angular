import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';
import { EvaluacionRoutingModule } from './evaluacion-routing.module';



@NgModule({ declarations: [], imports: [CommonModule,
        EvaluacionRoutingModule,
        MaterialModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class EvaluacionModule { }
