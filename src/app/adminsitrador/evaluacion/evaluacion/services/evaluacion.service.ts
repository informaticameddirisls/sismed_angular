import { Injectable } from '@angular/core';
import { environments } from '../../../../../environments/environments';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, tap, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EvaluacionService {


  private baseUrl = environments.baseUrl;
  private token = localStorage.getItem('token');
   private headers = new HttpHeaders;

  constructor(private http: HttpClient) {
    if (this.token) {
    // Encabezados de solicitud HTTP con el token
       this.headers = new HttpHeaders({
        'Authorization': `Bearer ${this.token}`
       });
     }
  }

  listar(parametros?:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/administrador/evaluacion/evaluacion/listar`, { headers }).pipe(
        tap((supermodulo) => {
        }),
        catchError(error => {
          console.error('Error al listar evaluaciones:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }

  }

  combo_estado(parametros?:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/administrador/evaluacion/evaluacion/combo_estado`, { headers }).pipe(
        tap((supermodulo) => {
        }),
        catchError(error => {
          console.error('Error al listar estado combo evaluaciones:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }

  }

  combo_tipo(parametros?:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/administrador/evaluacion/evaluacion/combo_tipo`, { headers }).pipe(
        tap((supermodulo) => {
        }),
        catchError(error => {
          console.error('Error al listar combo tipo evaluacion:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }

  }

  combo_tipo_pregunta(): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/administrador/evaluacion/evaluacion/combo_tipo_pregunta`, { headers }).pipe(
        tap((supermodulo) => {
        }),
        catchError(error => {
          console.error('Error al listar combo tipo pregunta:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }

  }

  crear_evaluacion(evaluacion:any): Observable<any> {

    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/administrador/evaluacion/evaluacion/crear_evaluacion`, evaluacion , { headers }).pipe(
        tap((response) => {
          if (response.success) {

          }

        }),
        catchError(error => {
          console.error('Error al crear evaluacion:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }


  buscar_evaluacion(parametros?:any): Observable<any> {
    var headers = this.headers;

    return this.http.get<any>(`${this.baseUrl}/api/administrador/evaluacion/buscar_evaluacion/${parametros}`, { headers }).pipe(
      tap((response) => {
        //console.log(response)
      }),
      catchError(error => {
        console.error('Error al lotes por vencer:', error);
        return throwError(error);
      })
    );

}


  preguntas_x_evaluacion(parametros?:any): Observable<any> {


      return this.http.get<any>(`${this.baseUrl}/api/administrador/evaluacion/preguntas_x_evaluacion/${parametros}`, {  }).pipe(
        tap((response) => {
          //console.log(response)
        }),
        catchError(error => {
          console.error('Error al lotes por vencer:', error);
          return throwError(error);
        })
      );
  }

  guardar_actualizar_persona(parametros?:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/administrador/evaluacion/guardar_actualizar_persona`,{parametros}, {  }).pipe(
        tap((persona) => {
        }),
        catchError(error => {
          console.error('Error al listar persona:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  guardar_pregunta_respuesta(parametros?:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/administrador/evaluacion/evaluacion/guardar_pregunta_respuesta`,{parametros}, { headers  }).pipe(
        tap((persona) => {
        }),
        catchError(error => {
          console.error('Error al listar persona:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  buscar_respuestas(id_pregunta?:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/administrador/evaluacion/evaluacion/preguntas_abiertas_x_persona/${id_pregunta}`, { headers }).pipe(
        tap((persona) => {
        }),
        catchError(error => {
          console.error('Error al listar persona:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  guardarPreguntasAbiertas(id_evaluacion:any,preguntas?:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.post<any>(`${this.baseUrl}/api/administrador/evaluacion/evaluacion/guardar_preguntas_abiertas/${id_evaluacion}`,{preguntas}, { headers }).pipe(
        tap((persona) => {
        }),
        catchError(error => {
          console.error('Error al listar persona:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  calcularResultadosEvaluacion(id_evaluacion:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/administrador/evaluacion/evaluacion/calcular_resultados_evaluacion/${id_evaluacion}`, { headers }).pipe(
        tap((persona) => {
        }),
        catchError(error => {
          console.error('Error al listar persona:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  resultadosEvaluacion(id_evaluacion:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/administrador/evaluacion/evaluacion/resultados_evaluacion/${id_evaluacion}`, { headers }).pipe(
        tap((persona) => {
        }),
        catchError(error => {
          console.error('Error al listar persona:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  iniciar_evaluacion(id_evaluacion:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/administrador/evaluacion/evaluacion/iniciar_evaluacion/${id_evaluacion}`, { headers }).pipe(
        tap((persona) => {
        }),
        catchError(error => {
          console.error('Error al listar persona:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  cerrado_evaluacion(id_evaluacion:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/administrador/evaluacion/evaluacion/cerrado_evaluacion/${id_evaluacion}`, { headers }).pipe(
        tap((persona) => {
        }),
        catchError(error => {
          console.error('Error al listar persona:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

  finalizado_evaluacion(id_evaluacion:any): Observable<any> {
    var headers = this.headers;
    if (this.token) {
      return this.http.get<any>(`${this.baseUrl}/api/administrador/evaluacion/evaluacion/finalizado_evaluacion/${id_evaluacion}`, { headers }).pipe(
        tap((persona) => {
        }),
        catchError(error => {
          console.error('Error al listar persona:', error);
          return throwError(error);
        })
      );
    } else {
      console.error('No se encontró un token en localStorage.');
      return throwError('No se encontró un token en localStorage.');
    }
  }

}
