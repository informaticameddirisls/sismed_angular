import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule, DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatAccordion, MatExpansionModule} from '@angular/material/expansion';

import { SelectionModel } from '@angular/cdk/collections';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormControl, FormsModule, Validators } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { EvaluacionService } from '../../services/evaluacion.service';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { ToastModule } from 'primeng/toast';
import {
  MatSnackBar,
  MatSnackBarAction,
  MatSnackBarActions,
  MatSnackBarLabel,
  MatSnackBarRef,
} from '@angular/material/snack-bar';

@Component({
    selector: 'adm-eval-eval-tabla-preguntas',
    templateUrl: './tabla-preguntas.component.html',
    styleUrl: './tabla-preguntas.component.css',
    imports: [CommonModule, MatIconModule, MatPaginatorModule, MatTableModule, MatButtonModule, MatCheckboxModule, MatRadioModule,
        MatExpansionModule, MatAutocompleteModule, MatFormFieldModule, MatSelectModule, FormsModule, MatCardModule, MatInputModule, MatMenuModule,
        MatListModule, MatDividerModule, ToastModule]
})
export class TablaPreguntasComponent implements OnInit, AfterViewInit {


  panelOpenState = false;
  ver_evaluacion:boolean = true;
  disableSelect = new FormControl(false);

  dataSource!: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);

  permisoTotal!: any;
  selectedRows: any[] = [];
  selectAllChecked = false;


  selectedTipoPregunta: any = null;






  pregunta_alternativa: any = null;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  @Input() id_evaluacion!: number;
  @Output() preguntasModificadas: EventEmitter<any[]> = new EventEmitter<any[]>();

  constructor(
    private evaluacionService: EvaluacionService,
    private dialog: MatDialog,
    private cd: ChangeDetectorRef,
    private _snackBar: MatSnackBar
  )
  {
    this.ListarTipoPregunta();
  }
  ngOnInit(): void {

    this.listarPreguntasAlternativas(this.id_evaluacion);
    console.log(this.id_evaluacion);
  }

  ngAfterViewInit(): void {
    this.cd.detectChanges();
  }
  guardarPreguntaAlternativa() {
    // Preparar los parámetros
    let parametros = { preguntas: this.pregunta_alternativa, cabeceras: this.id_evaluacion };

    // Realizar la solicitud al servicio
    this.evaluacionService.guardar_pregunta_respuesta(parametros).subscribe(
      (response) => {
        // Verificar si la respuesta tiene éxito
        if (response) {
          // Mostrar el mensaje de éxito en el snackbar
          const snackBarRef = this._snackBar.open(response.mensaje, 'ok', {
            horizontalPosition: 'start',
            verticalPosition: 'bottom',
            duration: 10 * 1000,
            panelClass: ['success-snackbar']
          });
        } else {
          // Si no se recibe éxito, muestra un mensaje de error en el snackbar
          const snackBarRef = this._snackBar.open('Hubo un problema al guardar la pregunta.', 'ok', {
            horizontalPosition: 'start',
            verticalPosition: 'bottom',
            duration: 10 * 1000,
            panelClass: ['error-snackbar']
          });
        }
        // Imprimir la respuesta en consola para depuración
        console.log(response, "guardarPreguntaAlternativa");
      },
      (error) => {
        // En caso de error en la solicitud, mostrar un mensaje de error en el snackbar
        const snackBarRef = this._snackBar.open('Error al conectar con el servidor.', 'ok', {
          horizontalPosition: 'start',
          verticalPosition: 'bottom',
          duration: 10 * 1000,
          panelClass: ['error-snackbar']
        });
        // Imprimir el error en consola para depuración
        console.error(error);
      }
    );

    // Imprimir los parámetros para verificar que se están enviando correctamente
    console.log(parametros);
  }


  agregar_alternativa(id_pregunta: any): void {
    // Buscamos la pregunta correspondiente
    let pregunta = this.pregunta_alternativa.find((pregunta: any) => pregunta.id_pregunta === id_pregunta);

    if (pregunta) {
      // Asegurarnos de que alternativas esté inicializado
      if (!pregunta.alternativas) {
        pregunta.alternativas = [];
      }

      // Agregamos una nueva alternativa
      pregunta.alternativas.push({
        id_alternativa: Date.now(),  // Usamos un ID único para la alternativa
        alternativa: '',  // El campo de la alternativa
        es_correcta: false  // Estado de la alternativa (por defecto, incorrecta)
      });

      console.log('Alternativa agregada:', pregunta.alternativas);
    }
  }



  trackAlternativa(index: number, alternativa: any): number {
    return alternativa.id_alternativa;  // Usamos el ID único de cada alternativa
  }


  agregar_pregunta(): void {
    console.log(this.selectedTipoPregunta,"this.selectedTipoPregunta");
    let tipo_pregunta = this.selectedTipoPregunta[0];  // Seleccionamos el primer tipo de pregunta
    // Inicializa pregunta_alternativa si no ha sido inicializado previamente
    // if (!this.pregunta_alternativa) {
    //   this.pregunta_alternativa = [];
    // }

    this.pregunta_alternativa.push({
      id_pregunta: Date.now(),  // Generamos un ID único para la pregunta
      id_tipo_pregunta: tipo_pregunta.id,
      pregunta: '',  // Aquí se debe ingresar la pregunta en el formulario
      tipo_pregunta: tipo_pregunta,
      alternativas: []  // Inicializamos un array vacío para las alternativas
    });

  }

  eliminar_pregunta(id_pregunta:any){
    const index = this.pregunta_alternativa.findIndex((pregunta: any) => pregunta.id_pregunta === id_pregunta);

    if (index !== -1) {
      // Elimina la alternativa en el índice encontrado
      this.pregunta_alternativa.splice(index, 1);
      console.log('Pregunta eliminada', this.pregunta_alternativa);
    } else {
      console.log('Pregunta no encontrada');
    }
  }

  onTipoPreguntaChange(event: any, pregunta: any): void {
    // Obtener el valor seleccionado
    console.log(pregunta,"pregunta");
    console.log(event,"event");
    console.log(this.selectedTipoPregunta,"selectedTipoPreguntaselectedTipoPreguntaselectedTipoPregunta");
    const tipo_pregunta = this.selectedTipoPregunta.find((tipoPregunta: any) => tipoPregunta.id === event.value);
    console.log(pregunta,"pregunta");
    console.log(event,"event");
    console.log(this.selectedTipoPregunta,"this.selectedTipoPregunta");
    if (tipo_pregunta) {
      const tipoPreguntaCopia = { ...tipo_pregunta };  // Crea una copia superficial del objeto

      // Asigna la copia de tipo_pregunta a pregunta.tipo_pregunta
      pregunta.tipo_pregunta = tipoPreguntaCopia;
      pregunta.id_tipo_pregunta = tipoPreguntaCopia.id;
      pregunta.alternativas = [{
        id_alternativa: Date.now(),  // Usamos un ID único para la alternativa
        alternativa: '',  // El campo de la alternativa
        es_correcta: null  // Estado de la alternativa (por defecto, incorrecta)
      }];
    }

  }

  listarPreguntasAlternativas(id_evaluacion:number): void {
    this.evaluacionService.preguntas_x_evaluacion(id_evaluacion).subscribe(
       (response) => {

          if (response.success) {
            this.pregunta_alternativa = response.data;
            console.log(response,'id_evaluacionid_evaluacion');

          } else {

           console.error('Error al listar supermódulos:', response);

         }
       },
       (error) => {
         console.error('Error al listar supermódulos:', error);
       }
     );
  }


  eliminarAlternativa(pregunta: any, id_alternativa: number): void {
    const index = pregunta.alternativas.findIndex((alternativa: any) => alternativa.id_alternativa === id_alternativa);

    if (index !== -1) {
      // Elimina la alternativa en el índice encontrado
      pregunta.alternativas.splice(index, 1);
      console.log('Alternativa eliminada', pregunta.alternativas);
    } else {
      console.log('Alternativa no encontrada');
    }
  }


  ListarTipoPregunta(){
    this.evaluacionService.combo_tipo_pregunta().subscribe(
      (response) => {

        if (response.success) {
          this.selectedTipoPregunta = response.data;
          console.log(this.selectedTipoPregunta);
         } else {

          console.error('Error al listar supermódulos:', response);

        }
      },
      (error) => {
        console.error('Error al listar supermódulos:', error);
      }
    );
  }

}
