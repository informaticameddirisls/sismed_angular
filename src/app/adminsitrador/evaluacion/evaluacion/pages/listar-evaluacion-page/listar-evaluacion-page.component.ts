import { CommonModule, DatePipe, JsonPipe } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, inject, ViewChild } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { DataService } from '../../../../../shared/services/data.service';
import { ExcelService } from '../../../../../shared/services/excel.service';
import { MatDialog } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { provideMomentDateAdapter } from '@angular/material-moment-adapter';
import 'moment/locale/es';
import {  EvaluacionService } from '../../services/evaluacion.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { ComboDocumentoIdentidadService } from '../../../../../shared/services/combo/combo-documento-identidad.service';
import { map, Observable } from 'rxjs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { FormValidationService } from '../../validators/form-validation.service';
import { UsuarioService } from '../../../../../usuarios/services/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {ToastModule} from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { RippleModule } from 'primeng/ripple';
import { QRCodeComponent } from 'angularx-qrcode';


@Component({
    selector: 'adm-eval-evaluacion-listar-evaluacion-page',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es' },
        provideMomentDateAdapter(),
        MessageService
    ],
    templateUrl: './listar-evaluacion-page.component.html',
    styleUrl: './listar-evaluacion-page.component.css',
    imports: [
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatExpansionModule,
        MatButtonModule,
        MatMenuModule,
        CommonModule, MatCardModule, MatDatepickerModule, FormsModule, ReactiveFormsModule, MatCheckboxModule, MatRadioModule,
        MatFormFieldModule, MatSelectModule, MatInputModule, ToastModule, RippleModule,
    ]
})
export class ListarEvaluacionPageComponent {


  dataSource!: MatTableDataSource<any>;
  supermoduloColumnas: string[] = ['id_evaluacion', 'nombre', 'descripcion', 'estado','acciones'];

  registroAEliminar: any | null = null;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  titulo: string = 'EVALUACION';

  baseRoute!: string;
  routeParams: any;
  section!: string;
  allRoutes: any[] = [];

  constructor(
    private evaluacionService: EvaluacionService,
    private dataService: DataService,
    private dialog: MatDialog,
    private router: Router,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {

    this.listar_evaluaciones();
    this.dataService.setPageTitle(this.titulo);
  }

  ngAfterViewInit() {
  }

  listar_evaluaciones(): void {
    this.evaluacionService.listar().subscribe(
      (response) => {
        console.log(response);
        if (response.success) {

          this.dataSource = new MatTableDataSource(response.data);
          this.dataSource.paginator = this.paginator;
          return
        } else {
          console.error('Error al listar supermódulos:', response);
        }
      },
      (error) => {
        console.error('Error al listar supermódulos:', error);
      }
    );
  }

  nuevaEvaluacion():void {
      this.router.navigate(['sismed', 'administrador', 'evaluacion', 'evaluacion', 'agregar']);
  }


  editar_evaluacion(id_evaluacion:any){
    console.log(id_evaluacion);
    this.router.navigate(['sismed', 'administrador', 'evaluacion', 'evaluacion', 'editar',id_evaluacion]);
  }
  corregir_evaluacion(id_evaluacion:any){
    console.log(id_evaluacion);
    this.router.navigate(['sismed', 'administrador', 'evaluacion', 'evaluacion', 'corregir',id_evaluacion]);
  }
  resultado_evaluacion(id_evaluacion:any){
    console.log(id_evaluacion);
    this.router.navigate(['sismed', 'administrador', 'evaluacion', 'evaluacion', 'resultados',id_evaluacion]);
  }

  ver_qr(id_evaluacion:any){
    console.log(id_evaluacion);
    this.router.navigate(['sismed', 'administrador', 'evaluacion', 'evaluacion', 'qr',id_evaluacion]);
  }

  cerrar_evaluacion(id_evaluacion:any){
    this.evaluacionService.cerrado_evaluacion(id_evaluacion).subscribe(
      (response)=>{
       console.log(response,"guardar_evaluacion");
       if (response.success) {

        const snackBarRef = this._snackBar.open('EVALUACION CERRADA CORRECTAMENTE', 'ok', {
          horizontalPosition: 'start',
          verticalPosition: 'bottom',
          duration: 10 * 500,
          panelClass: ['success-snackbar']
        });
        this.listar_evaluaciones();
        }
      }
    )
  }


  ruta_evaluacion(id_evaluacion:any){
    const ruta = ['sismed', 'administrador', 'evaluacion', 'resolucion', 'eval', id_evaluacion];

    window.open('/sismed/administrador/evaluacion/resolucion/eval/'+id_evaluacion);
    // Navegar a la ruta

  }

}
