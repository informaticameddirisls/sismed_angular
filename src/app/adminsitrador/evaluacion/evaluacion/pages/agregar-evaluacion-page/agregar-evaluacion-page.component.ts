import { CommonModule, DatePipe, JsonPipe } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, inject, ViewChild } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { DataService } from '../../../../../shared/services/data.service';
import { ExcelService } from '../../../../../shared/services/excel.service';
import { MatDialog } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { provideMomentDateAdapter } from '@angular/material-moment-adapter';
import 'moment/locale/es';
import {  EvaluacionService } from '../../services/evaluacion.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { ComboDocumentoIdentidadService } from '../../../../../shared/services/combo/combo-documento-identidad.service';
import { map, Observable } from 'rxjs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { FormValidationService } from '../../validators/form-validation.service';
import { UsuarioService } from '../../../../../usuarios/services/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {ToastModule} from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { RippleModule } from 'primeng/ripple';


@Component({
    selector: 'adm-eval-evaluacion-agregar-evaluacion-page',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es' },
        provideMomentDateAdapter(),
        MessageService
    ],
    templateUrl: './agregar-evaluacion-page.component.html',
    styleUrl: './agregar-evaluacion-page.component.css',
    imports: [
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatExpansionModule,
        MatButtonModule,
        MatMenuModule,
        CommonModule, MatCardModule, MatDatepickerModule, FormsModule, ReactiveFormsModule, MatCheckboxModule, MatRadioModule,
        MatFormFieldModule, MatSelectModule, MatInputModule, ToastModule, RippleModule
    ]
})
export class AgregarEvaluacionPageComponent {

  titulo: string = 'CREAR EVALUACION';
  formularioEvaluacion: FormGroup;
  array_combo_tipo: any;
  array_combo_estado: any;


  constructor(
    private evaluacionService: EvaluacionService,
    private dataService: DataService,
    private fb: FormBuilder,
    private formValidationService: FormValidationService,
    private dialog: MatDialog,
    private router: Router,
  ) {
      this.formularioEvaluacion = this.fb.group({
        id_evaluacion: [0, Validators.required],
        nombre: ['', Validators.required],
        descripcion: ['', Validators.required],
        id_estado: ['', Validators.required],
        id_tipo_evaluacion: ['',Validators.required],
        fecha_hora_limite: ['',Validators.required],
      });

      this.combo_tipo();
      this.combo_estado();
     }




  ngOnInit(): void {

    this.dataService.setPageTitle(this.titulo);
  }

  ngAfterViewInit() {
  }

  // Método para obtener los controles del formulario
  get f() {
    return this.formularioEvaluacion.controls;
  }

  esCampoValido( field: string ): boolean | null {
    return this.formValidationService.esCampoValido( this.formularioEvaluacion,field);
  }

  mensajeError( field: string ): string | null {
    return this.formValidationService.mensajeError(this.formularioEvaluacion,field);
  }

  mostrarError(field: string): boolean | null {
    if ( !this.formularioEvaluacion.controls[field] ) return false;
    return this.formValidationService.esCampoValido(this.formularioEvaluacion, field);
  }

  combo_tipo(){
       this.evaluacionService.combo_tipo().subscribe(
         (response)=>{
          this.array_combo_tipo = response.data;
         }
       )
  }

  combo_estado(){
    this.evaluacionService.combo_estado().subscribe(
      (response)=>{
        console.log(response);
       this.array_combo_estado = response.data;
      }
    )
}


  regresar() {
    this.router.navigate(['sismed', 'administrador', 'evaluacion', 'evaluacion', 'listar']);
  }


  onSubmit() {
    if (this.formularioEvaluacion.valid) {
      // Procesar el formulario si es válido
      const evaluacion = this.formularioEvaluacion.value;
      console.log(evaluacion);
       this.evaluacionService.crear_evaluacion(evaluacion).subscribe(
         (response)=>{
          console.log(response);
          if (response.success) {
             this.router.navigate(['sismed', 'administrador', 'evaluacion', 'evaluacion', 'editar', response.data]);
           }
         }
       )
      console.log('Datos del formulario:', evaluacion);
      // Llamar al servicio para guardar el usuario
      // this.usuarioService.guardarUsuario(usuario).subscribe(...);
    } else {
      // Marcar todos los campos como tocados para mostrar errores
      this.formularioEvaluacion.markAllAsTouched();
    }
  }


}
