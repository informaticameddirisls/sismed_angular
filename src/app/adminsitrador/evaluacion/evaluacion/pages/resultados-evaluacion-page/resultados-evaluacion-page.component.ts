import { CommonModule, DatePipe, JsonPipe } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, inject, OnInit, ViewChild } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { DataService } from '../../../../../shared/services/data.service';
import { ExcelService } from '../../../../../shared/services/excel.service';
import { MatDialog } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { provideMomentDateAdapter } from '@angular/material-moment-adapter';
import 'moment/locale/es';
import {  EvaluacionService } from '../../services/evaluacion.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { ComboDocumentoIdentidadService } from '../../../../../shared/services/combo/combo-documento-identidad.service';
import { map, Observable } from 'rxjs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { FormValidationService } from '../../validators/form-validation.service';
import { UsuarioService } from '../../../../../usuarios/services/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {ToastModule} from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { RippleModule } from 'primeng/ripple';
import { TablaPreguntasComponent } from '../../components/tabla-preguntas/tabla-preguntas.component';


@Component({
    selector: 'adm-eval-evaluacion-resultados-evaluacion-page',
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es' },
        provideMomentDateAdapter(),
        MessageService
    ],
    templateUrl: './resultados-evaluacion-page.component.html',
    styleUrl: './resultados-evaluacion-page.component.css',
    imports: [
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatExpansionModule,
        MatButtonModule,
        MatMenuModule,
        CommonModule, MatCardModule, MatDatepickerModule, FormsModule, ReactiveFormsModule, MatCheckboxModule, MatRadioModule,
        MatFormFieldModule, MatSelectModule, MatInputModule, ToastModule, RippleModule
    ]
})
export class ResultadosEvaluacionPageComponent implements OnInit {

  titulo: string = 'RESULTADOS EVALUACION - ';

  array_combo_tipo: any;
  array_combo_estado: any;
  id_evaluacion = this.activateRoute.snapshot.params['id'];
  preguntasModificadas: any;

  preguntasAbiertas: any;
  resultadosPersonas: any;

  constructor(
    private evaluacionService: EvaluacionService,
    private dataService: DataService,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private formValidationService: FormValidationService,
    private dialog: MatDialog,
    private activateRoute: ActivatedRoute,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {

     }


  buscar_evaluacion(id_evaluacion: number){
    this.evaluacionService.buscar_evaluacion(id_evaluacion).subscribe(
      (response)=>{
        console.log(response.data.nombre,'responseresponse');
        this.cdr.detectChanges();
      }
    )
  }


  ngOnInit(): void {

    this.dataService.setPageTitle(this.titulo+this.id_evaluacion);
    this.resultadosEvaluacion(this.id_evaluacion);

    this.cdr.detectChanges();

  }

  buscar_respuestas(id_evaluacion:any){
    this.evaluacionService.buscar_respuestas(id_evaluacion).subscribe(
      (response)=>{

       if (response.success) {
          console.log(response.data);
          this.preguntasAbiertas = response.data;
          //this.router.navigate(['sismed', 'administrador', 'evaluacion', 'evaluacion', 'editar', response.data]);
        }
      }
    )
  }

  ngAfterViewInit() {
  }



  regresar() {
    this.router.navigate(['sismed', 'administrador', 'evaluacion', 'evaluacion', 'listar']);
  }



  manejarPreguntasModificadas(preguntasModificadas: any[]): void {



    this.preguntasModificadas = preguntasModificadas;
    this.cdr.detectChanges();
  }

  isCorreccionesCompletas(): boolean {
    if (!Array.isArray(this.preguntasAbiertas)) {
      return false;
    }
    // Verifica si alguna corrección está vacía
    for (const pregunta of this.preguntasAbiertas) {
      for (const respuesta of pregunta.persona_respuestas) {
        if (respuesta.correccion === null || respuesta.correccion === '') {
          return false; // Si alguna corrección no ha sido seleccionada, retorna false
        }
      }
    }
    return true; // Si todas las correcciones han sido seleccionadas, retorna true
  }

  resultadosEvaluacion(id_evaluacion:any) {
    this.evaluacionService.resultadosEvaluacion(id_evaluacion).subscribe(
      (response)=>{
        if (response.success) {
          this.resultadosPersonas = response.data;
          console.log(this.resultadosPersonas);
        }

      }
    )

  }



}
