import { CommonModule, DatePipe, JsonPipe } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, inject, ViewChild } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { DataService } from '../../../../../shared/services/data.service';
import { ExcelService } from '../../../../../shared/services/excel.service';
import { MatDialog } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { provideMomentDateAdapter } from '@angular/material-moment-adapter';
import 'moment/locale/es';
import {  EvaluacionService } from '../../services/evaluacion.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { ComboDocumentoIdentidadService } from '../../../../../shared/services/combo/combo-documento-identidad.service';
import { map, Observable } from 'rxjs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { FormValidationService } from '../../validators/form-validation.service';
import { UsuarioService } from '../../../../../usuarios/services/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {ToastModule} from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { RippleModule } from 'primeng/ripple';
import { TablaPreguntasComponent } from '../../components/tabla-preguntas/tabla-preguntas.component';


@Component({
    selector: 'adm-eval-evaluacion-editar-evaluacion-page',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es' },
        provideMomentDateAdapter(),
        MessageService
    ],
    templateUrl: './editar-evaluacion-page.component.html',
    styleUrl: './editar-evaluacion-page.component.css',
    imports: [
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatExpansionModule,
        MatButtonModule,
        MatMenuModule,
        CommonModule, MatCardModule, MatDatepickerModule, FormsModule, ReactiveFormsModule, MatCheckboxModule, MatRadioModule,
        MatFormFieldModule, MatSelectModule, MatInputModule, ToastModule, RippleModule, TablaPreguntasComponent
    ]
})
export class EditarEvaluacionPageComponent {

  titulo: string = 'EDITAR EVALUACION - ';
  formularioEvaluacion: FormGroup;
  array_combo_tipo: any;
  array_combo_estado: any;
  id_evaluacion = this.activateRoute.snapshot.params['id'];
  preguntasModificadas: any;


  constructor(
    private evaluacionService: EvaluacionService,
    private dataService: DataService,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private formValidationService: FormValidationService,
    private dialog: MatDialog,
    private activateRoute: ActivatedRoute,
    private router: Router,
    private cd: ChangeDetectorRef,
    private _snackBar: MatSnackBar
  ) {
    this.formularioEvaluacion = this.fb.group({
      id_evaluacion: [this.id_evaluacion, Validators.required],
      nombre: ['', Validators.required],
      descripcion: ['', Validators.required],
      id_estado: ['', Validators.required],
      id_tipo_evaluacion: ['',Validators.required],
      fecha_hora_limite: ['',Validators.required],
    });

      this.combo_tipo();
      this.combo_estado();

     }


     buscar_evaluacion(id_evaluacion: number){
      this.evaluacionService.buscar_evaluacion(id_evaluacion).subscribe(
        (response)=>{
          console.log(response.data.nombre,'responseresponse');
          this.formularioEvaluacion = this.fb.group({
            id_evaluacion: [this.id_evaluacion, Validators.required],
            nombre: [response.data.nombre || '', Validators.required],
            descripcion: [response.data.descripcion || '', Validators.required],
            id_estado: [response.data.id_estado || '', Validators.required],
            id_tipo_evaluacion: [response.data.id_tipo_evaluacion || '',Validators.required],
            fecha_hora_limite: [response.data.fecha_hora_limite || '',Validators.required],
          });
          this.cdr.detectChanges();
        }
      )
    }


  ngOnInit(): void {

    this.dataService.setPageTitle(this.titulo+this.id_evaluacion);

    this.buscar_evaluacion(this.id_evaluacion);

  }

  ngAfterViewInit() {
  }

  // Método para obtener los controles del formulario
  get f() {
    return this.formularioEvaluacion.controls;
  }

  esCampoValido( field: string ): boolean | null {
    return this.formValidationService.esCampoValido( this.formularioEvaluacion,field);
  }

  mensajeError( field: string ): string | null {
    return this.formValidationService.mensajeError(this.formularioEvaluacion,field);
  }

  mostrarError(field: string): boolean | null {
    if ( !this.formularioEvaluacion.controls[field] ) return false;
    return this.formValidationService.esCampoValido(this.formularioEvaluacion, field);
  }

  combo_tipo(){
       this.evaluacionService.combo_tipo().subscribe(
         (response)=>{
          this.array_combo_tipo = response.data;
         }
       )
  }

  combo_estado(){
    this.evaluacionService.combo_estado().subscribe(
      (response)=>{
        console.log(response);
       this.array_combo_estado = response.data;
      }
    )
}


  regresar() {
    this.router.navigate(['sismed', 'administrador', 'evaluacion', 'evaluacion', 'listar']);
  }



  manejarPreguntasModificadas(preguntasModificadas: any[]): void {



    this.preguntasModificadas = preguntasModificadas;
    this.cd.detectChanges();
  }

  guardarPreguntas(preguntasModificadas:any){

    console.log(preguntasModificadas,"preguntasModificadaspreguntasModificadaspreguntasModificadas");

    this.evaluacionService.guardar_pregunta_respuesta(preguntasModificadas).subscribe(
      (response)=>{
       console.log(response);
       if (response.success) {
          //this.router.navigate(['sismed', 'administrador', 'evaluacion', 'evaluacion', 'editar', response.data]);
        }
      }
    )

  }

  guardar_evaluacion() {
    if (this.formularioEvaluacion.valid) {
      // Procesar el formulario si es válido
      const evaluacion = this.formularioEvaluacion.value;
      console.log(evaluacion);
       this.evaluacionService.crear_evaluacion(evaluacion).subscribe(
         (response)=>{
          console.log(response,"guardar_evaluacion");
          if (response.success) {
             this.router.navigate(['sismed', 'administrador', 'evaluacion', 'evaluacion', 'editar', response.data]);
           }
         }
       )


      console.log('Datos del formulario:', evaluacion);
      // Llamar al servicio para guardar el usuario
      // this.usuarioService.guardarUsuario(usuario).subscribe(...);
    } else {
      // Marcar todos los campos como tocados para mostrar errores
      this.formularioEvaluacion.markAllAsTouched();
    }
  }


  iniciar_evaluacion(){
    var id_evaluacion = this.id_evaluacion;
    this.evaluacionService.iniciar_evaluacion(id_evaluacion).subscribe(
      (response)=>{
       console.log(response,"guardar_evaluacion");
       if (response.success) {

        const snackBarRef = this._snackBar.open('ESTADO ACTUALIZADO CORRECTAMENTE', 'ok', {
          horizontalPosition: 'start',
          verticalPosition: 'bottom',
          duration: 10 * 500,
          panelClass: ['success-snackbar']
        });

        // Ejecutar una acción cuando el SnackBar se cierre
        snackBarRef.afterDismissed().subscribe(() => {
          this.router.navigate(['sismed', 'administrador', 'evaluacion', 'evaluacion', 'listar']);
        });




        }
      }
    )

  }


}
