import { CommonModule } from '@angular/common';
import {  ChangeDetectorRef, Component,  OnInit } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import {  MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import {  MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { DataService } from '../../../../../shared/services/data.service';
import { MatDialog } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { provideMomentDateAdapter } from '@angular/material-moment-adapter';
import 'moment/locale/es';
import {  EvaluacionService } from '../../services/evaluacion.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { FormValidationService } from '../../validators/form-validation.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {ToastModule} from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { RippleModule } from 'primeng/ripple';
import { TablaPreguntasComponent } from '../../components/tabla-preguntas/tabla-preguntas.component';
import { QRCodeComponent } from 'angularx-qrcode';


@Component({
    selector: 'adm-eval-evaluacion-qr-evaluacion-page',
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es' },
        provideMomentDateAdapter(),
        MessageService
    ],
    templateUrl: './qr-evaluacion-page.component.html',
    styleUrl: './qr-evaluacion-page.component.css',
    imports: [
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatExpansionModule,
        MatButtonModule,
        MatMenuModule,
        CommonModule, MatCardModule, MatDatepickerModule, FormsModule, ReactiveFormsModule, MatCheckboxModule, MatRadioModule,
        MatFormFieldModule, MatSelectModule, MatInputModule, ToastModule, RippleModule, QRCodeComponent
    ]
})
export class QrEvaluacionPageComponent implements OnInit {

  titulo: string = 'ENLACE PARA RESOLVER LA EVALUACION - ';

  array_combo_tipo: any;
  array_combo_estado: any;
  id_evaluacion = this.activateRoute.snapshot.params['id'];
  preguntasModificadas: any;

  preguntasAbiertas: any;
  resultadosPersonas: any;
  rutaQr: string = '';
  constructor(
    private evaluacionService: EvaluacionService,
    private dataService: DataService,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private formValidationService: FormValidationService,
    private dialog: MatDialog,
    private activateRoute: ActivatedRoute,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {

     }





  ngOnInit(): void {

    this.dataService.setPageTitle(this.titulo+this.id_evaluacion);

    if (!this.rutaQr) {
      const baseUrl = window.location.origin;
      const route = ['sismed', 'administrador', 'evaluacion', 'resolucion', 'eval', this.id_evaluacion];
      this.rutaQr = `${baseUrl}/${this.router.createUrlTree(route).toString()}`;

    }
    this.cdr.detectChanges();

  }



  ngAfterViewInit() {
  }



  regresar() {
    this.router.navigate(['sismed', 'administrador', 'evaluacion', 'evaluacion', 'listar']);
  }






}
