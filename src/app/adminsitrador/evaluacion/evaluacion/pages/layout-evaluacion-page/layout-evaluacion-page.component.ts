import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'adm-eval-eval-layout-evaluacion-page',
    imports: [RouterOutlet],
    templateUrl: './layout-evaluacion-page.component.html',
    styleUrl: './layout-evaluacion-page.component.css'
})
export class LayoutEvaluacionPageComponent {

}
