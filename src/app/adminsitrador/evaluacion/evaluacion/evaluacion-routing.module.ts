import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../auth/guards/auth.guard';

import { ListarEvaluacionPageComponent } from './pages/listar-evaluacion-page/listar-evaluacion-page.component';
import { AgregarEvaluacionPageComponent } from './pages/agregar-evaluacion-page/agregar-evaluacion-page.component';
import { EditarEvaluacionPageComponent } from './pages/editar-evaluacion-page/editar-evaluacion-page.component';
import { CorregirEvaluacionPageComponent } from './pages/corregir-evaluacion-page/corregir-evaluacion-page.component';
import { ResultadosEvaluacionPageComponent } from './pages/resultados-evaluacion-page/resultados-evaluacion-page.component';
import { QrEvaluacionPageComponent } from './pages/qr-evaluacion-page/qr-evaluacion-page.component';

const routes: Routes = [
  {

    path: 'evaluacion',
    children:
      [
        {
          path: 'listar',
          component: ListarEvaluacionPageComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'agregar',
          component: AgregarEvaluacionPageComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'editar/:id',
          component: EditarEvaluacionPageComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'corregir/:id',
          component: CorregirEvaluacionPageComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'resultados/:id',
          component: ResultadosEvaluacionPageComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'qr/:id',
          component: QrEvaluacionPageComponent,
          canActivate: [AuthGuard]
        },
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvaluacionRoutingModule { }
