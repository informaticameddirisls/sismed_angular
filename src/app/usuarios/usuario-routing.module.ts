import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth/guards/auth.guard';
import { ListarUsuarioPageComponent } from './pages/listar-usuario-page/listar-usuario-page.component';
import { AgregarUsuarioPageComponent } from './pages/agregar-usuario-page/agregar-usuario-page.component';
import { EditarUsuarioPageComponent } from './pages/editar-usuario-page/editar-usuario-page.component';

const routes: Routes = [
  {

    path: 'usuario',
    children:
      [
        {
          path: 'listar',
          component: ListarUsuarioPageComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'agregar',
          component: AgregarUsuarioPageComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'editar/:id',
          component: EditarUsuarioPageComponent,
          canActivate: [AuthGuard]
        }
      ]
  },
  //{ path: '**', redirectTo: 'listar', pathMatch: 'full' },
  // {
  //   path: '',
  //   //component: EmpresaLayoutPageComponent,
  //   children: [
  //     { path: 'listar', component: ListarPageComponent },
  //     {
  //       path: '**',
  //       redirectTo: 'listar'
  //     }
  //   ]
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRoutingModule { }
