import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { DataService } from '../../../shared/services/data.service';
import { ComboSupermoduloComponent } from "../../../shared/components/combo/combo-supermodulo/combo-supermodulo.component";
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { CdkDragDrop, DragDropModule, moveItemInArray } from '@angular/cdk/drag-drop';
import { Usuario } from '../../interfaces/Usuario.interface';
import { UsuarioService } from '../../services/usuario.service';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';

@Component({
    selector: 'app-listar-usuario-page',
    templateUrl: './listar-usuario-page.component.html',
    styleUrl: './listar-usuario-page.component.css',
    imports: [CommonModule, MatIconModule, MatPaginatorModule, MatTableModule, MatButtonModule]
})
export class ListarUsuarioPageComponent implements OnInit {

  //titulo para la pagina
  titulo: string = 'USUARIO';
  @Output() tituloEnviado = new EventEmitter<string>();

  //creacion de la tabla
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  dataSource: MatTableDataSource<any>;
  usuarioColumnas: string[] = ['id_usuario', 'nombre', 'correo_electronico', 'estado', 'acciones'];

  constructor(
    private dataService: DataService,
    private usuarioService: UsuarioService,
    private router: Router,

  ) {
    this.dataSource = new MatTableDataSource<any>([]);
  }


  ngOnInit(): void {

    this.listarUsuario();
    this.dataService.setPageTitle(this.titulo);

  }

  listarUsuario(): void {
    this.usuarioService.listar().subscribe(
      (response) => {
        console.log(response)
        if (response.success) {
          this.dataSource = new MatTableDataSource(response.data);
          this.dataSource.paginator = this.paginator;
        } else {
          console.error('Error al listar usuario:', response);
        }
      },
      (error) => {
        console.error('Error al listar usuario:', error);
      }
    );
  }

  agregarUsuario() {
    this.router.navigate(['sismed', 'administrador', 'maestra', 'usuario', 'agregar']);
  }

  editarUsuario(element:any) {
    this.router.navigate(['sismed', 'administrador', 'maestra', 'usuario', 'editar', element.id_usuario]);
  }

}
