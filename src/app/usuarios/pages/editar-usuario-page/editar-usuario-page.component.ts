import { Component, EventEmitter, Output } from '@angular/core';
import { DataService } from '../../../shared/services/data.service';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormValidationService } from '../../validators/form-validation.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '../../services/usuario.service';
import { Usuario } from '../../interfaces/Usuario.interface';
import { TablaPermisosComponent } from "../../components/tabla-permisos/tabla-permisos.component";
import {MatTabsModule} from '@angular/material/tabs';
import { UsuarioFormularioEditarComponent } from "../../components/usuario-formulario-editar/usuario-formulario-editar.component";
@Component({
    selector: 'app-editar-usuario-page',
    templateUrl: './editar-usuario-page.component.html',
    styleUrl: './editar-usuario-page.component.css',
    imports: [CommonModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule, TablaPermisosComponent, MatTabsModule, UsuarioFormularioEditarComponent]
})
export class EditarUsuarioPageComponent {

  formularioUsuario!: FormGroup;
  id_usuario =this.activateRoute.snapshot.params['id'];
  //titulo para la pagina
  titulo: string = 'EDITAR USUARIO';
  @Output() tituloEnviado = new EventEmitter<string>();

  constructor(
    private dataService: DataService,
    private fb: FormBuilder,
    private formValidationService: FormValidationService,
    private router: Router,
    private usuarioService: UsuarioService,
    private activateRoute: ActivatedRoute,

  ) {
    this.formularioUsuario = this.fb.group({
      id_usuario: [0, Validators.required],
      nombres: ['', Validators.required],
      ape_paterno: ['', Validators.required],
      ape_materno: ['', Validators.required],
      id_tipo_documento: [''],
      numero_documento: [''],
      sexo: [''],
      fecha_nac: [''],
      correo_electronico: ['', [Validators.required, Validators.email]],
      celular: [''],
      password: ['', Validators.required],
    });



  }

  buscarUsuario(idUsuario: number){
    this.usuarioService.buscar(idUsuario).subscribe(
      (response)=>{
        //console.log(response,"responsesssss");
        this.formularioUsuario = this.fb.group({
          id_usuario: [response.data.id_usuario, Validators.required],
          nombre: [response.data.nombre, Validators.required],
          correo_electronico: [response.data.correo_electronico, [Validators.required, Validators.email]]
        });
      }
    )
  }


  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);
  }

  onSubmit() {
    if (this.formularioUsuario.valid) {
      // Procesar el formulario si es válido
      const usuario: Usuario = this.formularioUsuario.value;

      this.usuarioService.editar(usuario).subscribe(
        (response)=>{
          if (response.success) {
            //this.router.navigate(['sismed', 'administrador', 'maestra', 'usuario', 'editar', response.data["id_usuario"]]);
          }
        }
      )
      // Llamar al servicio para guardar el usuario
      // this.usuarioService.guardarUsuario(usuario).subscribe(...);
    } else {
      // Marcar todos los campos como tocados para mostrar errores
      this.formularioUsuario.markAllAsTouched();
    }
  }


  esCampoValido(field: string): boolean | null {
    return this.formValidationService.esCampoValido(this.formularioUsuario, field);
  }

  mensajeError(field: string): string | null {
    return this.formValidationService.mensajeError(this.formularioUsuario, field);
  }

  // Método para validar y mostrar el mensaje de error
  mostrarError(field: string): boolean | null {
    if (!this.formularioUsuario.controls[field]) return false;
    return this.formValidationService.esCampoValido(this.formularioUsuario, field);
  }


  regresar() {
    this.router.navigate(['sismed', 'administrador', 'maestra', 'usuario', 'listar']);
  }
}
