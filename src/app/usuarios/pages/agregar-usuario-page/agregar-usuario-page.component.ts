import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DataService } from '../../../shared/services/data.service';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Usuario } from '../../interfaces/Usuario.interface';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormValidationService } from '../../validators/form-validation.service';
import { Router } from '@angular/router';
import { UsuarioService } from '../../services/usuario.service';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { map, Observable } from 'rxjs';
import { ComboDocumentoIdentidadService } from '../../../shared/services/combo/combo-documento-identidad.service';

@Component({
    selector: 'app-agregar-usuario-page',
    imports: [CommonModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatCheckboxModule],
    templateUrl: './agregar-usuario-page.component.html',
    styleUrl: './agregar-usuario-page.component.css'
})
export class AgregarUsuarioPageComponent implements OnInit {

  formularioUsuario: FormGroup;

  comboDocumentoIdentidad$: Observable<any[]>;
  //titulo para la pagina
  titulo: string = 'CREAR USUARIO';
  @Output() tituloEnviado = new EventEmitter<string>();

  constructor(
    private dataService: DataService,
    private fb: FormBuilder,
    private formValidationService: FormValidationService,
    private router: Router,
    private usuarioService: UsuarioService,
    private comboDocumentoIdentidadService: ComboDocumentoIdentidadService
  ){
    this.formularioUsuario = this.fb.group({
      id_usuario: [0, Validators.required],
      nombres: ['', Validators.required],
      ape_paterno: ['', Validators.required],
      ape_materno: ['', Validators.required],
      id_tipo_documento: [''],
      numero_documento: [''],
      sexo: [''],
      fecha_nac: [''],
      correo_electronico: ['', [Validators.required, Validators.email]],
      direccion: [''],
      celular: [''],
      password: ['', Validators.required],
    });


    this.comboDocumentoIdentidad$ = this.comboDocumentoIdentidadService.listarDocIdentidad().pipe(
      map(response => response.resultado) // Ajusta según la estructura de tu respuesta
    );

  }


  ngOnInit(): void {
    this.dataService.setPageTitle(this.titulo);
  }
  // Método para obtener los controles del formulario
  get f() {
    return this.formularioUsuario.controls;
  }

  esCampoValido( field: string ): boolean | null {
    return this.formValidationService.esCampoValido( this.formularioUsuario,field);
  }

  mensajeError( field: string ): string | null {
    return this.formValidationService.mensajeError(this.formularioUsuario,field);
  }

  onSubmit() {
    if (this.formularioUsuario.valid) {
      // Procesar el formulario si es válido
      const usuario = this.formularioUsuario.value;

      this.usuarioService.crear(usuario).subscribe(
        (response)=>{
          if (response.success) {
            this.router.navigate(['sismed', 'administrador', 'maestra', 'usuario', 'editar', response.data["id_usuario"]]);
          }
        }
      )
      console.log('Datos del formulario:', usuario);
      // Llamar al servicio para guardar el usuario
      // this.usuarioService.guardarUsuario(usuario).subscribe(...);
    } else {
      // Marcar todos los campos como tocados para mostrar errores
      this.formularioUsuario.markAllAsTouched();
    }
  }

    // Método para validar y mostrar el mensaje de error
    mostrarError(field: string): boolean | null {
      if ( !this.formularioUsuario.controls[field] ) return false;
      return this.formValidationService.esCampoValido(this.formularioUsuario, field);
    }


    regresar() {
      this.router.navigate(['sismed', 'administrador', 'maestra', 'usuario', 'listar']);
    }



}
