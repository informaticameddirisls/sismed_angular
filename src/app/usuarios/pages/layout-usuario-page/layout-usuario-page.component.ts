import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-layout-usuario-page',
    imports: [RouterOutlet],
    templateUrl: './layout-usuario-page.component.html',
    styleUrl: './layout-usuario-page.component.css'
})
export class LayoutUsuarioPageComponent {

}
