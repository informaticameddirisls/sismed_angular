import { Permisototal } from './../../../permisosTotales/interfaces/permiso-total.interface';
import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { PermisoTotalService } from '../../../permisosTotales/services/permiso-total.service';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { ModalFormPermisoTotalComponent } from '../../../permisosTotales/components/modal-form-permiso-total/modal-form-permiso-total.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatAccordion, MatExpansionModule} from '@angular/material/expansion';

import { SelectionModel } from '@angular/cdk/collections';
import { DialogEliminarComponent } from '../../../shared/components/dialog-eliminar/dialog-eliminar.component';
import { ComboSupermoduloComponent } from "../../../adminsitrador/maestras/supermodulos/components/combo-supermodulo/combo-supermodulo.component";
import { ComboModuloComponent } from "../../../adminsitrador/maestras/modulos/components/combo-modulo/combo-modulo.component";
import { ComboSubmoduloComponent } from "../../../adminsitrador/maestras/submodulos/components/combo-submodulo/combo-submodulo.component";
import { ComboPermisoComponent } from "../../../adminsitrador/maestras/permisos/components/combo-permiso/combo-permiso.component";
import { ComboAlmacenComponent } from "../../../adminsitrador/maestras/almacenes/components/combo-almacen/combo-almacen.component";
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormControl, FormsModule, Validators } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';

@Component({
    selector: 'usuario-tabla-permisos',
    templateUrl: './tabla-permisos.component.html',
    styleUrl: './tabla-permisos.component.css',
    imports: [CommonModule, MatIconModule, MatPaginatorModule, MatTableModule, MatButtonModule, MatCheckboxModule,
        ComboSupermoduloComponent, ComboModuloComponent, ComboSubmoduloComponent, ComboPermisoComponent, ComboAlmacenComponent,
        MatExpansionModule, MatAutocompleteModule, MatFormFieldModule, MatSelectModule, FormsModule]
})
export class TablaPermisosComponent implements OnInit, AfterViewInit {


  panelOpenState = false;

  disableSelect = new FormControl(false);

  dataSource!: MatTableDataSource<any>;
  selection = new SelectionModel<Permisototal>(true, []);

  permisoTotal!: Permisototal;
  selectedRows: any[] = [];
  selectAllChecked = false;
  supermoduloColumnas: string[] = ['select','id_permiso_total', 'codigo_almacen' ,'almacen','supermodulo','modulo','submodulo', 'permiso', 'estado'];


  selectedSupermodulo: any = null;
  selectedModulo: any = null;
  selectedSubmodulo: any = null;
  selectedPermiso: any = null;
  selectedAlmacen: any = null;
  selectedEstado: any = null;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  @Input() id_usuario!: number;


  constructor(
    private permisoTotalService: PermisoTotalService,
    private dialog: MatDialog,
    private cd: ChangeDetectorRef
  )
  {
  }
  ngOnInit(): void {
    this.listarPermisoTotal(this.id_usuario);
  }
  ngAfterViewInit(): void {
    this.cd.detectChanges();
  }

  listarPermisoTotal(id_usuario:number,parametros:any = {}): void {

    this.permisoTotalService.buscarPorUsuario(id_usuario,parametros).subscribe(
      (response) => {
        if (response.success) {

          this.dataSource = new MatTableDataSource(response.data);
          this.dataSource.paginator = this.paginator;
          this.selection.clear();
        } else {

          console.error('Error al listar supermódulos:', response);

        }
      },
      (error) => {
        console.error('Error al listar supermódulos:', error);
      }
    );
  }

  abrirFormulario(permisototalEditar?: Permisototal,id_usuario:number = this.id_usuario) {
    const dialogRef = this.dialog.open(ModalFormPermisoTotalComponent, {

      data: {permisototalEditar,id_usuario}
    });

    dialogRef.componentInstance.crearConfirmado.subscribe(confirmado => {
      if (confirmado) {
        this.buscarFiltrado();
      }
    });

  }


  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    var numRows;
    if (this.dataSource!=undefined) {
      numRows = this.dataSource.data.length;
    }else{
      numRows = 0;
    }
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource.data);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Permisototal): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id_permiso_total + 1}`;
  }


  abrirDialogoEliminar(): void {

    console.log(this.selection.selected.length)

    const dialogRef = this.dialog.open(DialogEliminarComponent, {
      data: {
        registro: this.selection,
        mensaje: `Se eliminaran ${this.selection.selected.length} registros, dese continuar?`,
        nombre: `PERMISO TOTAL`
      }
    });

    dialogRef.componentInstance.eliminarConfirmado.subscribe(confirmado => {
      console.log(confirmado)
      if (confirmado) {
        this.eliminarPermisoTotal(this.selection.selected);
      }
    });
  }

  eliminarPermisoTotal(seleccionado :any){
    console.log(seleccionado)

    this.permisoTotalService.eliminar(seleccionado).subscribe(
      (response) => {
        console.log(response)
        this.buscarFiltrado();
        console.log(response,'Supermódulo eliminado exitosamente.');
      },
      error => {
        console.error('Error al eliminar supermódulo:', error);
      }
    );
  }

  buscarFiltrado(){
    var selectedSupermodulo = this.selectedSupermodulo;
    var selectedModulo = this.selectedModulo;
    var selectedSubmodulo = this.selectedSubmodulo;
    var selectedPermiso = this.selectedPermiso;
    var selectedAlmacen = this.selectedAlmacen;
    var selectedEstado = this.selectedEstado;
    var parametros = {selectedSupermodulo,selectedModulo,selectedSubmodulo,selectedPermiso,selectedAlmacen,selectedEstado}
    this.listarPermisoTotal(this.id_usuario,parametros);
  }


  onSupermoduloSeleccionado(supermodulo: any): void {
    this.selectedSupermodulo = supermodulo.id_supermodulo;
    this.selectedModulo = null;
    this.selectedSubmodulo = null;
    this.selectedPermiso = null;
  }

  onModuloSeleccionado(modulo: any): void {
    console.log(modulo,"onModuloSeleccionado");
    if (modulo != null) {
      this.selectedModulo = modulo.id_modulo;
    }else{
      this.selectedModulo = null;
    }

    this.selectedSubmodulo = null; // Reiniciar selección de submódulo
    this.selectedPermiso = null;
  }

  onSubmoduloSeleccionado(submodulo: any): void {
    if (submodulo != null) {
      this.selectedSubmodulo = submodulo.id_submodulo;
    }else{
      this.selectedSubmodulo = null;
    }

    this.selectedPermiso = null;
  }

  onPermisoSeleccionado(permiso: any): void {
    this.selectedPermiso = permiso;
  }

  onAlmacenSeleccionado(almacen: any): void {
    console.log(almacen);
    this.selectedAlmacen = almacen;
  }

  activarPermisosTotales(){
    var seleccionado = this.selection.selected
    this.permisoTotalService.desactivar(seleccionado).subscribe(
      (response) => {
        console.log(response)
        this.buscarFiltrado();
        console.log(response,'Supermódulo eliminado exitosamente.');
      },
      error => {
        console.error('Error al eliminar supermódulo:', error);
      }
    );
  }

  desactivarPermisosTotales(){
    var seleccionado = this.selection.selected
    this.permisoTotalService.activar(seleccionado).subscribe(
      (response) => {
        console.log(response)
        this.buscarFiltrado();
        console.log(response,'Supermódulo eliminado exitosamente.');
      },
      error => {
        console.error('Error al eliminar supermódulo:', error);
      }
    );
  }
}
