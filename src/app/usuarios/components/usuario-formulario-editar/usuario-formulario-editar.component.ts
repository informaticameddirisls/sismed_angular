import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { DataService } from '../../../shared/services/data.service';
import { FormValidationService } from '../../validators/form-validation.service';
import { Router } from '@angular/router';
import { UsuarioService } from '../../services/usuario.service';
import { MatSelectModule } from '@angular/material/select';
import { ComboDocumentoIdentidadService } from '../../../shared/services/combo/combo-documento-identidad.service';
import { catchError, map, Observable } from 'rxjs';
import { MatCheckboxModule } from '@angular/material/checkbox';

@Component({
    selector: 'app-usuario-formulario-editar',
    imports: [CommonModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatCheckboxModule],
    templateUrl: './usuario-formulario-editar.component.html',
    styleUrl: './usuario-formulario-editar.component.css'
})
export class UsuarioFormularioEditarComponent {

  formularioUsuario: FormGroup;
  comboDocumentoIdentidad$: Observable<any[]>;
  @Input() id_usuario!: number;
  //titulo para la pagina
  @Output() tituloEnviado = new EventEmitter<string>();

  constructor(
    private dataService: DataService,
    private fb: FormBuilder,
    private formValidationService: FormValidationService,
    private router: Router,
    private usuarioService: UsuarioService,
    private comboDocumentoIdentidadService: ComboDocumentoIdentidadService
  ){
    this.formularioUsuario = this.fb.group({
      id_usuario: [0, Validators.required],
      nombres: ['', Validators.required],
      ape_paterno: ['', Validators.required],
      ape_materno: ['', Validators.required],
      id_tipo_documento: [''],
      numero_documento: [''],
      sexo: [''],
      fecha_nac: [''],
      correo_electronico: ['', [Validators.required, Validators.email]],
      direccion: [''],
      celular: [''],
      password: [''],
    });

    // Initialize comboDocumentoIdentidad$ as an Observable
    this.comboDocumentoIdentidad$ = this.comboDocumentoIdentidadService.listarDocIdentidad().pipe(
      map(response => response.resultado) // Ajusta según la estructura de tu respuesta
    );
  }


  ngOnInit(): void {
    this.buscarUsuario(this.id_usuario);
  }
  // Método para obtener los controles del formulario
  get f() {
    return this.formularioUsuario.controls;
  }

  onSubmit() {
    if (this.formularioUsuario.valid) {
      // Procesar el formulario si es válido
      const usuario = this.formularioUsuario.value;

      this.usuarioService.editar(usuario).subscribe(
        (response)=>{
          if (response.success) {
            this.router.navigate(['sismed', 'administrador', 'maestra', 'usuario', 'editar', response.data["id_usuario"]]);
          }
        }
      )
      console.log('Datos del formulario:', usuario);
      // Llamar al servicio para guardar el usuario
      // this.usuarioService.guardarUsuario(usuario).subscribe(...);
    } else {
      // Marcar todos los campos como tocados para mostrar errores
      this.formularioUsuario.markAllAsTouched();
    }
  }



  buscarUsuario(idUsuario: number){
    this.usuarioService.buscar(idUsuario).subscribe(
      response => {
        this.formularioUsuario.patchValue({
          id_usuario: response.data.id_usuario,
          nombres: response.data.persona.nombres,
          ape_paterno: response.data.persona.ape_paterno,
          ape_materno: response.data.persona.ape_materno,
          id_tipo_documento: response.data.persona.id_tipo_documento,
          numero_documento: response.data.persona.numero_documento,
          sexo: response.data.persona.sexo,
          fecha_nac: response.data.persona.fecha_nac,
          correo_electronico: response.data.correo_electronico,
          direccion: response.data.persona.direccion,
          celular: response.data.persona.celular,
          password: '',
        });
      },
      error => {
        console.error('Error fetching user', error);
      }
    );
  }

    esCampoValido(field: string): boolean | null {
      return this.formValidationService.esCampoValido(this.formularioUsuario, field);
    }

    mensajeError(field: string): string | null {
      return this.formValidationService.mensajeError(this.formularioUsuario, field);
    }

    // Método para validar y mostrar el mensaje de error
    mostrarError(field: string): boolean | null {
      if (!this.formularioUsuario.controls[field]) return false;
      return this.formValidationService.esCampoValido(this.formularioUsuario, field);
    }


    regresar() {
      this.router.navigate(['sismed', 'administrador', 'maestra', 'usuario', 'listar']);
    }


}
