import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioRoutingModule } from './usuario-routing.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MaterialModule } from '../material/material.module';



@NgModule({ declarations: [], imports: [CommonModule,
        MaterialModule,
        UsuarioRoutingModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class UsuarioModule { }
